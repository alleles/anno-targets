#!/usr/bin/env python3
# -*- coding: ISO-8859-1 -*-

from fpdf import FPDF, set_global

# Do not create cache-files for FPDF
set_global("FPDF_CACHE_MODE", 1)



class MyFPDF(FPDF):

    def inc_y(self, n):
        x, y = self.x, self.y
        self.set_xy(x, y+n)

    def inc_x(self, n):
        x, y = self.x, self.y
        self.set_xy(x+n, y)

    def inc_xy(self, nx, ny):
        x, y = self.x, self.y
        self.set_xy(x+nx, y+ny)

    def footer(self):
        x, y = self.x, self.y
        self.set_xy(150, 2)
        self.multi_cell(50, 5, 'Vedlegg side {} av {{nb}}'.format(self.page_no()), align="R")
        self.set_xy(x, y)



class Style(object):

    def __init__(self, pdf, style):
        self.pdf = pdf
        self.style = style
        self.store_attrs = [
            'font_family',
            'font_style',
            'font_size_pt',
            'font_size',
            'text_color',
        ]


    def store_style(self):
        for attr in self.store_attrs:
            setattr(self, attr, getattr(self.pdf, attr))


    def restore_style(self):
        # Cannot set attrs directly as set_font() does some font loading magic
        self.pdf.set_font(
            self.font_family,
            self.font_style,
            self.font_size_pt
        )
        self.pdf.text_color = self.text_color


    def set_style(self):
        if self.style is None:
            return

        self.pdf.set_font(
            self.style.get('font_family', self.font_family),
            self.style.get('font_style', self.font_style),
            self.style.get('font_size', self.font_size_pt)
        )

        if 'font_color' in self.style:
            self.pdf.set_text_color(*self.style['font_color'])


    def __enter__(self):
        self.store_style()
        self.set_style()


    def __exit__(self, type, value, traceback):
        self.restore_style()



class Table(object):
    """
    Custom table code, as the HTML/table writer built into fpdf isn't working well.
    """

    DEFAULT_WIDTH = 10

    def __init__(self, pdf):
        self.pdf = pdf


    def _get_cell_lines_count(self, cells, width):
        x_unit = float(width) / 100
        # Get number of lines
        ln = dict()
        for no, h in enumerate(cells):
            w = h.get('w', Table.DEFAULT_WIDTH) * x_unit
            ln[no] = len(self.pdf.multi_cell(w, 1, h['text'], 'LRTB', align='C', split_only=True))
        return ln


    def _get_max_row_lines(self, cells, width):
        return max(self._get_cell_lines_count(cells, width).values())


    def check_page_break(self, y, cell_height):
        if y + cell_height > self.pdf.page_break_trigger:
            y = (y % (self.pdf.page_break_trigger - cell_height)) + self.pdf.t_margin
            self.pdf.add_page()
            return y
        return y


    def write_row(self, start_x, start_y, cells, width=250, line_height=5):
        """
        Writes out one row of cells (dict).
        One cell is defined like:
        {
            'text': str (required),
            'w': int (optional),  # Width of cell
            'style': tuple (optional), # Style for cell
        }
        """

        x = start_x
        x_unit = float(width) / 100
        ln = self._get_cell_lines_count(cells, width)

        ln_max = max(ln.values())
        for no, h in enumerate(cells):
            w = h.get('w', Table.DEFAULT_WIDTH) * x_unit
            # Pad lines so they are equal to ln_max
            txt = h['text'] + '\n' * (ln_max - ln[no] + 1)
            self.pdf.set_xy(x, start_y)
            x += w
            with Style(self.pdf, h.get('style')):
                align = h.get('style', {}).get('align', 'C')
                self.pdf.multi_cell(w, line_height, txt, 'LRTB', align=align)
        return ln_max


    def write_rows(self, rows, start_x, start_y, line_height=5, width=250):
        # Disable page break as we handle it ourselves (fpdf's is somewhat broken)
        b_margin = self.pdf.b_margin
        page_break = self.pdf.auto_page_break

        # Margin is relevant. Keep at 10 to get good bottom margin
        self.pdf.set_auto_page_break(False, margin=10)

        # Write out table
        y = start_y
        for row in rows:
            row_lines = self._get_max_row_lines(row, width)
            # Check if we should add new page before proceeding
            y = self.check_page_break(y, row_lines*line_height)
            self.write_row(start_x, y, row, line_height=line_height, width=width)
            y += row_lines * line_height

        # Restore auto page break
        self.pdf.set_auto_page_break(page_break, margin=b_margin)

