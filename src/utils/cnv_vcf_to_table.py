import sys
import vcfiterator
from vcfiterator.processors import VEPInfoProcessor

COLUMN_KEYS = {
    'pos': 'Position',
    'type': 'Type',
    'score': 'Score',
    'length': 'Length',
    'indb': 'inDB count (exact / overlapping)',
    'genes': 'Genes',
    'genes_exons': 'Genes with exons',
    'csq': 'Consequences'
}

COLUMNS_ALIGN = {
    'pos': 'l',
    'type': 'c',
    'score': 'c',
    'length': 'c',
    'indb': 'c',
    'genes': 'l',
    'genes_exons': 'l',
    'csq': 'l'
}

FORMATTERS = {
    'pos': lambda x: str(x),
    'type': lambda x: str(x),
    'score': lambda x: str(x) if x < 10 else '**{}**'.format(str(x)),
    'length': lambda x: str(x),
    'indb': lambda x: '{} / {}'.format(*x),
    'genes': lambda x: '{} genes'.format(len(x)) if len(x) > 10 else ', '.join(sorted(list(x))),
    'genes_exons': lambda x: ', '.join(sorted(list(x))),
    'csq': lambda x: ' | '.join([', '.join(i) for i in sorted(list(x))])
}


def print_mdtable_header(columns):
    assert all(i in COLUMN_KEYS for i in columns)
    md_header = '| {} |'.format(' | '.join([COLUMN_KEYS[c] for c in columns]))
    BASE = {
        'l': ':-----',
        'c': ':----:',
        'r': '-----:'
    }
    md_header_base = ''
    for c in columns:
        md_header_base += '| ' + BASE[COLUMNS_ALIGN[c]]
    md_header_base += ' |'
    print(md_header + '\n' + md_header_base)


def print_mdtable_rows(vcf_file, columns):
    assert all(i in COLUMN_KEYS for i in columns)
    rows = sorted(
        list(get_rows(vcf_file)),
        key=lambda x: (x['type'], -int(x['score']))
    )
    for row in rows:
        included = [FORMATTERS[c](row[c]) for c in columns]
        print('| {} |'.format(' | '.join(included)))


def get_rows(vcf_file):
    vi = vcfiterator.VcfIterator(vcf_file)
    vi.addInfoProcessor(VEPInfoProcessor)

    for line in vi.iter():
        assert len(line['ALT']) == 1
        alt = line['ALT'][0]

        # This script only handles CNVs
        if alt != '<DEL>' and alt != '<DUP>':
            continue

        position = '{}:{}-{}'.format(
            line['CHROM'],
            line['POS'],
            line['INFO']['ALL']['END']
        )
        svtype = alt.replace('<', '').replace('>', '')
        length = abs(int(line['INFO']['ALL']['SVLEN']))
        try:
            score = float(line['QUAL'])
        except ValueError:
            score = 0.0
        genes = set()
        genes_with_exons = set()
        consequences = set()
        for t in line['INFO'][alt]['CSQ']:
            if 'SYMBOL' in t:
                genes.add(t['SYMBOL'])
                if t.get('EXON') and t.get('Feature').startswith('NM_'):
                    genes_with_exons.add('{} ({}: {})'.format(t['SYMBOL'], t['Feature'], t['EXON']))
            if t.get('Feature', '').startswith('NM_'):
                consequences.add(tuple(t['Consequence']))

        indb_identical_count = int(line['INFO']['ALL'].get('inDB_identical_count', 0))
        indb_overlap_count = int(line['INFO']['ALL'].get('indb_overlap_count', 0))

        row = {
            'pos': position,
            'type': svtype,
            'length': length,
            'score': score,
            'indb': (indb_identical_count, indb_overlap_count),
            'genes': genes,
            'genes_exons': genes_with_exons,
            'csq': consequences
        }

        yield row


if __name__ == "__main__":

    vcf_file = sys.argv[1]
    columns = sys.argv[2].split(',')
    print_mdtable_header(columns)
    print_mdtable_rows(vcf_file, columns)
