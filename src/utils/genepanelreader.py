import csv
import os
import glob
import logging

from collections import OrderedDict

log = logging.getLogger(__name__)


class GenepanelReader(object):
    """
    Parses a transcripts file from a gene panel and returns the result.
    If a phenotypes file is available, the result is merged into the result.
    """

    def _load_transcripts(self, transcripts_path):
        transcripts = OrderedDict()
        with open(transcripts_path) as fd:
            reader = csv.reader(fd, delimiter='\t')
            header = None
            for idx, r in enumerate(reader):
                # Skip first header
                if idx == 0:
                    continue
                if idx == 1:
                    # Split away first '#'
                    r[0] = r[0][1:]
                    header = r
                    continue
                if not r:
                    continue
                t = dict(list(zip(header, r)))
                transcripts[t['name']] = {
                    'chrom': t['chromosome'],
                    'tx_start': int(t['read start']),
                    'tx_end': int(t['read end']),
                    'gene': t['HGNC symbol'],
                    'gene_alias': t.get('gene aliases', '').split(','),
                    'transcript': t['name'],
                    'chromosome': t['chromosome'],
                }
                if 'inheritance' in t:
                    transcripts[t['name']]['inheritance'] = [t['inheritance']]
                if 'disease' in t:
                    transcripts[t['name']]['phenotype'] = [t['disease']]

        return transcripts

    def _load_phenotypes(self, phenotypes_path):
        if not phenotypes_path:
            return dict()
        phenotypes = dict()
        try:
            with open(phenotypes_path) as fd:
                reader = csv.reader(fd, delimiter='\t')
                header = None
                for idx, r in enumerate(reader):
                    # Skip first header
                    if idx == 0:
                        continue
                    if idx == 1:
                        # Split away first '#'
                        if r[0][0] == '#':
                            r[0] = r[0][1:]
                        header = r
                        continue
                    if not r:
                        continue
                    t = dict(list(zip(header, r)))

                    # Any additional columns will be join by commas and returned as a single dict entry
                    extra_phenotypes_header = [h for h in header if h not in ['HGNC symbol', 'HGNC id', 'phenotype', 'inheritance',
                        'phenotype MIM number', 'pmid', 'inheritance info', 'comment']]
                    if t['HGNC symbol'] not in phenotypes:
                        phenotypes[t['HGNC symbol']] = {
                            'gene': t['HGNC symbol'],
                            'phenotype': [t['phenotype']],
                            'inheritance': [t['inheritance']],
                            'phenotype MIM number': [t['phenotype MIM number']],
                            'inheritance info': [t['inheritance info']],
                            'HGNC id': [t.get('HGNC id', '')],
                            'extra_phenotypes': [','.join([t[eph] for eph in extra_phenotypes_header if t.get(eph)])]
                        }
                    else:
                        for field in ['phenotype', 'inheritance', 'phenotype MIM number', 'inheritance info', 'HGNC id']:
                            phenotypes[t['HGNC symbol']][field].append(t.get(field, ''))
                        phenotypes[t['HGNC symbol']]['extra_phenotypes'].append(','.join([t[eph] for eph in extra_phenotypes_header if t.get(eph)]))
        except FileNotFoundError as e:
            log.warning('Could not load phenotypes: ' + str(e))

        return phenotypes

    def _merge_transcripts_phenotypes(self, transcripts, phenotypes):
        for transcript_name in transcripts:
            gene_symbol = transcripts[transcript_name]['gene']
            if gene_symbol in phenotypes:
                transcripts[transcript_name].update(phenotypes[gene_symbol])

        return transcripts

    def get_display_inheritance(self, transcript):
        """
        Cleans the inheritance of the transcript, renaming some codes
        and gives out the sorted, distinct set of codes separated by comma.

        Codes that doesn't start with 'AR', 'AD' or 'X' are filtered out.
        """
        display_codes = list()
        try:
            for code in transcript['inheritance']:
                display_codes += code.replace('/', ';').replace('XD', 'X').replace('XR', 'X').replace(',', ';').split(';')
                display_codes = [c.strip() for c in display_codes]
        except KeyError:
            display_codes = ['N/A']
            log.error('No inheritance code found for {}'.format(transcript['gene']))
        display_codes = sorted(list(set(display_codes)))
        # For some old panels we have stuff like 'AD (imprinted)' and similar, that we need to keep
        return ','.join([c for c in display_codes for s in ['AR', 'AD', 'X', 'N/A'] if c.startswith(s)])

    def get_transcripts(self, transcripts_path, phenotypes_path=None):
        """
        :param transcripts_path: Path to genepanel's transcripts file.
        :param phenotypes_path: Path to genepanel's phenotype file. If None, try to autodetect it's presence.
        :returns: OrderedDict of format: {'NM_123.1': {data}, 'NM_125.1': {data}, ...}

        Phenotype file autodetection work by looking for the pattern *_phenotypes.tsv in the same
        folder as the input transcripts file's path.
        """

        transcripts = self._load_transcripts(transcripts_path)

        if phenotypes_path is None:
            # Try to find phenotypes files
            base_path = os.path.split(transcripts_path)[0]
            base_name = os.path.basename(base_path)
            match = glob.glob(os.path.join(base_path, '%s_phenotypes.tsv' % base_name))
            if match:
                phenotypes_path = match.pop()
            else:
                log.warn("Found no phenotypes file, assuming old-style genepanel.")

        if not phenotypes_path and not any('phenotype' in t for t in list(transcripts.values())):
            log.warn("Found no phenotype file and no phenotype information present in transcripts file.")

        if phenotypes_path:
            phenotypes = self._load_phenotypes(phenotypes_path)
            transcripts = self._merge_transcripts_phenotypes(transcripts, phenotypes)

        for t in list(transcripts.values()):
            t['display_inheritance'] = self.get_display_inheritance(t)

        return transcripts


if __name__ == '__main__':
    import argparse
    import json

    logging.basicConfig()

    parser = argparse.ArgumentParser(description='Loads a genepanel transcripts file and prints the result as json')
    parser.add_argument('--transcripts', required=True, help='Path to genepanel transcripts file', dest='transcripts')
    parser.add_argument('--phenotypes', required=False, help='Path to genepanel phenotypes file (will try to find automatically if not provided)', dest='phenotypes')

    args = parser.parse_args()

    print(json.dumps(GenepanelReader().get_transcripts(args.transcripts, phenotypes_path=args.phenotypes), indent=4))
