import os
import sys
import argparse

def create_ped_file(output, family, proband, proband_sex, father, mother):

    proband_sex_no = 1 if proband_sex == 'male' else 2

    template = "{family}\t{proband}\t{father}\t{mother}\t{sex}\t2\t1\n"
    template += "{family}\t{father}\t0\t0\t1\t1\t0\n"
    template += "{family}\t{mother}\t0\t0\t2\t1\t0\n"

    with open(output, 'w') as f:
        f.write(
            template.format(
                family=family,
                proband=proband,
                mother=mother,
                father=father,
                sex=proband_sex_no
            )
        )

    print(".ped-file created for", output)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Create pedigree file for trio analysis")
    parser.add_argument("--output", help="Name of pedigree file", dest='output', required=True)
    parser.add_argument("--family", help="Family name", dest='family', required=True)
    parser.add_argument("--proband", help="Proband sample ID", dest='proband', required=True)
    parser.add_argument("--proband_sex", help="Proband gender", dest='proband_sex', required=True)
    parser.add_argument("--father", help="Father sample ID", dest='father', required=True)
    parser.add_argument("--mother", help="Mother sample ID", dest='mother', required=True)

    args = parser.parse_args()

    create_ped_file(args.output, args.family, args.proband, args.proband_sex, args.father, args.mother)
