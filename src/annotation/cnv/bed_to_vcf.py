import sys

"""
CNV bed file to VCF converter.
Not meant to be a complete converter, but enough for simple annotation.

Expected input format:

1	0	65973	1	5.06
1	2435306	2435612	3	5.89
1	3328188	3329394	3	8.77
1	4772023	4772435	3	5.52

Output:


Based on minimal example per VEP documentation:

#CHROM  POS     ID   REF  ALT    QUAL  FILTER  INFO                    FORMAT
1       160283  sv1  .    <DUP>  .     .       SVTYPE=DUP;END=471362   .
1       1385015 sv2  .    <DEL>  .     .       SVTYPE=DEL;END=1387562  .

"""

HEADER = """##fileformat=VCFv4.1
##source=ANNO_TARGETS
##ALT=<ID=DEL,Description="Deletion">
##ALT=<ID=DUP,Description="Duplication">
##INFO=<ID=END,Number=1,Type=Integer,Description="End position of the structural variant">
##INFO=<ID=IMPRECISE,Number=0,Type=Flag,Description="Imprecise structural variation">
##INFO=<ID=SVTYPE,Number=1,Type=String,Description="Type of the SV.">
##INFO=<ID=SVLEN,Number=.,Type=Integer,Description="Difference in length between REF and ALT allele">
"""

VCF_HEADERS = ['CHROM', 'POS', 'ID', 'REF', 'ALT', 'QUAL', 'FILTER', 'INFO']


class VcfWriter(object):
    def __init__(self, path):
        self.file_obj = open(path, 'w')
        self.written_count = 0
        self._write_header()

    def _write_header(self):
        self.file_obj.write(HEADER)
        self.file_obj.write('#' + '\t'.join(VCF_HEADERS) + '\n')

    def __enter__(self):
        return self

    def write_data(self, data):

        chrom = data['chrom']
        start = int(data['start']) + 1  # BED is 0-index, VCF is 1-indexed
        end = int(data['end'])  # bed end is not inclusive
        cnv_type = data['cnv_type']
        score = float(data['score'])

        assert chrom in [str(i) for i in list(range(1, 23)) + ['X', 'Y', 'MT']]
        assert start
        assert end

        if cnv_type == '1':
            cnv_type = 'DEL'
        elif cnv_type == '3':
            cnv_type = 'DUP'

        assert cnv_type in ['DEL', 'DUP']

        if cnv_type == 'DEL':
            sv_len = start - end
        elif cnv_type == 'DUP':
            sv_len = end - start

        vcf_data = [
            chrom,
            str(start),
            'CONVERTED_{}'.format(str(self.written_count + 1)),
            'N',
            '<{}>'.format(cnv_type),
            '{0:.2f}'.format(score),
            '.',
            'IMPRECISE;SVTYPE={cnv_type};END={end};SVLEN={sv_len}'.format(cnv_type=cnv_type, end=end, sv_len=sv_len),
        ]
        self.file_obj.write(
            '\t'.join(vcf_data) + '\n'
        )
        self.written_count += 1

    def __exit__(self, type, value, traceback):
        self.file_obj.close()


BED_COLUMNS = ['chrom', 'start', 'end', 'cnv_type', 'score']


def read_bed(bed_file):
    with open(bed_file) as f:
        for line in f:
            if line.startswith('#'):
                continue
            line_data = {k: v.replace('\n', '') for k, v in zip(BED_COLUMNS, line.split('\t'))}
            yield line_data


if __name__ == "__main__":

    bed_input = sys.argv[1]
    vcf_output = sys.argv[2]

    with VcfWriter(vcf_output) as writer:
        for line in read_bed(bed_input):
            writer.write_data(line)

        print("Wrote {} lines".format(writer.written_count))
