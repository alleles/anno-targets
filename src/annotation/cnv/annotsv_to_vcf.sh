#!/usr/bin/env bash
set -euf -o pipefail

###### Format AnnotSV tsv to vcf
##
# Input tsv file (full path)
TSV=$1
PREFIX=${TSV%.*}
# Static config file with information about AnnotSV columns
HEADER_CONFIG=$2
PREFIX_HEADER=$(basename "${HEADER_CONFIG%.*}")
# VCF to annotate
VCF=$3
# Control the output filename
OUTPUT_FILENAME=$4

if [[ ! -f ${TSV} || ! -f ${HEADER_CONFIG} || ! -f ${VCF} ]]; then
    echo "One of the input files does not exist, aborting!"
    exit 1
fi
####### Helper extract subset of header values
function extract_cols() {
    ### Given file containing list of column names, one per line
    ### subset another file by header - selecting the subset of fields
    awk -F'\t' 'NR==FNR{
      arr[$1]++
      next
    }
    FNR==1{
      for(i=1; i<=NF; i++)
        if ($i in arr) a[i]++
    }
    {
      for (i in a)
        printf "%s\t", $i
      printf "\n"
    }' "${1}" "${2}"
}

#######
# Remove sample name from the header,
# otherwise it will always differ between samples.
awk -F'\t' -v OFS="\t" '{
    k=$1; 
    for (j=2;j<=NF;j++){
      if ($j~"compound_htz"){
        gsub("\\(.*","",$j)
        }; 
      k=k"\t"$j
      }; 
    print k}' "${TSV}" >"${PREFIX}1.tsv"
mv "${PREFIX}1.tsv" "${TSV}"

# Indicate a subset of columns to extract
# In the "full" mode the "split" columns are also returned but they all will be null
cat <<EOF >"${PREFIX_HEADER}.ids"
SV_chrom
SV_start
SV_end
EOF

# Columns from the config file selection
jq -r '.WGS.annotsv.fields.in[]' "${ANALYSIS_TYPE_CONFIG}" >>"${PREFIX_HEADER}.ids"

# Make a VCF header primer from a static config data file
CMD="jq -r '.[] | select( .Full == \"X\" and .Vcf == \"0\") |\
  [.Colname, .Annotation, .Full, .Split, .Number,.Type ] |\
  @tsv' \"${HEADER_CONFIG}\" |\
  awk 'NR==FNR{a[\$1]=1;next}{f=a[\$1]}f' \"${PREFIX_HEADER}.ids\" - |\
	awk -F'\t' -v OFS=\",\" '!(\$1~\"^SV_\"){print \"##INFO=<ID=\"\$1,\"Number=\"\$5,\"Type=\"\$6,\"Description=\\\"\"\$2\"\\\">\"}'"
eval "${CMD}" >"${PREFIX_HEADER}.hdr"

# Make a bed file with selected columns
CMD="extract_cols ${PREFIX_HEADER}.ids ${TSV} |\
	awk -F'\t' -v OFS=\"\t\" 'NR>1' |\
	sort -k1,1V -k2,2n |\
	perl -MURI::Escape -wlne 'print uri_escape \$_' |\
	sed 's/%09/\t/g' |\
	sed 's/%5Cn/\n/g'"
echo "${CMD} | bgzip >${PREFIX}.annotsv.bed.gz"
eval "${CMD}" | bgzip >"${PREFIX}.annotsv.bed.gz"

# Index bgzipped bed file - needed for bcftools
CMD="tabix -p bed ${PREFIX}.annotsv.bed.gz"
echo "${CMD}"
eval "${CMD}"

# Create a comma-separated list of column names to add to vcf
ANN_LIST=$(extract_cols "${PREFIX_HEADER}.ids" "${TSV}" \
    | awk 'NR==1{s=""; for (i=1;i<=NF;i++){if(!($i~ "^SV_")){s=s","$i} };gsub("^,","",s); print s}')

# Annotate the vcf with prepped bed file and selected column values (order matters!)
CMD="bcftools annotate -a ${PREFIX}.annotsv.bed.gz -h ${PREFIX_HEADER}.hdr -c CHROM,FROM,TO,${ANN_LIST} ${VCF} |\
  /anno-targets/bin/sv_standardizer --caller \"header\" - "
echo "${CMD}  >${OUTPUT_FILENAME}"
eval "${CMD}" >"${OUTPUT_FILENAME}"
