#!/bin/bash

_THISDIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
source "${_THISDIR}"/../../../thirdparty/log.sh

function cgrep() {
    local RETURN_CODE=0
    grep "$@" || RETURN_CODE=$?
    if ((RETURN_CODE > 1)); then
        return "${RETURN_CODE}"
    fi
}

function remove_decoys() {
    # Filter decoy contigs from vcf records and vcf header
    local INPUT_VCF="$1"
    local CANONICAL_CHROMS="$2"
    local CNV_CALLERS="$3"
    local FASTA_FAI="$4"
    local OUTFILE="$5"

    # Filter decoy contigs from vcf records
    sv_wgs_filtering \
        "${INPUT_VCF}" \
        --set-interpretation-group "${CANONICAL_CHROMS}" \
        --caller-priority "${CNV_CALLERS}" \
        --output-format vcf >tmp.vcf

    # Filter decoy contigs from vcf header
    # shellcheck disable=SC2046
    awk '!($1~"^GL|^hs|^NC|^MT")' "${FASTA_FAI}" >tmp.genome.fasta.fai

    # Clean up temporary files when the function returns control to the shell
    trap 'rm tmp.genome.fasta.fai tmp.vcf' RETURN

    # Reheader the vcf
    bcftools reheader -f tmp.genome.fasta.fai tmp.vcf >"${OUTFILE}"
}

function split_by_size() {
    # Split input file by given size groups
    local INPUT_VCF="$1"
    local LARGE_GROUP="$2"
    local SMALL_GROUP="$3"
    local CNV_CALLERS="$4"
    local OUTPUT_LARGE="$5"
    local OUTPUT_SMALL="$6"

    # Filter large variants
    # shellcheck disable=SC2086
    sv_wgs_filtering \
        "${INPUT_VCF}" \
        --set-interpretation-group "${LARGE_GROUP}" \
        --caller-priority ${CNV_CALLERS} \
        --output-format vcf >"${OUTPUT_LARGE}"

    # Filter small variants
    # shellcheck disable=SC2086
    sv_wgs_filtering \
        "${INPUT_VCF}" \
        --set-interpretation-group "${SMALL_GROUP}" \
        --caller-priority ${CNV_CALLERS} \
        --output-format vcf >"${OUTPUT_SMALL}"

    # Count variants
    local N_LARGE_VARIANTS N_SMALL_VARIANTS
    N_LARGE_VARIANTS=$(cgrep -vc "^#" "${OUTPUT_LARGE}")
    N_SMALL_VARIANTS=$(cgrep -vc "^#" "${OUTPUT_SMALL}")

    # Return counts (can be captured if needed)
    echo "${N_LARGE_VARIANTS} ${N_SMALL_VARIANTS}"
}

function filter_by_overlap() {
    local INPUT_VCF="$1"
    local OVERLAP_FILE="$2"
    local SLOP="$3"
    local CHROMSIZES="$4"
    local PARAMS=("${@:5}")

    bedtools slop -i "${OVERLAP_FILE}" -g "${CHROMSIZES}" -b "${SLOP}" \
        | bedtools intersect -wa -a "${INPUT_VCF}" -b stdin "${PARAMS[@]}"
}

function combine_vcfs() {
    cat "$1"
    cgrep -v "^#" "$2"
}

annotate_cnv_with_vep() {
    local INPUT_VCF="$1"
    local ANALYSIS_TYPE_CONFIG="$2"
    local ANNO_DATA="$3"
    local FASTA="$4"
    local CNV_CALLERS="$5"
    local OUTFILE="$6"

    local N_INPUT_VARIANTS
    N_INPUT_VARIANTS=$(cgrep -vc "^#" "${INPUT_VCF}")

    if [[ "${N_INPUT_VARIANTS}" -eq "0" ]]; then
        # HACK: If there are no variants in the vcf, VEP doesn't write anything.
        LSINFO "No variants found. Relaying input as is."
        cp "${INPUT_VCF}" vep.vcf
    else
        # Extract the variants that will be removed by VEP (Default: >10Mbp)
        local INTERPRETATION_GROUP_NON_VEP
        INTERPRETATION_GROUP_NON_VEP=$(
            jq '.WGS.svparams.interpretation_groups.skip_vep' "${ANALYSIS_TYPE_CONFIG}"
        )
        # Note the requirement that '.gt' is used for interpretation_groups.skip_vep
        local MAX_SV_SIZE
        MAX_SV_SIZE=$(
            echo "${INTERPRETATION_GROUP_NON_VEP}" | jq --arg keyName "info:SVLEN" '.[$keyName].gt'
        )

        local REFSEQ_VERSION
        REFSEQ_VERSION=$(jq -r '.refseq.version' "${ANNO_DATA}/sources.json")
        local VEP_GFF
        VEP_GFF="${ANNO_DATA}/RefSeq/GRCh37_refseq_${REFSEQ_VERSION}_VEP.gff.gz"

        sv_wgs_filtering \
            "${INPUT_VCF}" \
            --set-interpretation-group "${INTERPRETATION_GROUP_NON_VEP}" \
            --caller-priority "${CNV_CALLERS}" \
            --output-format vcf >tmp_cnv_non_vep.vcf

        # Run VEP (parameters from FindSV)
        vep_offline \
            --buffer_size=1000 \
            --custom "${VEP_GFF}",RefSeq_gff,gff,overlap,1 \
            --fasta "${FASTA}" \
            --force_overwrite \
            --fork "$(nproc)" \
            --format=vcf \
            --max_sv_size="${MAX_SV_SIZE}" \
            --refseq \
            --symbol \
            --vcf \
            -i "${INPUT_VCF}" \
            -o vep.vcf \
            2> >(grep -v 'WARNING: The feature_type .\+ is being skipped$')
    fi
    vcf-sort -c vep.vcf >vep_sorted.vcf
    # This invocation makes sure that DUP GT is ./. (This cannot
    # be performed prior to SVDB database generation)
    sv_standardizer \
        vep_sorted.vcf \
        --caller "ella" >"${OUTFILE}"
}

function query_database() {
    local SAMPLE_FILE="$1"
    local DATABASE_FILE="$2"
    local IN_OCC="$3"
    local OUT_OCC="$4"
    local IN_FRQ="$5"
    local OUT_FRQ="$6"

    svdb --query \
        --query_vcf "${SAMPLE_FILE}" \
        --db "${DATABASE_FILE}" \
        --overlap 0.6 \
        --bnd_distance 10000 \
        --in_occ "${IN_OCC}" \
        --out_occ "${OUT_OCC}" \
        --in_frq "${IN_FRQ}" \
        --out_frq "${OUT_FRQ}"
}

annotate_cnv_with_freq() {
    local CALLER="$1"
    local ANNO_DATA="$2"
    local INPUT_VCF="$3"
    local OUTFILE="$4"

    # Set inhouse databases. Ensure existence
    LSINFO "Annotation by inhouse databases:"
    if [[ ${CALLER} == "Dragen" ]]; then
        set +o noglob
        INDB_DR_MANTA=$(ls "${WGS_CNV_INHOUSE_DB_DIR}"/indb_dr_manta_*.vcf.gz)
        INDB_DR_CANVAS=$(ls "${WGS_CNV_INHOUSE_DB_DIR}"/indb_dr_canvas_*.vcf.gz)
        set -o noglob
        if [[ ! -f "${INDB_DR_CANVAS}" ]]; then
            LSERROR "Unexpected INDB_DR_CANVAS=${INDB_DR_CANVAS}"
            exit 1
        fi
        if [[ ! -f "${INDB_DR_MANTA}" ]]; then
            LSERROR "Unexpected INDB_DR_MANTA=${INDB_DR_MANTA}"
            exit 1
        fi
        LSINFO "${INDB_DR_CANVAS} and ${INDB_DR_MANTA}"
    else
        set +o noglob
        INDB_CNVNATOR=$(ls "${WGS_CNV_INHOUSE_DB_DIR}"/indb_cnvnator_*.vcf.gz)
        INDB_DELLY=$(ls "${WGS_CNV_INHOUSE_DB_DIR}"/indb_delly_*.vcf.gz)
        INDB_MANTA=$(ls "${WGS_CNV_INHOUSE_DB_DIR}"/indb_manta_*.vcf.gz)
        INDB_TIDDIT=$(ls "${WGS_CNV_INHOUSE_DB_DIR}"/indb_tiddit_*.vcf.gz)
        set -o noglob
        if [[ ! -f "${INDB_CNVNATOR}" ]]; then
            LSERROR "Unexpected INDB_CNVNATOR=${INDB_CNVNATOR}"
            exit 1
        fi
        if [[ ! -f "${INDB_DELLY}" ]]; then
            LSERROR "Unexpected INDB_DELLY=${INDB_DELLY}"
            exit 1
        fi
        if [[ ! -f "${INDB_MANTA}" ]]; then
            LSERROR "Unexpected INDB_MANTA=${INDB_MANTA}"
            exit 1
        fi
        if [[ ! -f "${INDB_TIDDIT}" ]]; then
            LSERROR "Unexpected INDB_TIDDIT=${INDB_TIDDIT}"
            exit 1
        fi
        LSINFO "${INDB_MANTA}, ${INDB_CNVNATOR}, ${INDB_DELLY} and ${INDB_TIDDIT}"
    fi

    # Expecting in-house database directory as global variable WGS_CNV_INHOUSE_DB_DIR
    local SWEGEN_DIR GNOMAD_DIR
    SWEGEN_DIR="${ANNO_DATA}/variantDBs/SweGen-SV"
    GNOMAD_DIR="${ANNO_DATA}/variantDBs/gnomAD-SV"

    set +o noglob
    local SWEGEN_CNVNATOR SWEGEN_MANTA SWEGEN_DELLY SWEGEN_TIDDIT GNOMAD
    SWEGEN_CNVNATOR=$(ls "${SWEGEN_DIR}"/SweGen_CNVNATORSVDB_*.sort.vcf)
    SWEGEN_DELLY=$(ls "${SWEGEN_DIR}"/SweGen_DELLY_*.sort.vcf)
    SWEGEN_MANTA=$(ls "${SWEGEN_DIR}"/SweGen_MANTA_*.sort.vcf)
    SWEGEN_TIDDIT=$(ls "${SWEGEN_DIR}"/SweGen_TIDDIT_*.sort.vcf)
    GNOMAD=$(ls "${GNOMAD_DIR}"/gnomad_v*_sv.sites.vcf.gz)
    set -o noglob

    if [[ 
        ! -f "${SWEGEN_CNVNATOR}" ||
        ! -f "${SWEGEN_DELLY}" ||
        ! -f "${SWEGEN_MANTA}" ||
        ! -f "${SWEGEN_TIDDIT}" ||
        ! -f "${GNOMAD}" ]] \
        ; then
        LSINFO "ERROR: Expected 5 VCFs: \
            SWEGEN_CNVNATOR='${SWEGEN_CNVNATOR}'
            SWEGEN_DELLY='${SWEGEN_DELLY}'
            SWEGEN_MANTA='${SWEGEN_MANTA}'
            SWEGEN_TIDDIT='${SWEGEN_TIDDIT}'
            GNOMAD='${GNOMAD}'
        " >&2
        return 1
    fi
    # SVDB indb was made in a two step process
    # 1. svdb --build --files ${VCF_FILES_PR_CALLER} --prefix indb_${CALLER}_${VERSION}
    # 2. svdb --export \
    #	      --bnd_distance 10000 \
    #         --db indb_${CALLER}_${VERSION}.db \
    #    	  --overlap 0.8 \
    #	      --prefix indb_${CALLER}_${VERSION}
    # 3. ln -s $(realpath $indb_${CALLER}_${VERSION}.vcf) indb_${CALLER}.vcf
    local QUERY_SWEGEN_CNVNATOR
    QUERY_SWEGEN_CNVNATOR="${SWEGEN_CNVNATOR} OCC OCC_SWEGEN_CNVNATOR FRQ FRQ_SWEGEN_CNVNATOR"
    local QUERY_SWEGEN_DELLY
    QUERY_SWEGEN_DELLY="${SWEGEN_DELLY} OCC OCC_SWEGEN_DELLY FRQ FRQ_SWEGEN_DELLY"
    local QUERY_SWEGEN_MANTA
    QUERY_SWEGEN_MANTA="${SWEGEN_MANTA} OCC OCC_SWEGEN_MANTA FRQ FRQ_SWEGEN_MANTA"
    local QUERY_SWEGEN_TIDDIT
    QUERY_SWEGEN_TIDDIT="${SWEGEN_TIDDIT} OCC OCC_SWEGEN_TIDDIT FRQ FRQ_SWEGEN_TIDDIT"
    local QUERY_GNOMAD
    QUERY_GNOMAD="${GNOMAD} AC OCC_GNOMAD AF FRQ_GNOMAD"

    if [[ ${CALLER} == "Dragen" ]]; then
        QUERY_INDB_DR_CANVAS="${INDB_DR_CANVAS} OCC OCC_INDB_CANVAS FRQ FRQ_INDB_CANVAS"
        QUERY_INDB_DR_MANTA="${INDB_DR_MANTA} OCC OCC_INDB_MANTA FRQ FRQ_INDB_MANTA"

        declare -a QUERIES=(
            "${QUERY_INDB_DR_CANVAS}"
            "${QUERY_INDB_DR_MANTA}"
            "${QUERY_GNOMAD}"
            "${QUERY_SWEGEN_CNVNATOR}"
            "${QUERY_SWEGEN_DELLY}"
            "${QUERY_SWEGEN_MANTA}"
            "${QUERY_SWEGEN_TIDDIT}"
        )
    else
        QUERY_INDB_CNVNATOR="${INDB_CNVNATOR} OCC OCC_INDB_CNVNATOR FRQ FRQ_INDB_CNVNATOR"
        QUERY_INDB_DELLY="${INDB_DELLY} OCC OCC_INDB_DELLY FRQ FRQ_INDB_DELLY"
        QUERY_INDB_MANTA="${INDB_MANTA} OCC OCC_INDB_MANTA FRQ FRQ_INDB_MANTA"
        QUERY_INDB_TIDDIT="${INDB_TIDDIT} OCC OCC_INDB_TIDDIT FRQ FRQ_INDB_TIDDIT"

        declare -a QUERIES=(
            "${QUERY_INDB_CNVNATOR}"
            "${QUERY_INDB_DELLY}"
            "${QUERY_INDB_MANTA}"
            "${QUERY_INDB_TIDDIT}"
            "${QUERY_SWEGEN_CNVNATOR}"
            "${QUERY_SWEGEN_DELLY}"
            "${QUERY_SWEGEN_MANTA}"
            "${QUERY_SWEGEN_TIDDIT}"
            "${QUERY_GNOMAD}"
        )
    fi

    LSINFO "... starting frequency annotation by SVDB"
    for QUERY in "${QUERIES[@]}"; do
        IFS=$' ' read -ra QUERY_ARRAY <<<"${QUERY}"
        # shellcheck disable=SC2086
        query_database "${INPUT_VCF}" "${QUERY_ARRAY[@]}" >out.vcf
        mv out.vcf "${INPUT_VCF}"
    done
    LSINFO "Frequency annotation by SVDB done"
    mv "${INPUT_VCF}" "${OUTFILE}"
}

annotate_with_annotsv() {
    local INPUT_VCF="$1"
    local ANNO_DATA="$2"
    local VCF="$3"
    local OUTFILE="$4"

    local N_INPUT_VARIANTS
    N_INPUT_VARIANTS=$(cgrep -vc "^#" "${INPUT_VCF}")

    if [[ "${N_INPUT_VARIANTS}" -eq "0" ]]; then
        LSINFO "Empty input file, skipping AnnotSV"
        cp "${INPUT_VCF}" annotsv.vcf
    else
        if [[ -f ${VCF} ]]; then
            LSINFO "Filtered SNP file is available for compound heterozygosity calculation."
            LSINFO "Using it!"
            AnnotSV \
                -SVinputFile "${INPUT_VCF}" \
                -genomeBuild GRCh37 \
                -outputDir . \
                -outputFile annotsv.tsv \
                -annotationMode full \
                -annotationsDir "${ANNO_DATA}"/AnnotSV \
                -SVminSize 7 \
                -candidateSnvIndelFiles "${VCF}" 1>&2
        else
            LSINFO "Filtered SNP file is not available for compound heterozygosity calculation."
            LSINFO "Running AnnotSV without it!"
            AnnotSV \
                -SVinputFile "${INPUT_VCF}" \
                -genomeBuild GRCh37 \
                -outputDir . \
                -outputFile annotsv.tsv \
                -annotationMode full \
                -annotationsDir "${ANNO_DATA}"/AnnotSV \
                -SVminSize 7 1>&2
        fi

        LSINFO "...adding AnnotSV annotations to CNV vcf"
        annotsv_to_vcf annotsv.tsv \
            /anno-targets/src/annotation/cnv/header.template.json \
            "${INPUT_VCF}" \
            annotsv.vcf 1>&2
    fi
    mv annotsv.vcf "${OUTFILE}"
}

annotate_with_liftover() {
    local INPUT_VCF="$1"
    local ANNO_DATA="$2"
    local OUTFILE="$3"

    # bed file for mapping, passing the original coordinates via name column
    local QUERY_STR='chr%CHROM\t%POS\t%END\t%CHROM:%POS:%END\n'
    bcftools query -f "${QUERY_STR}" "${INPUT_VCF}" >original.bed

    # lift the variants over to GRCh38
    CrossMap bed --unmap-file unlifted.bed \
        "${ANNO_DATA}/liftOver/hg19ToHg38.over.chain" \
        original.bed \
        lifted.bed

    # create header lines for new annotations
    cat <<EOF >header.h
##INFO=<ID=hg38_coord,Number=.,Type=String,Description="Variant coordinates lifted over to hg38">
##INFO=<ID=hg38_chr,Number=1,Type=String,Description="Variant chromosome lifted over to hg38">
##INFO=<ID=hg38_start,Number=1,Type=Integer,Description="Variant start coordinate lifted over to hg38">
##INFO=<ID=hg38_end,Number=1,Type=Integer,Description="Variant end coordinate lifted over to hg38">
EOF

    # create bed file with annotation columns
    awk -v OFS='\t' '{
        gsub("chr", "", $1)
        split($4, a, ":")
        print a[1],a[2],a[3], $1":"$2"-"$3, $1, $2, $3
    }' lifted.bed | bgzip -c >lifted_annotated.bed.gz

    tabix -p bed -f lifted_annotated.bed.gz

    bcftools annotate -a lifted_annotated.bed.gz -h header.h \
        -c CHROM,FROM,TO,hg38_coord,hg38_chr,hg38_start,hg38_end "${INPUT_VCF}" \
        >"${OUTFILE}"

    # Clean up temporary files
    rm original.bed lifted.bed unlifted.bed header.h
    rm lifted_annotated.bed.gz lifted_annotated.bed.gz.tbi
}

add_quality_filters() {
    local INPUT_VCF="$1"
    local ANALYSIS_TYPE_CONFIG="$2"
    local CNV_CALLERS="$3"
    local OUTFILE="$4"

    local QUALITY_FILTERS
    QUALITY_FILTERS=$(jq '.WGS.svparams.filters' "${ANALYSIS_TYPE_CONFIG}")
    if [[ "${QUALITY_FILTERS}" != "null" ]]; then
        LSINFO "Adding additional quality filters: ${QUALITY_FILTERS}"
        # shellcheck disable=SC2086
        sv_postprocessing \
            "${INPUT_VCF}" \
            --set-filters "${QUALITY_FILTERS}" \
            --caller-priority ${CNV_CALLERS} >out.vcf
    else
        LSINFO "Not adding any additional quality filters"
        cp "${INPUT_VCF}" out.vcf
    fi
    mv out.vcf "${OUTFILE}"
}

apply_all_filters() {
    local INPUT_VCF="$1"
    local INTERPRETATION_GROUP_CNV="$2"
    local FREQUENCY_FILTERS="$3"
    local RESCUE_FILTERS="$4"
    local CNV_CALLERS="$5"
    local PARAMS=("${@:6}")

    # shellcheck disable=SC2086
    sv_wgs_filtering \
        "${INPUT_VCF}" \
        --set-interpretation-group "${INTERPRETATION_GROUP_CNV}" \
        --set-frequency-filters "${FREQUENCY_FILTERS}" \
        --set-rescue-filters "${RESCUE_FILTERS}" \
        --caller-priority ${CNV_CALLERS} \
        "${PARAMS[@]}"
}
