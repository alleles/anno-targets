import argparse
import re


class ConvertMutect2Vcf(object):
    def __init__(self, vcffile):
        self.vcf = vcffile

    def convert(self):
        with open(self.vcf, "r") as f:
            for line in f:
                line = line.rstrip()
                if line.startswith("#"):
                    print(line.replace(
                        "OLD_MULTIALLELIC", "OLD_MULTIALLELIC_DISREGARDED"
                    ))
                else:
                    vcf_column = line.split("\t")
                    gt_column = vcf_column[9].split(":")
                    if gt_column[0] != "0/1":
                        gt_column[0] = "0/1"
                        vcf_column[9] = ":".join(gt_column)

                    new_line = "\t".join(vcf_column)
                    print(new_line.replace(
                        "OLD_MULTIALLELIC", "OLD_MULTIALLELIC_DISREGARDED"
                    ))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Correct genotype field for mutect2 output vcf after decomposition"
    )
    parser.add_argument("--vcf", help="Path to vcf file.", dest="vcf", required=True)
    args = parser.parse_args()

    mutect2_vcf = ConvertMutect2Vcf(vcffile=args.vcf)

    mutect2_vcf.convert()
