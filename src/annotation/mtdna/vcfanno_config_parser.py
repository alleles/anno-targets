"""
VCFAnno configuration parser module
The `run` function processes the original VCFAnno configuration file, assesses which data sets in
the original annotation directory are relevant for the mitochondrion and writes a mtDNA-specific
configuration file returning its path to the caller.
"""

import gzip
import logging
import os

from vcfiterator import VcfIterator
from vcfiterator.processors import VEPInfoProcessor


logger = logging.getLogger(__name__)


class VCFAnnoConfigParserException(Exception):
    pass


def run(sequence_field_name, full_vcfanno_config, data_dir, ignore_list):
    logger.info("Reading VCFAnno configuration relevant to mtDNA")

    mt_vcfanno_config = dict()
    mt_vcfanno_config['annotation'] = list()

    annolist = full_vcfanno_config['annotation']

    ignore_arr = ignore_list.split(",")

    for a in annolist:
        if any(pattern in a["file"] for pattern in ignore_arr):
            logger.info(f"Skipping annotation VCF '{a['file']}' matching one of {ignore_arr}.")
            continue
        with gzip.open(os.path.join(data_dir, a["file"]), "rt") as vcf:
            if any(row[sequence_field_name] == "MT" for row in VcfIterator(vcf).iter()):
                mt_vcfanno_config['annotation'].append(a)

    return mt_vcfanno_config

