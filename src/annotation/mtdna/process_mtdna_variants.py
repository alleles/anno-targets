#!/usr/bin/env python

"""
This script generates tabular reports with information about called mtDNA variants.
It inputs mtDNA VCFs from either basepipe or triopipe.
"""

import argparse
import logging
import os
import subprocess
import sys
import tempfile
import toml

import vcfanno_config_parser
import vcf_parser


src_dir = os.path.dirname(os.path.dirname(__file__))
repo_base_dir = os.path.dirname(os.path.dirname(src_dir))
default_data_dir = os.getenv("ANNO_DATA", os.path.join(repo_base_dir, "anno-data"))

logger = logging.getLogger(__name__)


def get_analysis_info(analysis_file):
    from datamodel import analysis
    return analysis.from_env()


def parse_command_line_args(argv):
    parser = argparse.ArgumentParser(description="Mitochondrial variant processor")
    parser.add_argument(
        "--analysis-file",
        dest="analysis_file",
        help="Path to file containing analysis parameters [currently not in use]"
    )
    parser.add_argument(
        "--custom-opts",
        dest="custom_opts",
        help=f"Optional custom VEP options (`--custom` flag)"
    )
    parser.add_argument(
        "--data-dir",
        default=default_data_dir,
        dest="data_dir",
        help=f"Path to directory containing the annotation data [default='{default_data_dir}']"
    )
    parser.add_argument(
        "--ignore-list",
        default="",
        dest="ignore_list",
        help="List of data sets to be ignored in VCFAnno's configuration file for mtDNA variants"
    )
    parser.add_argument(
        "--logfile",
        dest="logfile",
        help="Path to write log messages to"
    )
    parser.add_argument(
        "--mtdna-ref",
        dest="mtdna_ref",
        help="Path to the mtDNA reference file (FASTA format)",
        required=True
    )
    parser.add_argument(
        "--nprocs",
        default=1,
        dest="nprocs",
        help="Number of parallel execution threads for VCFAnno"
    )
    parser.add_argument(
        "--vcfanno-config",
        dest="vcfanno_config",
        help="Path to the original VCFAnno configuration file",
        required=True
    )
    parser.add_argument("-o", default="mtdna-report.tsv", dest="output")
    parser.add_argument("-w", default="mtdna-warnings.txt", dest="warnings")
    parser.add_argument("-v", action="store_true", default=False, dest="verbose")
    parser.add_argument(dest="mtdna_vcf")
    return(parser.parse_args(argv))


def write_report(analysis_info, args):
    delete_temp = not args.verbose

    # VT decompose
    decomposed_vcf = tempfile.NamedTemporaryFile(
        dir=os.path.curdir, prefix="tmp_mtdna_decomposed_", suffix=".vcf", delete=delete_temp
    )
    logger.info("Decomposing VCF")
    cmdh = subprocess.run(
        [
            "vt", "decompose", "-s",
            "-o", decomposed_vcf.name,
            args.mtdna_vcf
        ],
        stderr=subprocess.PIPE,
    )
    if cmdh.returncode != 0:
        raise RuntimeError(f"Failed to decompose VCF: {cmdh.stderr.decode()}")

    # VT normalize
    normalized_vcf = tempfile.NamedTemporaryFile(
        dir=os.path.curdir, prefix="tmp_mtdna_normalized_", suffix=".vcf", delete=delete_temp
    )
    logger.info("Normalizing VCF")
    cmdh = subprocess.run(
        [
            "vt", "normalize", "-m",
            "-o", normalized_vcf.name,
            "-r", args.mtdna_ref,
            decomposed_vcf.name
        ],
        stderr=subprocess.PIPE,
    )
    if cmdh.returncode != 0:
        raise RuntimeError(f"Failed to normalize VCF: {cmdh.stderr.decode()}")

    # VCFAnno
    annotated_vcf = tempfile.NamedTemporaryFile(
        dir=os.path.curdir, prefix="tmp_mtdna_vcfanno_", suffix=".vcf", delete=delete_temp
    )
    mt_vcfanno_config = tempfile.NamedTemporaryFile(
        dir=os.path.curdir, prefix="vcfanno_config_mito", suffix=".toml", delete=delete_temp
    )
    mt_vcfanno_config_dict = vcfanno_config_parser.run(
        vcf_parser.sequence_field_name,
        toml.load(args.vcfanno_config),
        args.data_dir,
        args.ignore_list
    )
    with open(mt_vcfanno_config.name, "w") as tfh:
        toml.dump(mt_vcfanno_config_dict, tfh)
    with open(annotated_vcf.name, "w") as ofh:
        run_env = os.environ.copy()
        run_env["GOGC"] = "1000"
        run_env["IRELATE_MAX_GAP"] = "1000"
        logger.info("Running VCFAnno")
        cmdh = subprocess.run(
            [
                "vcfanno",
                "-p", str(args.nprocs),
                "-base-path", args.data_dir,
                mt_vcfanno_config.name,
                normalized_vcf.name
            ],
            env=run_env,
            stderr=subprocess.PIPE,
            stdout=ofh
        )
        if cmdh.returncode != 0:
            raise RuntimeError(f"VCFAnno exited prematurely: {cmdh.stderr.decode()}")

    # VEP
    vcf_check = subprocess.run(
        ["grep", "-cv", "^#", annotated_vcf.name], stdout=subprocess.PIPE
    )
    n_records = int(vcf_check.stdout)
    if n_records == 0:
        logger.info("Skipping VEP for empty VCF")
        final_vcf = annotated_vcf
    else:
        final_vcf = tempfile.NamedTemporaryFile(
            dir=os.path.curdir, prefix="tmp_mtdna_vep_", suffix=".vcf", delete=delete_temp
        )
        vep_cmd = [
            "vep_offline",
            "-i", annotated_vcf.name,
            "-o", final_vcf.name,
            "--allele_number",
            "--allow_non_variant",
            "--biotype",
            "--buffer_size=5000",
            "--canonical",
            "--distance=0",
            "--domains",
            "--exclude_predicted",
            "--failed=1",
            "--fasta={ref}".format(ref=args.mtdna_ref),
            "--force_overwrite",
            "--fork=12",
            "--hgvs",
            "--hgvsg",
            "--no_escape",
            "--no_stats",
            "--numbers",
            "--pick",
            "--polyphen=b",
            "--protein",
            "--pubmed",
            "--merged",
            "--regulatory",
            "--sift=b",
            "--symbol",
            "--use_given_ref",
            "--vcf",
        ]
        if args.custom_opts:
            vep_cmd.append("--custom={opts}".format(opts=args.custom_opts))
        logger.info(f"Running VEP for {n_records} variants")
        cmdh = subprocess.run(vep_cmd, stderr=subprocess.PIPE)
        if cmdh.returncode != 0:
            raise RuntimeError(f"VEP exited prematurely: {cmdh.stderr.decode()}")

    # Parse VCF
    logger.info("Parsing VCF")
    mother_haplogroup, proband_haplogroup, table = (
        vcf_parser.run(final_vcf.name, analysis_info, logger)
    )

    # Print transposed table
    logger.info("Writing table")
    with open(args.output, "w") as ofh, open(args.warnings, "w") as wfh:
        info_column = table.pop(0)
        if table:
            for key, name in info_column.items():
                bare_record = [f"**{name}**"] + [record[key].replace("'", "") for record in table]
                ofh.write("\t".join(bare_record) + "\n")
        if mother_haplogroup and proband_haplogroup and mother_haplogroup != proband_haplogroup:
            wfh.write(
                "The mother's mitochondrial haplotype differs from the proband's: "
                + mother_haplogroup + " <> " + proband_haplogroup
            )

    # Print haplogroup
    print(f"**Haplogroup** {proband_haplogroup or 'n.a.'}\n\n")

    decomposed_vcf.close()
    normalized_vcf.close()
    mt_vcfanno_config.close()
    annotated_vcf.close()
    final_vcf.close()


def main(argv=None):
    argv = argv or sys.argv[1:]
    args = parse_command_line_args(argv)

    loglevel = logging.INFO
    if args.verbose:
        loglevel = logging.DEBUG

    logging.basicConfig(level=loglevel)

    if args.logfile:
        fh = logging.FileHandler(args.logfile)
        logger.addHandler(fh)

    logger.info("Fetching analysis information")
    analysis_info = get_analysis_info(args.analysis_file)

    logger.info(f"Writing report for '{analysis_info.analysis_name}'")
    write_report(analysis_info, args)


if __name__ == "__main__":
    sys.exit(main())
