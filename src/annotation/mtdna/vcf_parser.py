"""
VCF parser module
The `run` function processes the original mtDNA VCF file, filters the variants and returns a table
of candidate variants for interpretation.

Note: the VCF is expected to be decomposed and normalized
"""

import logging

from vcfiterator import VcfIterator
from vcfiterator.processors import VEPInfoProcessor


class VCFParserException(Exception):
    pass

sequence_field_name = "CHROM"

# Base VCF fields
base_fields = {"pos": "POS", "ref": "REF", "alt": "ALT", "filter": "FILTER"}
# ClinVar field
clinvar_fields = {
    "clinical_significance": "CLINVAR_CLINSIG"
}
# gnomAD-MT fields
gnomad_fields = {
    "gnomad_ac_het": "GNOMAD_MT__AC_Het",
    "gnomad_ac_hom": "GNOMAD_MT__AC_Hom",
    "gnomad_af_het": "GNOMAD_MT__AF_Het",
    "gnomad_af_hom": "GNOMAD_MT__AF_Hom",
    "gnomad_an": "GNOMAD_MT__AN",
}
# HelixMTdb fields
helix_mtdb_fields = {
    "helixmtdb_af_het": "HELIXMTDB__AF_Het",
    "helixmtdb_af_hom": "HELIXMTDB__AF_Hom",
    "helixmtdb_het": "HELIXMTDB__Het",
    "helixmtdb_hom": "HELIXMTDB__Hom",
}
# In-house database fields
indb_fields = {
    "indb_ousmito_het": "inDB_OUSMito__Het",
    "indb_ousmito_hom": "inDB_OUSMito__Hom",
}
# Mitomap confirmed pathological mutations
mitomapcpm_fields = {
    "associated_diseases": "MITOMAPCPM__Associated_diseases"
}
# MitoTIP fields
mitotip_fields = {
    "score": "MITOTIP_Score",
    "quartile": "MITOTIP_Quartile",
    "status": "MITOTIP_MITOMAP_Status",
}
# Mitochondrial haplogroup field
mito_haplogroup_fields = {
    "haplogroup_marker": "haplogroup.HAPLOGROUP_MARKER"
}
# Mother genotype fields
mother_fields = {
    "ad_mother": "AD_Mother",
    "af_mother": "AF_Mother",
    "dp_mother": "DP_Mother",
}
# Proband genotype fields
proband_fields = {
    "ad_proband": "AD_Proband",
    "af_proband": "AF_Proband",
    "dp_proband": "DP_Proband",
}
# VEP fields (only used by VcfIterator)
vep_fields = {
    "csq": "Consequence",
    "symbol": "SYMBOL",
    "hgvsc": "HGVSc",
    "hgvsp": "HGVSp",
    "hgvsg": "HGVSg",
}

all_fields = dict()
all_fields.update(base_fields)
all_fields.update(mito_haplogroup_fields)
all_fields.update(mitomapcpm_fields)
all_fields.update(indb_fields)
all_fields.update(clinvar_fields)
all_fields.update({k: f"VEP_{v}" for k, v in vep_fields.items()})
all_fields.update(proband_fields)
all_fields.update(mother_fields)
all_fields.update(helix_mtdb_fields)
all_fields.update(gnomad_fields)
all_fields.update(mitotip_fields)

output_fields = dict(
    (k, v) for k, v in all_fields.items()
    if k not in indb_fields.keys()
    and k not in mito_haplogroup_fields.keys()
    and k not in mitotip_fields.keys()
    and k != "helixmtdb_af_het"
    and k != "helixmtdb_af_hom"
    and k != "gnomad_af_het"
    and k != "gnomad_af_hom"
    and k != "gnomad_an"
)


def get_haplogroup(haplogrep_results_file):
    with open(haplogrep_results_file) as fh:
        lines = fh.read().splitlines()
        haplogroup = lines[1].split("\t")[1].replace('"', '')
        return haplogroup


def run(input_vcf, analysis_info, logger = logging.getLogger()):
    logger.debug(f"Parsing '{input_vcf}'")

    vi = VcfIterator(input_vcf)
    vi.addInfoProcessor(VEPInfoProcessor)

    mother_haplogroup = None
    proband_haplogroup = None
    proband_haplogroup_votes = dict()

    table = list()

    table.append(output_fields)

    # Iterate over VCF records
    for row in vi.iter():
        variant_info = dict()

        logger.debug(f"Processing row {row}")

        # Extract general information valid for the whole variant record (leave allele consequences)
        general_info = row["INFO"].pop("ALL")
        if logger.getEffectiveLevel() == logging.DEBUG:
            logger.debug("General variant info:")
            for k, v in general_info.items():
                logger.debug(f"  '{k}' [{type(k)}] = {v}")

        # Since the VCF is decomposed both the "ALT" field and what's left of `row["INFO"]` should
        # contain a single item..
        assert len(row["ALT"]) == 1 and len(row["INFO"]) == 1, (
            f"Multiple alternative alleles found in record '{row}' of '{input_vcf}':"
            " Please, make sure that every record contains a single alternative allele"
        )
        allele, allele_info = row["INFO"].popitem()
        record_allele = row["ALT"] = row["ALT"].pop()
        assert allele == record_allele, (
            f"The alternative allele in the INFO field [{allele}] does not match the one in the "
            f"base VCF record [{record_allele}]"
        )

        # Record any given haplogroup for the proband
        record_haplogroup = general_info.get("haplogroup.HAPLOGROUP")
        if record_haplogroup:
            if isinstance(record_haplogroup, str):
                record_haplogroup = [record_haplogroup]
            for h in record_haplogroup:
                proband_haplogroup_votes[str(h)] = (proband_haplogroup_votes.get(str(h), 0) + 1)

        # Add base entries and `general_info` field entries
        for field_key, field_name in all_fields.items():
            if field_key in base_fields.keys():
                variant_info[field_key] = str(row.get(field_name, ""))
            else:
                # NOTE: VEP_<name> fields will only be initialized to empty strings here:
                #       they will be filled in when parsing allele consequences below
                variant_info[field_key] = str(general_info.get(field_name, ""))

        # Initialize synonymous variant flag
        is_synonymous = False

        # Add allele consequences
        allele_csq_arr = allele_info["CSQ"]
        for vep_key, vep_name in vep_fields.items():
            csq_item_arr = list()
            for allele_csq in allele_csq_arr:
                csq_item = allele_csq.get(vep_name)
                if not csq_item:
                    continue
                if vep_name == "Consequence":
                    is_synonymous = is_synonymous or "synonymous_variant" in csq_item
                csq_item_str = str(csq_item)
                if isinstance(csq_item, list):
                    csq_item_str = ", ".join(str(entry) for entry in csq_item)
                csq_item_arr.append(csq_item_str)
            csq_str = "/".join(csq_item_arr)
            if not csq_str:
                continue
            variant_info[vep_key] = csq_str

        # Proband data
        proband_info = dict()
        try:
            proband_info = next(
                info for id, info in row["SAMPLES"].items()
                if analysis_info.sample_id in id
            )
        except StopIteration:
            logger.error(f"No sample in '{input_vcf}' matches the proband's ID")
            raise VCFParserException
        
        variant_info["ad_proband"] = f"'{proband_info.get('AD')}'"
        variant_info["af_proband"] = str(proband_info.get('AF'))
        variant_info["dp_proband"] = str(proband_info.get('DP'))
        
        # Skip variant if not present in the proband
        if "1" not in str(proband_info.get('GT')):
            continue

        # Mother data
        if analysis_info.mother_sample_id:
            mother_info = dict()
            try:
                mother_info = next(
                    info for id, info in row["SAMPLES"].items()
                    if analysis_info.mother_sample_id in id
                )
            except StopIteration:
                logger.error(f"No sample in '{input_vcf}' matches the mother's ID")
                raise VCFParserException
            
            variant_info["ad_mother"] = f"'{mother_info.get('AD')}'"
            variant_info["af_mother"] = str(mother_info.get('AF'))
            variant_info["dp_mother"] = str(mother_info.get('DP'))

        # Assemble output information
        output_info = dict((k, v) for k, v in variant_info.items() if k in output_fields)

        # Apply filter chain

        if variant_info["associated_diseases"]:
            # Include variant if it is known to be pathological
            logger.info(f"Adding {variant_info} to table")
            table.append(output_info)
            continue

        # Discard if synonymous
        if is_synonymous:
            logger.info(f"Discarding synonymous variant {variant_info}")
            continue

        # skip haplogroup marker
        if variant_info.get("haplogroup_marker"):
            logger.info(f"Discarding haplogroup marker {variant_info}")
            continue
            
        # if the allele ratio is smaller than 0.1, filter out
        try:
            af = variant_info.get("af_proband", 1.0)
            if float(af) < 0.1:
                logger.info(f"Discarding {variant_info} due to allele ratio [<0.1]")
                continue
        except ValueError as e:
            logger.warning(
                f"{all_fields['af_proband']} ['{af}'] can't be cast to a float to filter on"
            )
            logger.debug(str(general_info))
        
        # if HelixMTdb's allele frequency is larger than 0.001, filter out
        try:
            af = variant_info.get("helixmtdb_af_het", 0.0)
            if float(af) > 0.001:
                logger.info(
                    f"Discarding {variant_info} due to heterozygous allele freq. [HelixMTdb >0.001]"
                )
                continue
        except ValueError as e:
            logger.warning(
                f"{all_fields['helixmtdb_af_het']} ['{af}'] can't be cast to a float to filter on"
            )
            logger.debug(str(general_info))

        try:
            af = variant_info.get("helixmtdb_af_hom", 0.0)
            if float(af) > 0.001:
                logger.info(
                    f"Discarding {variant_info} due to homozygous allele freq. [HelixMTdb >0.001]"
                )
                continue
        except ValueError as e:
            logger.warning(
                f"{all_fields['helixmtdb_af_hom']} ['{af}'] can't be cast to a float to filter on"
            )
            logger.debug(str(general_info))

        # if gnomAD's allele frequency is larger than 0.001, filter out
        try:
            af = variant_info.get("gnomad_af_het", 0.0)
            if float(af) > 0.001:
                logger.info(
                    f"Discarding {variant_info} due to heterozygous allele freq. [gnomAD >0.001]"
                )
                continue
        except ValueError as e:
            logger.warning(
                f"{all_fields['gnomad_af_het']} ['{af}'] can't be cast to a float to filter on"
            )
            logger.debug(str(general_info))

        try:
            af = variant_info.get("gnomad_af_hom", 0.0)
            if float(af) > 0.001:
                logger.info(
                    f"Discarding {variant_info} due to homozygous allele freq. [gnomAD >0.001]"
                )
                continue
        except ValueError as e:
            logger.warning(
                f"{all_fields['gnomad_af_hom']} ['{af}'] can't be cast to a float to filter on"
            )
            logger.debug(str(general_info))

        # If the execution reaches this point, no filters were applied
        logger.info(f"Adding {variant_info} to table")
        table.append(output_info)

    # Verify haplogroups

    if analysis_info.mother_mtdna_haplo:
        mother_haplogroup = get_haplogroup(analysis_info.mother_mtdna_haplo)
        logger.info(f"Mother's HaploGrep record: {mother_haplogroup}")

    if analysis_info.mtdna_haplo:
        proband_haplogroup = get_haplogroup(analysis_info.mtdna_haplo)
        logger.info(f"Proband's HaploGrep record: {proband_haplogroup}")

    n_groups = len(proband_haplogroup_votes)

    if n_groups > 0:
        if n_groups > 1:
            logger.warning(
                f"The proband is assigned to {n_groups} different haplogroups: using most frequent"
            )
        proband_haplogroup = max(proband_haplogroup_votes, key=proband_haplogroup_votes.get)

    if mother_haplogroup:
        if not proband_haplogroup:
            proband_haplogroup = mother_haplogroup
        if proband_haplogroup != mother_haplogroup:
            logger.warning(
                "The mother's mitochondrial haplotype differs from the proband's: "
                + mother_haplogroup + " <> " + proband_haplogroup
            )

    return (mother_haplogroup, proband_haplogroup, table)