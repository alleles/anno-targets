from pydantic import BaseModel
from typing import Optional


class Analysis(BaseModel):
    analysis_name: str
    analysis_type: str
    annotate_cnv: bool
    bam: str
    bigwig: Optional[str]
    capture_kit: str
    cnv_vcf: str
    convading_log: Optional[str]
    convading_long_list: Optional[str]
    convading_short_list: Optional[str]
    convading_total_list: Optional[str]
    date_requested: str
    exon_regions: str
    father_bam: Optional[str]
    father_bigwig: Optional[str]
    father_capture_kit: Optional[str]
    father_cnv_vcf: Optional[str]
    father_mtdna_haplo: Optional[str]
    father_qc_report: Optional[str]
    father_qc_warnings: Optional[str]
    father_sample_id: Optional[str]
    father_sequencer_id: Optional[str]
    gene_panel_name: str
    gene_panel_version: str
    mother_bam: Optional[str]
    mother_bigwig: Optional[str]
    mother_capture_kit: Optional[str]
    mother_cnv_vcf: Optional[str]
    mother_mtdna_haplo: Optional[str]
    mother_qc_report: Optional[str]
    mother_qc_warnings: Optional[str]
    mother_sample_id: Optional[str]
    mother_sequencer_id: Optional[str]
    mtdna_haplo: str
    mtdna_vcf: str
    phenotypes_file: str
    pipeline_qc_report: Optional[str]
    pipeline_qc_warnings: Optional[str]
    priority: int
    report_config: str
    sample_id: str
    sequencer_id: str
    sex: Optional[str]
    specialized_pipeline: str
    transcripts_file: str
    vcf: str
    wes_cnv_inhouse_db: Optional[str]
    wgs_cnv_inhouse_db_dir: Optional[str]


def from_env():
    from os import getenv
    
    return Analysis(
       analysis_name = getenv("ANALYSIS_NAME"),
       analysis_type = getenv("ANALYSIS_REQUESTED_DATE"),
       annotate_cnv = getenv("ANNOTATION_CNV") == "True",
       bam = getenv("BAM"),
       bigwig = getenv("BIGWIG"),
       capture_kit = getenv("CAPTUREKIT"),
       cnv_vcf = getenv("CNV_VCF"),
       convading_log = getenv("CONVADING_LOG"),
       convading_long_list = getenv("CONVADING_LONGLIST"),
       convading_short_list = getenv("CONVADING_SHORTLIST"),
       convading_total_list = getenv("CONVADING_TOTALLIST"),
       date_requested = getenv("ANALYSIS_REQUESTED_DATE"),
       exon_regions = getenv("EXON_REGIONS"),
       father_bam = getenv("FATHER_BAM"),
       father_bigwig = getenv("FATHER_BIGWIG"),
       father_capture_kit = getenv("FATHER_CAPTURE_KIT"),
       father_cnv_vcf = getenv("FATHER_CNV_VCF"),
       father_mtdna_haplo = getenv("FATHER_MTDNA_HAPLO"),
       father_qc_report = getenv("FATHER_QC_REPORT"),
       father_qc_warnings = getenv("FATHER_QC_WARNINGS"),
       father_sample_id = getenv("FATHER_SAMPLE_ID"),
       father_sequencer_id = getenv("FATHER_SEQUENCER_ID"),
       gene_panel_name = getenv("GP_NAME"),
       gene_panel_version = getenv("GP_VERSION"),
       mother_bam = getenv("MOTHER_BAM"),
       mother_bigwig = getenv("MOTHER_BIGWIG"),
       mother_capture_kit = getenv("MOTHER_CAPTURE_KIT"),
       mother_cnv_vcf = getenv("MOTHER_CNV_VCF"),
       mother_mtdna_haplo = getenv("MOTHER_MTDNA_HAPLO"),
       mother_qc_report = getenv("MOTHER_QC_REPORT"),
       mother_qc_warnings = getenv("MOTHER_QC_WARNINGS"),
       mother_sample_id = getenv("MOTHER_SAMPLE_ID"),
       mother_sequencer_id = getenv("MOTHER_SEQUENCER_ID"),
       mtdna_haplo = getenv("MTDNA_HAPLO"),
       mtdna_vcf = getenv("MTDNA_VCF"),
       phenotypes_file = getenv("PHENOTYPES"),
       pipeline_qc_report = getenv("PIPELINE_QC_REPORT"),
       pipeline_qc_warnings = getenv("PIPELINE_QC_WARNINGS"),
       priority = int(getenv("PRIORITY")),
       report_config = getenv("REPORT_CONFIG"),
       sample_id = getenv("SAMPLE_ID"),
       sequencer_id = getenv("SEQUENCER_ID"),
       sex = getenv("GENDER"),
       specialized_pipeline = getenv("SPECIALIZED_PIPELINE"),
       transcripts_file = getenv("TRANSCRIPTS"),
       vcf = getenv("VCF"),
       wes_cnv_inhouse_db = getenv("WES_CNV_INHOUSE_DB"),
       wgs_cnv_inhouse_db_dir = getenv("WGS_CNV_INHOUSE_DB_DIR"),
    )
