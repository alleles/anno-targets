import sys
import os
import json
import argparse

from utils import CoverageData


def get_failed(low_coverage, low_coverage_transcripts, depth_threshold):

    if not low_coverage or not os.path.exists(low_coverage):
        raise RuntimeError("Missing input --low_coverage or file does not exist.")

    if not low_coverage_transcripts or not os.path.exists(low_coverage_transcripts):
        raise RuntimeError("Missing input --low_coverage_transcripts or file does not exist.")

    assert depth_threshold >= 0

    cdata = CoverageData(low_coverage_transcripts)
    lc_rows = cdata.loadLowCoverage(low_coverage)

    # Organize by position, as there can be duplicate rows due to multiple
    # transcripts for a single gene. These would be repeated twice,
    # with only the transcript differing
    depth = dict()  # {(pos, start, end): depth}
    for row in lc_rows:
        key = (row['chromosome'], row['start'], row['end'])
        transcript_data = {
            'gene': row['gene'],
            'transcript': row['transcript'],
            'exon': row['exon']
        }
        if key not in depth:
            depth[key] = {
                'depth': int(row['reads']),
                'transcripts': [
                    transcript_data
                ]
            }
        else:
            depth[key]['transcripts'].append(transcript_data)

    # Check threshold
    failed = list()
    for key, value in depth.items():
        if value['depth'] <= depth_threshold:
            failed.append({
                'chr': key[0],
                'start': key[1],
                'stop': key[2],
                'transcripts': value['transcripts'],
                'depth': value['depth']
            })

    return failed


def check_failed(failed, failed_regions_threshold):

    # Pass only if number of failed regions is less or equal to threshold
    return len(failed) <= failed_regions_threshold


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Check number of low quality regions.")
    parser.add_argument("--output", help="Path to output JSON file with results.", dest='output')
    parser.add_argument("--depth-threshold", type=int, help="Threshold for required depth", dest='depth_threshold')
    parser.add_argument("--failed-regions-threshold", type=int, help="Threshold for number of regions with low coverage", dest='failed_regions_threshold')
    parser.add_argument("--low-coverage", help="Path to lowCoverage file. Required for type 'target''", dest='low_coverage')
    parser.add_argument("--low-coverage-transcripts", help="Path to transcripts used for lowCoverage file. Required for type 'target''", dest='low_coverage_transcripts')
    args = parser.parse_args()

    failed = get_failed(
        args.low_coverage,
        args.low_coverage_transcripts,
        args.depth_threshold
    )

    passed = check_failed(failed, args.failed_regions_threshold)

    with open(args.output, 'w') as f:
        json.dump({'failed_regions': failed}, f)

    if passed:
        print("PASSED: Found {} failed regions. Threshold is {}.".format(len(failed), args.failed_regions_threshold))
    else:
        sys.exit("FAILED: Found {} failed regions. Threshold is {}.".format(len(failed), args.failed_regions_threshold))

