#!/usr/bin/env python3
# -*- coding: iso-8859-1 -*-

"""
Main script for coverage quality control.
Requires bedtools2.20.1 or higher.

Calculates the percentage of coverageRegions (exons and aggregated to transcripts)
that are equal to or above a minimum coverage threshold,
and also the regions that are below the threshold.
Takes a single BAM file as input,
or a file that contains one path at each line.

Uses GATK DepthOfCoverage to get read-filtered coveragePerBasepair.
Converts this to a BED file that is split into two where
one file has those above the threshold and another the positions that
have coverage lower than the threshold value.
Counts how many matches from the above-threshold file
that there are to each coverageRegion.
Finally, merges the continuous regions that are below the coverage threshold
and outputs lowCoverageRegions.bed.

"""

import os
import sys
import argparse
import tempfile
from collections import OrderedDict

from pybedtools import BedTool


# gene coverage aggregation levels
aggregation={1: "gene", 2: "transcript", 3: "exon"}


def run_coverage_check(coveragePerBP, coverageRegions, outputDir, sampleName, minimumCoverage):

    # Split perBPCoverage into above/below threshold
    aboveThresholdFd, aboveThresholdPath = tempfile.mkstemp(".bed", sampleName + "_above")
    belowThresholdFd, belowThresholdPath = tempfile.mkstemp(".bed", sampleName + "_below")
    os.close(aboveThresholdFd)
    os.close(belowThresholdFd)
    with open(coveragePerBP) as f:
        with open(aboveThresholdPath, 'w') as g, open(belowThresholdPath, 'w') as h:
            split_bed_on_score(f, g, h, minimumCoverage)

    covRegionsBED = BedTool(coverageRegions)
    aboveBED = BedTool(aboveThresholdPath)
    covRegionsCountAboveBED = covRegionsBED.intersect(aboveBED, c=True) # Keeps all covRegions
    make_aggregated_report(covRegionsCountAboveBED, outputDir, 3, sampleName) # 3 = exon
    make_aggregated_report(covRegionsCountAboveBED, outputDir, 2, sampleName) # 2 = transcript

    belowBED = BedTool(belowThresholdPath)
    make_low_coverage_regions_report(belowBED, covRegionsBED, outputDir, sampleName)

    for tmpFile in (aboveThresholdPath, belowThresholdPath):
        os.remove(tmpFile)


def split_bed_on_score(inFile, aboveFile, belowFile, minPass):
    """Writes BED records with score >= minPass to aboveFile, else belowFile"""
    for line in inFile:
        if line.strip().isspace(): continue
        if int(line.split('\t')[4]) >= minPass:
            aboveFile.write(line)
        else:
            belowFile.write(line)


def make_aggregated_report(covRegionsCountAboveBED, outputdir, level=3, sampleName=""):
    """
    Writes a coverageReport at an aggregated level

    Assumes all regions at the lowest level of aggregation are given in the BED object and outputs
    them in the same order. NOTE: the `count` member of the `Interval` objects yielded by iterating
    over `BedTool` objects isn't clearly documented at the time of writing but seems to hold an
    integer coercion of whatever is in the "last" field of the corresponding `BedTool` record.
    """

    outputFilePath = os.path.join(
      outputdir, "{}_coverageReport_{}".format(sampleName, aggregation.get(level, "gene"))
    )

    # Find all regionBED's overlaps with aboveBED and add their widths up
    # NOTE: the assumption here is, reasonably enough, that the latter are _disjoint_ intervals.
    #       regions with 0 overlaps are also reported (see BEDTools '-wao' command line flag).
    with open(outputFilePath, "w") as fh:
        fh.write("#sample name\t" + "\t".join((
          aggregation.get(a) for a in range(1, level+1)
        )))
        fh.write("\tfraction covered\tnucleotides covered\tnucleotides in total\n")
        covered_lengths = OrderedDict()
        total_lengths = OrderedDict()

        for record in covRegionsCountAboveBED:
            # transcript regions are named (fourth BED file field) 'gene[__hgnc]__transcript__exon'
            if "__" in record.name:
                name = "__".join(record.name.rsplit("__", len(aggregation)-1)[:level])
            # custom regions are named (fourth BED file field) arbitrarily (with no '__')
            else:
                name = record.name

            covered_lengths[name] = covered_lengths.get(name, 0) + record.count
            total_lengths[name] = total_lengths.get(name, 0) + record.length

        names = set()
        txtLines = dict()
        for name in covered_lengths:
            if name in names:
                continue  # Aggregate already output
            try:
                f = 100. * (covered_lengths[name] / (total_lengths[name] * 1.0))
            except ZeroDivisionError as e:
                f = 0.0
            names.add(name)
            nameParts = name.rsplit("__", level-1)
            if len(nameParts) < level:
                nameParts += ["N/A"] * (level - len(nameParts))
            txtLines["\t".join(
              [sampleName] + nameParts + \
              ["%.1f%%\t%d\t%d\n" % (f, covered_lengths[name], total_lengths[name])]
            )] = f

        fh.writelines(sorted(txtLines, key=lambda k: txtLines.get(k, 0)))



def make_low_coverage_regions_report(belowBED, covRegionsBED, outputDir, sampleName=""):
    """Writes a BED6+3 with any continuous regions below threshold"""
    if belowBED.count() > 0:
        belowMergedBED = belowBED.merge(c=5, o="min")
        # Get the names of the regions, then remove the extra columns.
        belowMergedWithNames = belowMergedBED.intersect(covRegionsBED, loj=True, wa=True, wb=True)
    else:
        belowMergedWithNames = ()  # Empty tuple, to create an empty output file
    outputFilePath = os.path.join(outputDir, "{}_lowCoverage.bed".format(sampleName))
    with open(outputFilePath, 'w') as output:
        for i in belowMergedWithNames:
            dunder = "__"
            if dunder in i[7]:
                gene, transcript, exon = i[7].rsplit(dunder, len(aggregation)-1)
            else:
                gene, transcript, exon = i[7], "N/A", "N/A" # Custom region
            gene = gene.split("__")[0]  # strip eventual HGNC ID from gene
            output.write("{pos}\t{name}\t{score}\t+\t{sampleName}\t{gte}\n".format(
              pos='\t'.join(i[0:3]), name=i[7], score=i[3], sampleName=sampleName,
              gte='\t'.join((gene, transcript, exon))
            ))


def main(argv=None):
    argv = argv or sys.argv[1:]
    parser = argparse.ArgumentParser(description="""
    Outputs for each region the percentage that is above the minimum coverage threshold,
    and which continuous regions that are below the threshold.""")

    parser.add_argument("--coveragePerBP", dest="coveragePerBP", required=True,
                        help="Path to coverage per bp bed file")
    parser.add_argument("--regions", dest="coverageRegions", required=True,
                        help="Full path to coverageRegions.bed, the file that "
                             "specifies the regions to calculate coverage on")
    parser.add_argument("--outputdir", dest="outputDir", required=False,
                        default='.', help="Path to output directory")
    parser.add_argument("--samplename", dest="sampleName", required=False,
                        default="nosamplename", help="Name of sample")
    parser.add_argument("--minimumcoverage", dest="minimumCoverage", required=False, type=int,
                        default=10, help="Minimum acceptable coverage (no of reads)")
    args = parser.parse_args(argv)

    run_coverage_check(args.coveragePerBP, args.coverageRegions, args.outputDir, args.sampleName, args.minimumCoverage)

    return 0


if __name__ == "__main__":
    sys.exit(main())
