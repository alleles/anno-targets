#!/bin/bash

su root -c "mysqld_safe --skip-grant-tables --nowatch"
exec "$@"
