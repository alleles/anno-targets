SHELL = /bin/bash

_IGNORE_VARS :=

MAKEFILE_DIR := $(dir $(abspath $(firstword $(MAKEFILE_LIST))))
# local names for the Makefiles pulled out of $ELLA_ANNO_SLUG
MAKEFILE_CORE := Makefile.core
MAKEFILE_CORE_PATH := $(MAKEFILE_DIR)/Makefile.core
MAKEFILE_UTILS := Makefile.utils
MAKEFILE_UTILS_PATH := $(MAKEFILE_DIR)/Makefile.utils

NULL_STRING :=
BLANKSPACE = $(NULL_STRING) # this is how we get a single space
_IGNORE_VARS += NULL_STRING BLANKSPACE
# check the make version for compatibility. if using < v4, print a warning and sleep for a few
# to make sure the user sees it. MAKE_VERSION is a built-in variable that's transformed from
# 'X.Y.Z' to 'X Y Z', and the first "word"/digit is checked.
ifneq ($(word 1,$(subst .,$(BLANKSPACE),$(MAKE_VERSION))),4)
$(warning Using $(MAKE_VERSION), but least v4.* is recommended. Rules may not work as expected!)
$(shell sleep 3)
endif

#----
# Variables describing current repo state, for container tags/labels/names
#----
ifneq ($(CI_COMMIT_REF_NAME),)
# CI provides branch/tag info, since it runs in a detached state and is otherwise unavailable.
BRANCH = $(CI_COMMIT_REF_NAME)
else
BRANCH = $(shell git rev-parse --abbrev-ref HEAD)
endif
COMMIT_HASH = $(shell git rev-parse --short=8 HEAD)
COMMIT_DATE = $(shell git log -1 --pretty=format:'%cI')
# sed is important to strip out any creds (i.e., CI token) from repo URL
REPO_URL = $(shell git remote get-url origin | sed -e 's/.*\?@//; s/:/\//g')

#----
# Variables for running anno-targets locally 
#----
# if not running CI, use tty/interactive
ifeq ($(CI),)
TERM_OPTS := -it
endif

# data directories to mount to Docker/Singularity images
ANNO_DATA ?= $(PWD)/anno-data
ANNO_RAWDATA ?= $(PWD)/anno-rawdata

# Default values for using make dev. NOTE: should anything actually be in here?
TARGETS_OUT ?= $(PWD)/anno-targets-out
SAMPLE_REPO ?= $(PWD)/sample-repo
SENSITIVE_DB ?= $(PWD)/sensitive-db
# directories that should exist and be user owned for `make dev`
DEV_DIRS = TARGETS_OUT SAMPLE_REPO SENSITIVE_DB
# exposed port in Docker container during make dev
API_PORT = 6000-6100

# set any custom args to `docker run` BEFORE generate / download / upload data steps
ANNOBUILDER_OPTS ?=

# set any custom args to sync_data.py DURING generate / download / upload data steps
RUN_CMD_ARGS ?=

# set any custom `docker run` args for the `make dev` rule
ANNO_OPTS ?=

# Configs for download/generate/upload steps
DATASETS := /anno/ops/amg-anno-datasets.json
DATASETS_OPT := -f $(DATASETS)

# Variables for data spaces configuration for the ella-anno and amg-anno buckets
SPACES_CONFIG_ELLA_ANNO ?=
SPACES_CONFIG_ANNO_TARGETS ?= $(PWD)/ops/spaces_config.json

# used to ensure files created / modified during data steps are owned by the correct user
UID_GID = $(shell id -u):$(shell id -g)

# allow user override if /tmp partition is too small
SINGULARITY_TMPDIR ?= /tmp

# from: https://gitlab.com/gitlab-org/cloud-deploy/-/releases/v0.3.6
AWSCLI_IMAGE = registry.gitlab.com/gitlab-org/cloud-deploy/aws-base:v0.3.6@sha256:5663ebd4017341b7c34381b046847ce69e767ca3acc3b8dfdb6b2208db6cec74
AWS_CONFIG_FILE ?= $(HOME)/.aws/credentials

#----
# Variables for Building and naming Docker and Singularity images
#----
# The ella-anno Docker images to build on top of, generally a specific release tag e.g., v1.2.3
#   available tags: https://gitlab.com/alleles/ella-anno/container_registry/916130

# Extract latest version tag (tag starting with 'v').
_ELLA_ANNO_API_TAGS_URL := https://gitlab.com/api/v4/projects/alleles%2Fella-anno/repository/tags/?search=^v
# GitLab API orders tags by date as default, so the first one is the latest.
ELLA_ANNO_TAG ?= $(shell curl -s "$(_ELLA_ANNO_API_TAGS_URL)" | grep -oP "(?<=\"name\":\")v[^\"]*" | head -n 1)
ELLA_ANNO_BASE_IMAGE ?= registry.gitlab.com/alleles/ella-anno
ELLA_ANNO_SLUG ?= $(ELLA_ANNO_BASE_IMAGE):$(ELLA_ANNO_TAG)
ELLA_ANNOBUILDER_SLUG ?= $(ELLA_ANNO_BASE_IMAGE):builder-$(ELLA_ANNO_TAG)

# Target-specific args for docker build
override BUILD_OPTS += --build-arg BUILDKIT_INLINE_CACHE=1 \
                       --build-arg ELLA_ANNO_IMAGE_NAME=$(ELLA_ANNO_SLUG) \
                       --build-arg ELLA_ANNOBUILDER_IMAGE_NAME=$(ELLA_ANNOBUILDER_SLUG)

# Set variable to use registry.gitlab.com/... images instead of local/...
USE_REGISTRY ?=

# PULL_NOT_BUILD implies/requires USE_REGISTRY
ifneq ($(PULL_NOT_BUILD),)
override USE_REGISTRY = 1
endif

ifeq ($(USE_REGISTRY),)
BASE_IMAGE = local/anno-targets
else
BASE_IMAGE = registry.gitlab.com/alleles/anno-targets
endif

# Use release/annotated git tag if available, otherwise branch name
RELEASE_TAG ?= $(shell git tag -l --points-at HEAD)
ifeq ($(RELEASE_TAG),)
RELEASE_TAG = $(BRANCH)
else
ifneq ($(words $(RELEASE_TAG)),1)
# if multiple tags select first v% (i.e., ignore v*-rc* if non-rc available)
RELEASE_TAG := $(firstword $(filter v%, $(RELEASE_TAG)))
endif
endif
PROD_TAG = $(RELEASE_TAG)
BUILDER_TAG = builder-$(RELEASE_TAG)

# put it all together
IMAGE_NAME ?= $(BASE_IMAGE):$(PROD_TAG)
CONTAINER_NAME ?= anno-targets-$(PROD_TAG)-$(USER)
ANNOBUILDER_IMAGE_NAME ?= $(BASE_IMAGE):$(BUILDER_TAG)
ANNOBUILDER_CONTAINER_NAME ?= anno-targets-builder-$(PROD_TAG)-$(USER)
SINGULARITY_IMAGE_NAME ?= anno-$(PROD_TAG).sif
DO_SINGULARITY_IMAGE_URL = s3://amg-anno/releases/$(RELEASE_TAG)/$(notdir $(SINGULARITY_IMAGE_NAME))

# BuildKit for faster builds
export DOCKER_BUILDKIT = 1

# set up the Docker labels. They should follow the OCI label standard
#   ref: https://github.com/opencontainers/image-spec/blob/main/annotations.md
OCI_BASE_LABEL = org.opencontainers.image
TARGETS_BASE_LABEL = io.ousamg.anno-targets

override BUILD_OPTS += --label $(TARGETS_BASE_LABEL).git.url="$(REPO_URL)" \
	--label $(TARGETS_BASE_LABEL).git.commit_hash="$(COMMIT_HASH)" \
	--label $(TARGETS_BASE_LABEL).git.commit_date="$(COMMIT_DATE)" \
	--label $(TARGETS_BASE_LABEL).git.branch="$(BRANCH)"
ifneq ($(RELEASE_TAG),)
override BUILD_OPTS += --label $(OCI_BASE_LABEL).revision=$(RELEASE_TAG)
endif

# The Makefile(s) from ella-anno are used in several places (e.g. in annobuilder-sync-template).
# The following rules always (see .PHONY target below) copy them from $ELLA_ANNOBUILDER_SLUG and
# name them respectively $MAKEFILE_CORE and $MAKEFILE_UTILS locally.
.PHONY: $(MAKEFILE_CORE) $(MAKEFILE_UTILS) include-utils

$(MAKEFILE_CORE): pull-ella-anno
	@docker run --rm \
		-u "$(UID_GID)" \
		-v $(PWD):/local_repo \
		$(ELLA_ANNO_SLUG) \
		cp /anno/Makefile /local_repo/$(MAKEFILE_CORE)

$(MAKEFILE_UTILS): pull-ella-anno
	@docker run --rm \
		-u "$(UID_GID)" \
		-v $(PWD):/local_repo \
		$(ELLA_ANNO_SLUG) \
		cp /anno/Makefile.utils /local_repo/$(MAKEFILE_UTILS)

include-utils: $(MAKEFILE_UTILS)
	$(eval include $(MAKEFILE_UTILS_PATH))


##---------------------------------------------
## Help / Debugging
##   Other vars: VAR_ORIGIN, FILTER_VARS
##---------------------------------------------
.PHONY: vars local-vars help all

all: help

help: include-utils ## print this help and exit
	$(print-help)

local-vars: include-utils ## print out vars set by command line and in the Makefile
	$(disable-origins)
	$(list-vars)
	@true

vars: include-utils ## print out variables available in the Makefile and the origin of their value
	$(list-vars)
	@true


##---------------------------------------------
## General development
##   Other vars: ANNO_OPTS, IMAGE_NAME, ANNOBUILDER_IMAGE_NAME, CONTAINER_NAME, ANNOBUILDER_CONTAINER_NAME
##               RELEASE_TAG, BUILD_OPTS, ELLA_ANNO_SLUG, ELLA_ANNOBUILDER_SLUG, USE_REGISTRY
##---------------------------------------------
.PHONY: dev build pull-ella-anno build-release pull-ella-annobuilder tar-data update-requirements
.PHONY: _singularity-build singularity-dev singularity-release

dev: include-utils $(DEV_DIRS) ## start $IMAGE_NAME as $CONTAINER_NAME using $API_PORT, $ANNO_OPTS
	$(call check_defined, CONTAINER_NAME IMAGE_NAME)
	docker run -d \
		$(ANNO_OPTS) \
		-u root \
		-e TARGET_DATA=/target_data \
		--restart=always \
		--name $(CONTAINER_NAME) \
		-p $(API_PORT):6000 \
		-v $(ANNO_DATA):/anno/data \
		-v $(TARGETS_OUT):/targets-out \
		-v $(SAMPLE_REPO):/samples \
		-v $(SENSITIVE_DB):/target_data/sensitive-db \
		$(IMAGE_NAME)

shell: ## get a bash shell in $CONTAINER_NAME
	docker exec -it $(CONTAINER_NAME) /bin/bash

# create user owned directories to be used with make dev above
$(DEV_DIRS):
	mkdir -p $($@)

_git-worktree-spec:
	$(eval WORKTREE_SPEC = $(shell git -C $(MAKEFILE_DIR) stash create))
	$(if $(value WORKTREE_SPEC),, $(eval override WORKTREE_SPEC = HEAD))

# NOTE: the ella-anno / anno, annobuilder / ella-annobuilder is confusing
#   should we use "anno" to refer to upstream and "targets" to refer to this repo?
#   the pull vs. build does help a bit too
build: _git-worktree-spec include-utils pull-ella-anno ## \
## build the prod docker $IMAGE_NAME based on $ELLA_ANNO_SLUG
	$(call check_defined, IMAGE_NAME)
	$(eval override BUILD_OPTS += --label $(TARGETS_BASE_LABEL).image.base=$(ELLA_ANNO_SLUG))
	git archive $(WORKTREE_SPEC) \
	    | docker build -t $(IMAGE_NAME) $(BUILD_OPTS) --target prod -

build-annobuilder: _git-worktree-spec include-utils pull-ella-annobuilder ## \
## build the anno-targets builder image named $ANNOBUILDER_IMAGE_NAME
	$(call check_defined, ANNOBUILDER_IMAGE_NAME)
	$(eval override BUILD_OPTS += --label $(TARGETS_BASE_LABEL).image.base=$(ELLA_ANNOBUILDER_SLUG))
	git archive $(WORKTREE_SPEC) \
	    | docker build -t $(ANNOBUILDER_IMAGE_NAME) $(BUILD_OPTS) --target builder .

pull-ella-anno: ## docker pull the upstream image $ELLA_ANNO_SLUG
	docker pull $(ELLA_ANNO_SLUG)

pull-ella-annobuilder: ## docker pull the upstream annobuilder image $ELLA_ANNOBUILDER_SLUG
	docker pull $(ELLA_ANNOBUILDER_SLUG)

# use container registry during CI
.PHONY: _pull_registry_anno _pull_registry_annobuilder
ci-build-docker: include-utils
	$(call check_defined, USE_REGISTRY, Must have USE_REGISTRY enabled to push $(IMAGE_NAME))
	$(MAKE) build BUILD_OPTS="--cache-from=$(IMAGE_NAME) --cache-from=$(ANNOBUILDER_IMAGE_NAME) --cache-from=$(BASE_IMAGE):dev"
	$(MAKE) build-annobuilder BUILD_OPTS="--cache-from=$(ANNOBUILDER_IMAGE_NAME) --cache-from=$(IMAGE_NAME) --cache-from=$(BASE_IMAGE):builder-dev"

ci-push-docker:
	docker push $(IMAGE_NAME)
	docker push $(ANNOBUILDER_IMAGE_NAME)

ci-pull-docker:
	docker pull $(IMAGE_NAME)
	docker pull $(ANNOBUILDER_IMAGE_NAME)

_pull_registry_anno: include-utils
	$(call check_defined, USE_REGISTRY, Must have USE_REGISTRY enabled to pull $(IMAGE_NAME))
	-docker pull $(IMAGE_NAME)

_pull_registry_annobuilder: include-utils
	$(call check_defined, USE_REGISTRY, Must have USE_REGISTRY enabled to pull $(ANNOBUILDER_IMAGE_NAME))
	-docker pull $(ANNOBUILDER_IMAGE_NAME)

pipenv-update: $(MAKEFILE_CORE) pull-ella-anno ## update the requirements.txt
	$(eval REQS_CONTAINER = $(CONTAINER_NAME)-reqs-update)
	docker run -it \
		--name $(REQS_CONTAINER) \
		-u anno-user \
		-v $(PWD)/requirements.txt:/dist/requirements.txt:ro \
		-v $(PWD)/ops:/local_anno/ops \
		$(ELLA_ANNO_SLUG) \
		/local_anno/ops/update_requirements.sh
	docker cp $(REQS_CONTAINER):/dist/requirements.txt.new .
	git diff --no-index requirements.txt requirements.txt.new || true
	docker rm $(REQS_CONTAINER)
	@echo "Use 'mv requirements.txt.new requirements.txt' to apply the changes"


##---------------------------------------------
## AnnoBuilder: generate / download processed data sets for anno/anno-targets
##   Docker commands run in $ANNOBUILDER_IMAGE_NAME named $ANNOBUILDER_CONTAINER_NAME
##   Other variables: PKG_NAME, DB_CREDS, RUN_CMD_ARGS, ANNO_DATA, ANNO_RAWDATA
##---------------------------------------------
.PHONY: build-annobuilder generate-amg-data generate-amg-package download-amg-data download-data
.PHONY: upload-amg-data upload-amg-package $(MAKEFILE_CORE)

# * All the data rules below are run using this template. Unless stated otherwise, the local
#   datasets.json and spaces_config.json are used instead of the ella-anno defaults.
# * ANNOBUILDER_OPTS is checked before being modified so that mounts don't get repeated if the
#   template is called more than once in a single make run. e.g., make download-data
# * The template also ensures that $ANNO_DATA is created and owned by the current user. i.e.,
#   not root, anno-user, or whatever is used by default inside the docker container.
# * Explicitly exports the necessary variables to make sure they're picked up by the other Makefile
#   and that it is clear which variables are being overridden in the sub-make call.
# * Note that RUN_CMD_ARGS must still be passed as a CLI parameter. Sub-make calls get the same CLI
#   parameters from the parent make, which will still take precedence over values passed via export.
#   ref: https://stackoverflow.com/questions/32637204/why-doesnt-gnu-makes-override-pass-through-to-sub-makes
define annobuilder-sync-template
	$(MAKE) $(MAKEFILE_CORE) $(MAKEFILE_UTILS)
	$(eval include $(MAKEFILE_UTILS_PATH))
	$(call check_defined, CMD, 'You must set CMD before using this template')
	[ -d "$(ANNO_DATA)" ] || mkdir -p "$(ANNO_DATA)"
	$(if $(shell echo "$(ANNOBUILDER_OPTS)" | grep -q 'source=$(PWD)/ops/datasets.json' || echo yes),
		$(eval override ANNOBUILDER_OPTS += --mount type=bind,source=$(PWD)/ops/datasets.json,target=$(DATASETS) \
			-v $(PWD)/scripts:/anno/anno-targets-scripts \
			-w /anno
		)
	)
	$(eval _RUN_CMD_ARGS = $(RUN_CMD_ARGS) $(DATASETS_OPT))
	$(eval export ANNO_DATA ANNO_RAWDATA DB_CREDS ANNOBUILDER_IMAGE_NAME SPACES_CONFIG)
	$(MAKE) -f $(MAKEFILE_CORE_PATH) $(CMD) RUN_CMD_ARGS="$(_RUN_CMD_ARGS)" ANNOBUILDER_OPTS="$(ANNOBUILDER_OPTS)"
endef
_IGNORE_VARS += annobuilder-sync-template


# The generate/download/upload steps below work off the parent (ella-anno) repo's Makefile with
# this repo's datasets.json file instead of the ella-anno default. As a result, if more more
# insight on what's happening is needed, check out https://gitlab.com/alleles/ella-anno

_anno-targets:
	$(eval SPACES_CONFIG := $(SPACES_CONFIG_ANNO_TARGETS))
	$(eval DATASETS_OPT := -f $(DATASETS))

generate-amg-data: build-annobuilder _anno-targets ## \
## generate any versioned data sets in datasets.json that do not exist locally
	$(eval CMD := generate-data)
	$(annobuilder-sync-template)

generate-amg-package: include-utils build-annobuilder _anno-targets ## \
## generate the version of $PKG_NAME in datasets.json
	@$(call check_defined, PKG_NAME, 'Use PKG_NAME to specify which package to install')
	$(eval CMD := generate-package PKG_NAME=$(PKG_NAME))
	$(annobuilder-sync-template)

download-amg-data: _anno-targets ## \
## download all data sets in datasets.json, if they do not already exist locally
	$(eval CMD := download-data)
	$(annobuilder-sync-template)

download-amg-package: include-utils _anno-targets ## \
## download the version of $PKG_NAME in datasets.json
	@$(call check_defined, PKG_NAME, 'Use PKG_NAME to specify which package to download')
	$(eval CMD := download-package PKG_NAME=$(PKG_NAME))
	$(annobuilder-sync-template)

_ella-anno:
	$(eval SPACES_CONFIG := $(SPACES_CONFIG_ELLA_ANNO))
	$(eval DATASETS_OPT := )

download-anno-data: _ella-anno ## \
## download all data sets in ella-anno's datasets.json
	$(eval CMD := download-data)
	$(annobuilder-sync-template)

download-anno-package: include-utils _ella-anno ## \
## download the version of $PKG_NAME in ella-anno's datasets.json
	@$(call check_defined, PKG_NAME, 'Use PKG_NAME to specify which package to download')
	$(eval CMD := download-package PKG_NAME=$(PKG_NAME))
	$(annobuilder-sync-template)

download-data: download-amg-data download-anno-data ## \
## download all data sets in ella-anno and this repo's datasets.json files

dataset-versions:
	@jq -r 'to_entries|map("\(.key)=\(.value.version)")|.[]' ops/datasets.json
	@docker run --entrypoint "cat" $(ELLA_ANNO_SLUG) /anno/ops/datasets.json | jq -r 'to_entries|map("\(.key)=\(.value.version)")|.[]'

upload-amg-data: _anno-targets pull-ella-annobuilder ## \
## upload any data sets in datasets.json that do not already exist in DigitalOcean
	$(eval CMD := upload-data)
	$(annobuilder-sync-template)

upload-amg-package: include-utils _anno-targets pull-ella-annobuilder ## \
## upload $PKG_NAME data set to DigitalOcean
	$(eval CMD := upload-package) PKG_NAME=$(PKG_NAME)
	$(annobuilder-sync-template)

tar-data: pull-ella-annobuilder ## package $ANNO_DATA \
## (eventually packages listed in the $PKG_NAMES comma-separated list only) for deployment
	$(eval CMD := tar-data PKG_NAMES=$(PKG_NAMES))
	$(annobuilder-sync-template)
	$(eval CMD := tar-data DATASETS=$(DATASETS) PKG_NAMES=$(PKG_NAMES))
	$(annobuilder-sync-template)


##---------------------------------------------
## Creating release artifacts
##---------------------------------------------
.PHONY: release-docker-build release-docker-push release-singularity-pull release-singularity-build \
	release-singularity-upload ci-release-docker ci-release-singularity local-release

define awscli-template
	$(MAKE) $(AWS_CONFIG_FILE)
	$(eval override AWS_CONFIG_FILE = $(realpath $(AWS_CONFIG_FILE)))
	docker run --rm \
		-u $(UID_GID) \
		-v $(AWS_CONFIG_FILE):$(AWS_CONFIG_FILE) \
		-e AWS_CONFIG_FILE=$(AWS_CONFIG_FILE) \
		-v $(PWD):$(PWD) \
		-w $(PWD) \
		$(AWSCLI_IMAGE) \
		aws --endpoint https://fra1.digitaloceanspaces.com \
		$(AWSCLI_CMD)
endef

build-singularity: ## build $SINGULARITY_IMAGE_NAME from local $IMAGE_NAME
	$(SUDO) singularity build $(SINGULARITY_IMAGE_NAME) docker-daemon://$(IMAGE_NAME)

pull-singularity: ## create $SINGULARITY_IMAGE_NAME from remote docker $IMAGE_NAME
	singularity pull $(SINGULARITY_IMAGE_NAME) docker://$(IMAGE_NAME)

.SILENT: $(AWS_CONFIG_FILE)
$(AWS_CONFIG_FILE):
	$(call check_defined, DB_CREDS)
	[ -f $(DB_CREDS) ] || (echo "DB_CREDS file '$(DB_CREDS)' not found"; exit 1)
	$(eval AWS_CONFIG_DIR = $(dir $(abspath $(AWS_CONFIG_FILE))))
	mkdir -p $(AWS_CONFIG_DIR)
	echo '[default]' > $(AWS_CONFIG_FILE)
	sed 's/SPACES_KEY/aws_access_key_id/; s/SPACES_SECRET/aws_secret_access_key/' $(DB_CREDS) >> $(AWS_CONFIG_FILE)
	echo created AWS config file: $(AWS_CONFIG_FILE)

release-docker-build: include-utils pull-ella-anno ## build release Docker $IMAGE_NAME image
	$(call check_defined, IMAGE_NAME RELEASE_TAG USE_REGISTRY)
	git archive $(RELEASE_TAG) | docker build -t $(IMAGE_NAME) $(BUILD_OPTS) --target prod -

release-docker-push: include-utils ## push release Docker $IMAGE_NAME image
	$(call check_defined, IMAGE_NAME RELEASE_TAG USE_REGISTRY)
	docker push $(IMAGE_NAME)

release-singularity-build: build-singularity ## alias for build-singularity

release-singularity-pull: pull-singularity ## alias for pull-singularity

release-singularity-upload: ## upload Singularity image to S3 bucket
	$(eval AWSCLI_CMD := s3 cp --no-progress $(SINGULARITY_IMAGE_NAME) $(DO_SINGULARITY_IMAGE_URL))
	$(awscli-template)

ci-release-singularity: $(AWS_CONFIG_FILE) release-singularity-pull release-singularity-upload \
## write $AWS_CONFIG_FILE, pull Singularity image and push it to S3 bucket

ci-release-docker: release-docker-build release-docker-push \

local-release: release-docker-build release-singularity-build ## Builds release Docker and Singularity images locally. Requires USE_REGISTRY=1

fetch-singularity-release: include-utils ## Downloads singularity release images from DigitalOcean. Uses: $SINGULARITY_IMAGE_NAME, $DO_SINGULARITY_IMAGE_URL
	$(call check_defined, RELEASE_TAG)
	$(eval AWSCLI_CMD := s3 cp $(DO_SINGULARITY_IMAGE_URL) $(SINGULARITY_IMAGE_NAME))
	$(awscli-template)

##---------------------------------------------
## Testing
##   Other vars: ANNO_DATA
##---------------------------------------------
.PHONY: debug _test-cancer-germline _test-cardio _test-exome _test-genome _test-minimal _test-trio
.PHONY: _test-trio-minimal test-all test-unit _get_lint_script

define test-template
	$(MAKE) $(MAKEFILE_UTILS)
	$(eval include $(MAKEFILE_UTILS_PATH))
	$(call check_defined, CMD, 'You must set CMD before using this template')
	mkdir -p scratch && chmod 777 scratch
	docker run $(TERM_OPTS) \
	-u $(UID_GID) \
	-e SCRATCH=/scratch \
	-v $(ANNO_DATA):/anno/data \
	-v $(PWD)/targets:/anno-targets/targets \
	-v $(PWD)/test:/anno-targets/test \
	-v $(PWD)/scratch:/scratch \
	$(ADDITIONAL_MOUNTS) \
	$(IMAGE_NAME) \
	$(CMD)
endef
_IGNORE_VARS += test-template

debug: ## get a bash shell in a new $IMAGE_NAME using test-template
	$(eval CMD := bash)
	$(test-template)

_test-cancer-germline: _mount_data
	$(eval CMD := /anno-targets/test/targets/ella/test_cancer_germline)
	$(test-template)

_test-cardio: _mount_data
	$(eval CMD := /anno-targets/test/targets/ella/test_cardio)
	$(test-template)

_test-exome: _mount_data
	$(eval CMD := /anno-targets/test/targets/ella/test_exome)
	$(test-template)

_test-exome-trio:
	$(eval CMD := /anno-targets/test/targets/ella/test_exome_trio)
	$(test-template)

_test-exome-trio-minimal:
	$(eval CMD := /anno-targets/test/targets/ella/test_exome_trio_minimal)
	$(test-template)

_test-genome: _mount_data
	$(eval CMD := /anno-targets/test/targets/ella/test_genome)
	$(test-template)

_test-genome-minimal: _mount_data
	$(eval CMD := /anno-targets/test/targets/ella/test_genome_minimal)
	$(test-template)

_test-genome-trio: _mount_data
	$(eval CMD := /anno-targets/test/targets/ella/test_genome_trio)
	$(test-template)

_test-genome-trio-minimal: _mount_data
	$(eval CMD := /anno-targets/test/targets/ella/test_genome_trio_minimal)
	$(test-template)

HGMD_TEST_VCF = $(PWD)/test/testdata/hgmd_dummy/hgmd_xxxx.x_norm.vcf.gz
HGMD_TEST_TBI = $(HGMD_TEST_VCF).tbi
HGMD_VCF_PATTERN = $(ANNO_DATA)/variantDBs/HGMD/hgmd_*.vcf.gz
CLINVAR_TEST_VCF = $(PWD)/test/testdata/clinvar_dummy/clinvar_xxxxxxxx.vcf.gz
CLINVAR_TEST_TBI = $(CLINVAR_TEST_VCF).tbi
CLINVAR_VCF_PATTERN = $(ANNO_DATA)/variantDBs/clinvar/clinvar_*.vcf.gz

_mount_data:
	$(eval HGMD_VCF = $(shell ls $(HGMD_VCF_PATTERN) | tail -n1))
	$(eval HGMD_TBI = $(HGMD_VCF).tbi)
	$(if $(HGMD_VCF),$(eval ADDITIONAL_MOUNTS += -v $(HGMD_TEST_VCF):/anno/data/variantDBs/HGMD/$(notdir $(HGMD_VCF))),$(error Missing required HGMD VCF))
	$(if $(HGMD_TBI),$(eval ADDITIONAL_MOUNTS += -v $(HGMD_TEST_TBI):/anno/data/variantDBs/HGMD/$(notdir $(HGMD_TBI))),$(error Missing required HGMD VCF index))
	$(eval CLINVAR_VCF = $(shell ls $(CLINVAR_VCF_PATTERN) | tail -n1))
	$(eval CLINVAR_TBI = $(CLINVAR_VCF).tbi)
	$(if $(CLINVAR_VCF),$(eval ADDITIONAL_MOUNTS += -v $(CLINVAR_TEST_VCF):/anno/data/variantDBs/clinvar/$(notdir $(CLINVAR_VCF))),$(error Missing required CLINVAR VCF))
	$(if $(CLINVAR_TBI),$(eval ADDITIONAL_MOUNTS += -v $(CLINVAR_TEST_TBI):/anno/data/variantDBs/clinvar/$(notdir $(CLINVAR_TBI))),$(error Missing required CLINVAR VCF index))

TEST_TARGETS = \
	test-cancer-germline \
	test-cardio \
	test-exome \
	test-exome-trio \
	test-exome-trio-minimal \
	test-genome \
	test-genome-minimal \
	test-genome-trio \
	test-genome-trio-minimal \

$(TEST_TARGETS): %: _%

test-unit: ## run unit tests
	$(eval CMD := /anno-targets/test/unit/run_unit_tests)
	$(test-template)

_get_lint_script: pull-ella-anno
	@docker run --rm -u "$(UID_GID)" -v $(PWD):/local_repo $(ELLA_ANNO_SLUG) cp /anno/tests/opstests/lint_shell_scripts.py /local_repo/bin/lint_shell_scripts.py

test-lint: _get_lint_script ## Lint all scripts, uses $LINT_OPTS
	docker run -u "$(UID_GID)" $(TERM_OPTS) --rm -v $(PWD):/anno-targets -w /anno-targets $(IMAGE_NAME) bash -c '. sourceme; python3 bin/lint_shell_scripts.py $(LINT_OPTS)'
