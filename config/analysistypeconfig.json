{
    "CancerGermline": {
        "cnv": {
            "columns": ["pos", "type", "length", "genes_exons", "csq"]
        },
        "ella": {
            "reportIncludeCoverage": true,
            "warningConvadingSampleCVThreshold": 0.099,
            "warningIncludeLowCoverage": true
        },
        "qc": {
            "coverageDepthThreshold": 20,
            "coverageFailedRegionsThreshold": 12
        },
        "regions": {
            "exclude": [
                {
                    "file": "data/regions/cancer/EPCAM_exon1.bed"
                },
                {
                    "file": "data/regions/cancer/POLD1_exons1-6_exons15-27.bed"
                },
                {
                    "file": "data/regions/cancer/POLE_exons1-8_exons15-49.bed"
                }
            ]
        }
    },
    "CancerSomatic": {
        "cnv": {
            "columns": ["pos", "type", "length", "genes_exons", "csq"]
        },
        "ella": {
            "reportIncludeCoverage": true
        },
        "qc": {
            "coverageDepthThreshold": 250
        },
        "regions": {
            "exclude": [
                {
                    "file": "data/regions/cancer/EPCAM_exon1.bed"
                },
                {
                    "file": "data/regions/cancer/POLD1_exons1-6_exons15-27.bed"
                },
                {
                    "file": "data/regions/cancer/POLE_exons1-8_exons15-49.bed"
                }
            ]
        }
    },
    "Cardio": {
        "ella": {
            "reportIncludeCoverage": true
        },
        "qc": {
            "coverageDepthThreshold": 20
        }
    },
    "Exome": {
        "cnv": {
            "columns": ["pos", "type", "score", "length", "indb", "genes", "csq"]
        },
        "qc": {
            "coverageDepthThreshold": 10
        }
    },
    "WGS": {
        "annotsv": {
            "fields": {
                "in": [
                    "Annotation_mode",
                    "CytoBand",
                    "Gene_count",
                    "compound_htz",
                    "Repeat_type_left",
                    "Repeat_type_right",
                    "Gap_left",
                    "Gap_right",
                    "SegDup_left",
                    "SegDup_right",
                    "ENCODE_blacklist_left",
                    "ENCODE_blacklist_characteristics_left",
                    "ENCODE_blacklist_right",
                    "ENCODE_blacklist_characteristics_right",
                    "AnnotSV_ranking_score",
                    "AnnotSV_ranking_criteria",
                    "ACMG_class"
                ]
            }
        },
        "qc": {
            "coverageDepthThreshold": 10
        },
        "regions": {
            "intersect": [
                {
                    "file": "data/regions/exons/refGeneExons_20180619.bed",
                    "min_genes": 2,
                    "slop": 100
                }
            ]
        },
        "svdb": {
            "criteria": {
                "canvas": {
                    "indb": {
                        "info:FRQ_INDB_CANVAS": {
                            "gt": 0.01
                        },
                        "info:OCC_INDB_CANVAS": {
                            "gt": 10
                        }
                    },
                    "swegen": {
                        "info:FRQ_SWEGEN_CNVNATOR": {
                            "gt": 0.01
                        },
                        "info:OCC_SWEGEN_CNVNATOR": {
                            "gt": 10
                        }
                    }
                },
                "cnvnator": {
                    "indb": {
                        "info:FRQ_INDB_CNVNATOR": {
                            "gt": 0.01
                        },
                        "info:OCC_INDB_CNVNATOR": {
                            "gt": 10
                        }
                    },
                    "swegen": {
                        "info:FRQ_SWEGEN_CNVNATOR": {
                            "gt": 0.01
                        },
                        "info:OCC_SWEGEN_CNVNATOR": {
                            "gt": 10
                        }
                    }
                },
                "common": {
                    "gnomad": {
                        "info:FRQ_GNOMAD": {
                            "gt": 0.01
                        },
                        "info:OCC_GNOMAD": {
                            "gt": 50
                        }
                    }
                },
                "delly": {
                    "indb": {
                        "info:FRQ_INDB_DELLY": {
                            "gt": 0.01
                        },
                        "info:OCC_INDB_DELLY": {
                            "gt": 10
                        }
                    },
                    "swegen": {
                        "info:FRQ_SWEGEN_DELLY": {
                            "gt": 0.01
                        },
                        "info:OCC_SWEGEN_DELLY": {
                            "gt": 10
                        }
                    }
                },
                "manta": {
                    "indb": {
                        "info:FRQ_INDB_MANTA": {
                            "gt": 0.01
                        },
                        "info:OCC_INDB_MANTA": {
                            "gt": 10
                        }
                    },
                    "swegen": {
                        "info:FRQ_SWEGEN_MANTA": {
                            "gt": 0.01
                        },
                        "info:OCC_SWEGEN_MANTA": {
                            "gt": 10
                        }
                    }
                },
                "tiddit": {
                    "indb": {
                        "info:FRQ_INDB_TIDDIT": {
                            "gt": 0.01
                        },
                        "info:OCC_INDB_TIDDIT": {
                            "gt": 10
                        }
                    },
                    "swegen": {
                        "info:FRQ_SWEGEN_TIDDIT": {
                            "gt": 0.01
                        },
                        "info:OCC_SWEGEN_TIDDIT": {
                            "gt": 10
                        }
                    }
                }
            },
            "exceptions": {
                "rescue_high_ACMG_class_on_X": {
                    "chrom": {
                        "in": ["X"]
                    },
                    "filter": {
                        "contains": ["HighACMGClass"],
                        "search": "^(?!.*cnvLength5k).*"
                    }
                },
                "rescue_homozygote_DEL_on_X": {
                    "chrom": {
                        "in": ["X"]
                    },
                    "filter": {
                        "search": "^(?!.*cnvLength5k).*"
                    },
                    "format:GT": {
                        "in": ["1", "1/1", "1|1"]
                    },
                    "info:SVTYPE": {
                        "in": ["DEL"]
                    }
                }
            }
        },
        "svparams": {
            "filters": {
                "HighACMGClass": {
                    "common": {
                        "high_ACMG_class": {
                            "chrom": {
                                "in": ["X"]
                            },
                            "info:ACMG_class": {
                                "in": ["4", "5"]
                            }
                        }
                    }
                },
                "MinSizeIntronVariant": {
                    "common": {
                        "small_intron_variants": {
                            "info:CSQ": {
                                "search": "\\|intron_variant\\||\\|intron_variant&non_coding_transcript_variant\\||\\|intron_variant&downstream_gene_variant\\||\\|downstream_gene_variant\\||\\|feature_truncation&intron_variant\\||(?:\\|intron_variant\\||\\|downstream_gene_variant\\|)"
                            },
                            "info:SVLEN": {
                                "lt": 1000
                            }
                        }
                    }
                },
                "cnvLength5k": {
                    "common": {
                        "set_cnvLength_filter_for_canvas": {
                            "id": {
                                "search": "^DRAGEN:(LOSS|GAIN)"
                            },
                            "info:SVLEN": {
                                "lt": 5000
                            }
                        }
                    }
                }
            },
            "gene_panel_slop": 1000,
            "interpretation_groups": {
                "canonical": {
                    "chrom": {
                        "in": [
                            "1",
                            "2",
                            "3",
                            "4",
                            "5",
                            "6",
                            "7",
                            "8",
                            "9",
                            "10",
                            "11",
                            "12",
                            "13",
                            "14",
                            "15",
                            "16",
                            "17",
                            "18",
                            "19",
                            "20",
                            "21",
                            "22",
                            "X",
                            "Y"
                        ]
                    },
                    "filter": {
                        "contains": ["PASS"]
                    }
                },
                "ella": {
                    "filter": {
                        "search": "^[^.]*(PASS|HighACMGClass)"
                    },
                    "info:SVTYPE": {
                        "in": ["DEL", "DUP"]
                    }
                },
                "ella_debug": {
                    "info:SVTYPE": {
                        "in": ["DEL", "DUP", "INS", "INV", "BND"]
                    }
                },
                "large": {
                    "info:SVLEN": {
                        "ge": 1000000000000
                    },
                    "info:SVTYPE": {
                        "in": ["DEL", "DUP", "INS", "INV", "BND"]
                    }
                },
                "skip_vep": {
                    "info:SVLEN": {
                        "gt": 10000000
                    },
                    "info:SVTYPE": {
                        "in": ["DEL", "DUP", "INS", "INV", "BND"]
                    }
                },
                "small": {
                    "info:SVLEN": {
                        "lt": 1000000000000
                    },
                    "info:SVTYPE": {
                        "in": ["DEL", "DUP", "INS", "INV", "BND"]
                    }
                }
            }
        }
    }
}
