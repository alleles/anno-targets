"""
Specify a set of environmental variables needed. The constraints and
validations for each variable will be enforced at runtime.
"""

from pydantic import StringConstraints
from pydantic_settings import BaseSettings
from typing import Literal
from typing_extensions import Annotated


class ParserInputs(BaseSettings):
    """
    required environmental variables
    """
    SAMPLE_ID: Annotated[str, StringConstraints(pattern=r'.*(wgs|EKG|EHG|[Ee]xcap).*')]
    GP_NAME: str
    GP_VERSION: str
    TYPE: Literal['single', 'trio']
    CAPTUREKIT: str

    class Config:
        case_sensitive = True
        env_prefix = ''


if __name__ == "__main__":
    p = ParserInputs()
    print(p.json(indent=4))
