{
    "$schema": "http://json-schema.org/draft-04/schema#",
    "additionalProperties": false,
    "definitions": {
        "annotsv": {
            "additionalProperties": false,
            "properties": {
                "fields": {
                    "additionalProperties": false,
                    "properties": {
                        "in": {
                            "additionalItems": false,
                            "items": {
                                "maxLength": 100,
                                "type": "string"
                            },
                            "type": "array"
                        }
                    },
                    "required": ["in"],
                    "type": "object"
                }
            },
            "required": ["fields"],
            "type": "object"
        },
        "cnv": {
            "additionalProperties": false,
            "properties": {
                "columns": {
                    "items": {
                        "enum": [
                            "pos",
                            "type",
                            "score",
                            "length",
                            "indb",
                            "genes",
                            "genes_exons",
                            "csq"
                        ],
                        "type": "string"
                    },
                    "type": "array"
                }
            },
            "type": "object"
        },
        "ella": {
            "additionalProperties": false,
            "properties": {
                "reportIncludeCoverage": {
                    "type": "boolean"
                },
                "warningConvadingSampleCVThreshold": {
                    "type": "number"
                },
                "warningIncludeLowCoverage": {
                    "type": "boolean"
                }
            },
            "type": "object"
        },
        "qc": {
            "additionalProperties": false,
            "properties": {
                "coverageDepthThreshold": {
                    "minimum": 0,
                    "type": "integer"
                },
                "coverageFailedRegionsThreshold": {
                    "minimum": 0,
                    "type": "integer"
                }
            },
            "required": ["coverageDepthThreshold"],
            "type": "object"
        },
        "regions": {
            "additionalProperties": false,
            "properties": {
                "exclude": {
                    "additionalItems": false,
                    "items": {
                        "additionalProperties": false,
                        "optional": ["slop", "min_genes"],
                        "properties": {
                            "file": {
                                "type": "string"
                            },
                            "min_genes": {
                                "minimum": 0,
                                "type": "integer"
                            },
                            "slop": {
                                "minimum": 1,
                                "type": "integer"
                            }
                        },
                        "required": ["file"],
                        "type": "object"
                    },
                    "type": "array"
                },
                "intersect": {
                    "additionalItems": false,
                    "items": {
                        "additionalProperties": false,
                        "optional": ["slop", "min_genes"],
                        "properties": {
                            "file": {
                                "type": "string"
                            },
                            "min_genes": {
                                "minimum": 0,
                                "type": "integer"
                            },
                            "slop": {
                                "minimum": 1,
                                "type": "integer"
                            }
                        },
                        "required": ["file"],
                        "type": "object"
                    },
                    "type": "array"
                }
            },
            "type": "object"
        },
        "svdb": {
            "properties": {
                "criteria": {
                    "patternProperties": {
                        "(common|cnvnator|manta|delly|tiddit|canvas)": {
                            "patternProperties": {
                                "(gnomad|swegen|indb)": {
                                    "patternProperties": {
                                        "^info:(NSAMPLES|OCC_)[A-Z_]*$": {
                                            "properties": {
                                                "gt": {
                                                    "minimum": 0,
                                                    "type": "integer"
                                                }
                                            },
                                            "type": "object"
                                        },
                                        "^info:FRQ_[A-Z_]*$": {
                                            "properties": {
                                                "gt": {
                                                    "maximum": 1,
                                                    "minimum": 0,
                                                    "type": "number"
                                                }
                                            },
                                            "type": "object"
                                        }
                                    },
                                    "type": "object"
                                }
                            },
                            "type": "object"
                        }
                    },
                    "type": "object"
                },
                "exceptions": {
                    "patternProperties": {
                        "rescue_.*": {
                            "patternProperties": {
                                "(chrom|pos|id|ref|alt|qual)": {
                                    "properties": {
                                        "in": {
                                            "items": {
                                                "type": "string"
                                            },
                                            "minItems": 1,
                                            "type": "array"
                                        }
                                    },
                                    "type": "object"
                                },
                                "^(info|format):.*$": {
                                    "patternProperties": {
                                        "(lt|le|gt|ge|eq|ne)": {
                                            "minimum": 0,
                                            "type": "number"
                                        },
                                        "in": {
                                            "items": {
                                                "type": "string"
                                            },
                                            "minItems": 1,
                                            "type": "array"
                                        }
                                    },
                                    "type": "object"
                                },
                                "filter": {
                                    "patternProperties": {
                                        "(contains|in)": {
                                            "items": {
                                                "type": "string"
                                            },
                                            "minItems": 1,
                                            "type": "array"
                                        }
                                    },
                                    "type": "object"
                                }
                            },
                            "type": "object"
                        }
                    },
                    "type": "object"
                }
            },
            "required": ["criteria", "exceptions"],
            "type": "object"
        },
        "svparams": {
            "properties": {
                "filters": {
                    "patternProperties": {
                        ".*": {
                            "patternProperties": {
                                "(manta|delly|tiddit|cnvnator|canvas|exceptions|common)": {
                                    "patternProperties": {
                                        ".*": {
                                            "patternProperties": {
                                                "(chrom|pos|id|ref|alt|qual)": {
                                                    "properties": {
                                                        "in": {
                                                            "items": {
                                                                "type": "string"
                                                            },
                                                            "minItems": 1,
                                                            "type": "array"
                                                        },
                                                        "search": {
                                                            "type": "string"
                                                        }
                                                    },
                                                    "type": "object"
                                                },
                                                "^(info|format):.*$": {
                                                    "patternProperties": {
                                                        "(lt|le|gt|ge|eq|ne)": {
                                                            "minimum": 0,
                                                            "type": "number"
                                                        },
                                                        "in": {
                                                            "items": {
                                                                "type": "string"
                                                            },
                                                            "minItems": 1,
                                                            "type": "array"
                                                        },
                                                        "search": {
                                                            "type": "string"
                                                        }
                                                    },
                                                    "type": "object"
                                                },
                                                "filter": {
                                                    "patternProperties": {
                                                        "(contains|in)": {
                                                            "items": {
                                                                "type": "string"
                                                            },
                                                            "minItems": 1,
                                                            "type": "array"
                                                        },
                                                        "search": {
                                                            "type": "string"
                                                        }
                                                    },
                                                    "type": "object"
                                                }
                                            },
                                            "type": "object"
                                        }
                                    },
                                    "type": "object"
                                }
                            },
                            "type": "object"
                        }
                    },
                    "type": "object"
                },
                "gene_panel_slop": {
                    "minimum": 0,
                    "type": "integer"
                },
                "interpretation_groups": {
                    "patternProperties": {
                        "(canonical|small|large|skip_vep|ella)": {
                            "patternProperties": {
                                "(chrom|pos|id|ref|alt|qual)": {
                                    "properties": {
                                        "in": {
                                            "items": {
                                                "type": "string"
                                            },
                                            "minItems": 1,
                                            "type": "array"
                                        }
                                    },
                                    "type": "object"
                                },
                                "^(info|format):.*$": {
                                    "patternProperties": {
                                        "(lt|le|gt|ge|eq|ne)": {
                                            "minimum": 0,
                                            "type": "number"
                                        },
                                        "in": {
                                            "items": {
                                                "type": "string"
                                            },
                                            "minItems": 1,
                                            "type": "array"
                                        }
                                    },
                                    "type": "object"
                                },
                                "^filter": {
                                    "patternProperties": {
                                        "(contains|in)": {
                                            "items": {
                                                "type": "string"
                                            },
                                            "minItems": 1,
                                            "type": "array"
                                        }
                                    },
                                    "type": "object"
                                }
                            },
                            "type": "object"
                        }
                    },
                    "type": "object"
                }
            },
            "required": ["filters", "gene_panel_slop"],
            "type": "object"
        }
    },
    "patternProperties": {
        "^.*$": {
            "additionalProperties": false,
            "properties": {
                "annotsv": {
                    "$ref": "#/definitions/annotsv"
                },
                "cnv": {
                    "$ref": "#/definitions/cnv"
                },
                "ella": {
                    "$ref": "#/definitions/ella"
                },
                "qc": {
                    "$ref": "#/definitions/qc"
                },
                "regions": {
                    "$ref": "#/definitions/regions"
                },
                "svdb": {
                    "$ref": "#/definitions/svdb"
                },
                "svparams": {
                    "$ref": "#/definitions/svparams"
                }
            },
            "required": ["qc"]
        }
    },
    "type": "object"
}
