# Release vX.X.X

## Release content

Software [Fill in for 'X' and 'Y' once the individual versions are available]:

- ella-anno vX.X.X
- anno-targets vY.Y.Y

Data:

…list packages and versions

## Major steps of release

### Prepare

- [ ] Endringskontroll if relevant (only for significant source code changes, not data updates, not patches)
- [ ] All necessary credentials set (DigitalOcean, HGMD, ClinVar API)

### Create release artifacts

- [ ] Generate data (`ella-anno` or `anno-targets`), i.e `make generate-package  …  `
- [ ] Upload data to DO
- [ ] Merge all relevant `ella-anno` feature branches into `dev` (tag the latter [vX.X.X-rcX] if needed)
- [ ] Merge all relevant `anno-targets` feature branches into `dev` (tag the latter [vY.Y.Y-rcY] if needed)
- [ ] Once all tests PASS, merge `ella-anno` and/or `anno-targets`'s `dev` branches into the corresponding `master` branches. Make sure to test for dependencies in `anno-targets` when making changes in `ella-anno`
- [ ] Tag `ella-anno` (TAG: vX.X.X) and/or `anno-targets` (TAG: vY.Y.Y) on their respective `master` branches
- [ ] Download data from DO and create data tar file
- [ ] Run `make fetch-singularity-release` on `anno-targets`'s `master` branch tagged vY.Y.Y

### Release notes

- [ ] Release notes in `ella-anno`
- [ ] Release package
- [ ] docs-store release log page
- [ ] User announcement in GDx operational Teams channel
