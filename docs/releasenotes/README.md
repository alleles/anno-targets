---
title: Latest release
---

# Release notes: Latest releases

|Major versions|Minor versions|
|:--|:--|
| [v2.18](#version-218)| [v2.18.1](#version-2181), [v2.18.2](#version-2182), [v2.18.3](#version-2183), [v2.18.4](#version-2184)  |
| [v2.17](#version-217)| [v2.17.1](#version-2171), [v2.17.1a](#version-2171a) |
| [v2.16](#version-216)| |
| [v2.15](#version-215)| [v2.15.1](#version-2151), [v2.15.2](#version-2152), [v2.15.3](#version-2153) |
| [v2.14](#version-214)| [v2.14.1](#version-2141), [v2.14.2](#version-2142), [v2.14.3](#version-2143) |

## Version 2.18.4
Release date 04.03.2025
<!-- MR !229 -->
- Update GATK from v3.8 to a v4.2.6.1

## Version 2.18.3
Release date 03.03.2025
<!-- MR !229 -->
- Hotfix: Combinations of panels that should include mtDNA variants do not actually trigger mtDNA report
<!-- MR !232 -->
- Support custom regions
<!-- MR !234 -->
- Always upload test artefacts
<!-- MR !226 -->
- Update HGMD to version 2024.4

## Version 2.18.2

Release date 24.02.2025
<!-- MR !225 -->
- Treat cram same as bam
<!-- MR !224 -->
- Test python upgrade in ella-anno
<!-- MR !218 -->
- Fix fetched anno targets version display
<!-- MR !215 -->
- Remove `ensembl_transcript` item
<!-- MR !214 -->
- Add config for skipping excluding/intersecting regions for small panels

## Version 2.18.1

Release date 09.02.2025
<!-- MR !220 -->
- Use VEP merged cache
<!-- MR !219 -->
- Rectify environment variable name

## >> Version 2.18

Release date 30.01.2025
<!-- MR !185 -->
- Introduce mtDNA report

## Version 2.17.1a

Release date 10.01.2025
<!-- MR !199 -->
- Updated HGMD to version 2024.3
<!-- MR !212 -->
- Adjust to new VEP v113.3

## Version 2.17.1

Release date 20.12.2024
<!-- MR !203 -->
- Add in-house database for genomes on NovaseqX (small variants only)

## >> Version 2.17

Release date 20.12.2024
<!-- MR !121 -->
- Update hard-filtering of CNVs
<!-- MR !124 -->
- Fix error that rescues cnvLength5k variants

## >> Version 2.16

Release date 18.11.2024
<!-- MR !110 -->
- Standardizing CNV VCF for ELLA
<!-- MR !116 -->
- `datasets.json` must be adapted to changes in `ella-anno`

## Version 2.15.3

Release date 06.08.2024
<!-- MR !183 -->
- Refactored CNV script and optimized runtime

<!-- MR !188 -->
- Updated HGMD to version 2024.2

## Version 2.15.2

Release date 08.07.2024
<!-- MR !172 -->
- Clean up AnnotSV output fields

<!-- MR !180 -->
- Adjusted hg38 coordinates for CNV variants


## Version 2.15.1

Release date 20.06.2024
<!-- MR !179 -->
- BUG FIX: Create analysis name when it is not available in the ENV

<!-- MR !178 -->
- Add hg38 coordinates for CNV variants

<!-- MR !177 -->
- Implement changes based on Steps/VCpipe development

## >>  Version 2.15

Release date 16.05.2024
<!-- MR !173 -->
- Implement changes based on Steps/VCpipe development

## Version 2.14.3

Release date 05.04.2024
<!-- MR !170 -->
- Update HGMD to version 2024.1
<!-- MR !161 -->
- The variable holding the Digital Ocean credentials must be renamed
<!-- MR !166 -->
- Change licence
<!-- MR !167 -->
- Bug: changes in VEP 110.1 affecting cnv annotation
<!-- MR !168 -->
- Vep110 refseq patch
<!-- MR !169 -->
- Percent signs cause interpolation errors in `configparser`

## Version 2.14.2

Unreleased (a new VEP caused a few bugs)

<!-- MR !162 -->
- Remove remaining calls to VEP merged cache
<!-- MR !163 -->
- Update AnnotSV to v3.4
<!-- MR !164 -->
- Adjust anno release template

## Version 2.14.1
<!-- MR !156 -->
- Update HGMD to version 2023.4
<!-- MR !159 -->
- Use correct URL for quagen hgmd download
<!-- MR !157 -->
- HGMD update script is broken

## >> Version 2.14
<!-- MR !148 -->
- Filter all intronic CNVs <300 bp
<!-- MR !151 -->
- Remove leftover genomicSuperDup files
<!-- MR !152 -->
- Remove dead code
<!-- MR !154 -->
- Fix anno release test
