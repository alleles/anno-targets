# Annopipe


## Annotation

| Step                         | Single/Trio    | Trio                       | Dependencies            | Notes                  |
|------------------------------|----------------|----------------------------|-------------------------|------------------------|
| Slice by genepanel           | bedtools slice |                            | transcripts             | Handled by anno        |
| Annotation                   | vt/vep/vcfanno |                            | Not relevant            | Handled by anno        |


## CNV Calling and Annotation

Only run if cnv file exists in preprocessed

| Step                     | Single/Trio        | Trio | Dependencies                    | Notes                            |
|--------------------------|--------------------|------|---------------------------------|----------------------------------|
| CNV: filter by genepanel | bedtools intersect |      | transcripts                     |                                  |
| CNV: annotate            | cnvScan_annotate   |      | params_resources, params_cnv_db | Need to move cnvdata to anno[^1] |

[^1]: make it part of the base anno pipeline?


## QC

| Step                 | Single/Trio            | Trio                  | Dependencies                                                | Notes |
|----------------------|------------------------|-----------------------|-------------------------------------------------------------|-------|
| QC                   |                        | gatk T SelectVariants | fasta                                                       |       |
| Coverage calculation | qc_coverage            |                       | slop2                                                       |       |
|                      | qc_analysis            |                       | capturekit, noslop_list and contamination_test, transcripts |       |
| Coverage report      | report_coverage_single |                       | report_config                                               |       |
| Report warning       | report_warning         |                       | captureKit                                                  |       |


## QC join

| Step            | Single/Trio | Trio                 | Dependencies                                                                  | Notes |
|-----------------|-------------|----------------------|-------------------------------------------------------------------------------|-------|
| Coverage report |             | report_coverage_trio | pedigree, coveragetranscriptproband/mother/father, transcripts, report_config |       |


# Ella

| Step        | Single/Trio        | Trio | Dependencies             | Notes |
|-------------|--------------------|------|--------------------------|-------|
| ella export | bedtools intersect |      | transcripts, ella_remove |       |


