#!/bin/bash -e
set -o pipefail

# This should ONLY be run inside one of the Docker containers built using the Makefile
#
# To update existing packages to the newest available versions:
#    - run: make pipenv-update
# To add a new package:
#    - add the package name without a version to requirements.txt
#    - run: make pipenv-update
# To remove a package:
#    - delete the line with the package name from requirements.txt
#    - run: make pipenv-update
#
# If upstream ella-anno updates their pacakges / Pipfile, this should be re-run to ensure
# that the versions we have set are compatibile.
#
# TODO: add a mechanism to lock a package at a single version. Pipenv does not like extending
#   existing Pipfiles or support multi-env Pipfiles

# tell pipenv where Pipfile is so it doesn't have to search
export PIPENV_PIPFILE=${PIPENV_PIPFILE:-/anno/Pipfile}

# setup
DIST_ROOT=/dist
REQS_TXT=${DIST_ROOT}/requirements.txt
REQS_NEW=${REQS_TXT}.new
REQS_NO_VERSIONS=${DIST_ROOT}/requirements-unversioned.txt

do_update() {
    # get the version-less requirements, ignore repo installs
    sed -e 's/==.\+$//' "${REQS_TXT}" | grep -v 'git+' >"${REQS_NO_VERSIONS}"

    # save the existing venv in case we want to compare
    pipenv install -r "${REQS_NO_VERSIONS}"

    pipenv lock -r | grep -f "${REQS_NO_VERSIONS}" >"${REQS_NEW}"
    grep 'git+' ${REQS_TXT} >>"${REQS_NEW}"
}

do_update
