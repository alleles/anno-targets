#!/bin/bash
set -euf -o pipefail
THISDIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

PYTHONPATH=${THISDIR}/thirdparty/site-packages python3 -m pytest "${THISDIR}/src"
