import hypothesis as ht
import hypothesis.strategies as st
import tempfile
import subprocess
import os
import json

ROOT = os.path.normpath(os.path.join(os.path.dirname(__file__), "../../"))
TESTDIR = os.path.abspath(os.path.curdir)


def get_tmp_file():
    tmp_file = tempfile.NamedTemporaryFile(dir=TESTDIR, delete=False, mode='wt')
    assert tmp_file.name.startswith(ROOT), f"Temporary files must be created under '{ROOT}'"
    return tmp_file


@st.composite
def bed_regions(draw):
    regions = draw(
        st.lists(
            st.tuples(
                st.sampled_from([1, 2, "X", "Y"]),
                st.integers(min_value=1, max_value=1000),
                st.integers(min_value=1, max_value=1000),
            ),
            min_size=1,
        )
    )

    for i, r in enumerate(regions):
        if r[1] < r[2]:
            regions[i] = (str(r[0]), r[1], r[2])
        else:
            regions[i] = (str(r[0]), r[2], r[1])
        ht.assume(r[1] != r[2])

    return regions


def write_bed(bed_regions):
    f = get_tmp_file()
    for chrom, pos1, pos2 in bed_regions:
        f.write(f"{chrom}\t{pos1}\t{pos2}\n")
    return f.name


def create_config(bed_regions_list):
    config = []
    for bed_regions in bed_regions_list:
        regions_file = write_bed(bed_regions)
        config.append({"file": regions_file.removeprefix(f"{ROOT}/")})
    return config


def apply_regions(config, action, regions_file):
    config_json = json.dumps(config).replace('"', '\\"')
    cmdarr = [
        f"source '{ROOT}'/targets/utils.source",
        f"apply_regions '{config_json}' {action} '{regions_file}'"
    ]
    cmd = "; ".join(cmdarr)
    return subprocess.check_output(f'/bin/bash -c "{cmd}"', shell=True)


def intersection(a, b):
    # Check for intersection between genomic intervals ("chrom", start, open_end)
    if a[0] != b[0]:
        return []
    elif a[2] <= b[1]:
        return []
    elif a[1] >= b[2]:
        return []
    elif a[1] <= b[1] and a[2] >= b[2]:
        return b
    elif a[1] >= b[1] and a[2] <= b[2]:
        return a
    elif a[1] <= b[1]:
        return (a[0], b[1], a[2])
    elif a[2] >= b[2]:
        return (a[0], a[1], b[2])
    else:
        raise NotImplementedError()


@ht.settings(deadline=None)
@ht.given(st.one_of(bed_regions()), st.lists(bed_regions(), min_size=1, max_size=3))
def test_intersect(regions, intersects):
    regions_file = write_bed(regions)
    config = create_config(intersects)

    # Intersect regions_file with intersect regions. regions_file will be overwritten.
    apply_regions(config, "intersect", regions_file)

    # TODO: Add variable slop. Slop is set constant here, but should be variable.
    slop = 0

    # Combine intersects. For an interval to be included in the final list it need only intersect
    # one of the provided intersects-files
    combined_intersects = sum(intersects, [])
    with open(regions_file, "r") as f:
        for r in f:
            r = r.strip().split()
            chrom, start, end = r[0], int(r[1]), int(r[2])

            # Check that all subintervals intersect with given intersects
            tmp_start = start
            tmp_end = start + 1
            while tmp_end <= end:
                assert any(
                    intersection(
                        (chrom, tmp_start, tmp_end),
                        (i[0], max(0, i[1] - slop), i[2] + slop),
                    )
                    for i in combined_intersects
                )
                tmp_start += 1
                tmp_end += 1


@ht.settings(deadline=None)
@ht.given(st.one_of(bed_regions()), st.lists(bed_regions(), min_size=1, max_size=3))
def test_exclude(regions, excludes):
    regions_file = write_bed(regions)
    config = create_config(excludes)

    # Exclude regions from regions_file. regions_file will be overwritten.
    apply_regions(config, "exclude", regions_file)

    # TODO: Add variable slop. Slop is set constant here, but should be variable.
    slop = 0

    # Combine excludes.
    combined_excludes = sum(excludes, [])
    with open(regions_file, "r") as f:
        for r in f:
            r = r.strip().split()
            chrom, start, end = r[0], int(r[1]), int(r[2])

            # Check that all subintervals does NOT intersect with any of the give exclude regions
            tmp_start = start
            tmp_end = start + 1
            while tmp_end <= end:
                assert not any(
                    intersection(
                        (chrom, tmp_start, tmp_end),
                        (i[0], max(0, i[1] - slop), i[2] + slop),
                    )
                    for i in combined_excludes
                )
                tmp_start += 1
                tmp_end += 1
