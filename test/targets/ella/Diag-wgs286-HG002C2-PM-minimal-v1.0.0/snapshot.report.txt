# Sample information


## Diag-wgs286-HG002C2-PM

| Sequencing info    |               |
|:-------------------|:-------------:|
| Sequencer ID       | E00401 |
| Capture kit        | wgs  |


## Diag-wgs286-HG004B3-MK

| Sequencing info    |               |
|:-------------------|:-------------:|
| Sequencer ID       | E00401 |
| Capture kit        | wgs  |


## Diag-wgs286-HG003B2-FM

| Sequencing info    |               |
|:-------------------|:-------------:|
| Sequencer ID       | E00401 |
| Capture kit        | wgs  |



# Pipeline versions





# Analysis supplement

## mtDNA

**Haplogroup** H5a7


 | No variants | 
 |:--- |


