### Gener/Regioner med dekningsgrad under 100%:

|Gen/Region|Transkript|Sykdom|Arvegang|Antall kodende<br> bp + 26 bp (1)|Dekningsgrad<br> (% bp) (2)|
|---|---|---|---|---|---|
|ABCC9|NM_005691.3|Hypertrichotic osteochondrodysplasia (Cantu syndrome)<br>Cardiomyopathy, dilated, 1O<br>?Atrial fibrillation, familial, 12<br>Intellectual disability and myopathy syndrome|AD<br>AD<br>AD<br>AR|5638|99.7%|
|AKAP9|NM_005751.4|?Long QT syndrome 11|AD|13024|98.4%|
|APOB|NM_000384.3|Hypercholesterolemia, familial, 2<br>Hypobetalipoproteinemia|AD<br>AR|14446|99.9%|
|BRAF|NM_001374258.1|Colon cancer, advanced, somatic; Colon cancer, somatic; Colorectal cancer with chromosomal instability, somatic; Colorectal cancer, somatic; {?Colorectal cancer, susceptibility to}; {Colon cancer, susceptibility to}; {Colorectal cancer, susceptibility to}; {Colorectal cancer}<br>Melanoma, malignant, somatic<br>Adenocarcinoma of lung, response to tyrosine kinase inhibitor in; Adenocarcinoma of lung, somatic; Lung cancer, somatic; Nonsmall cell lung cancer, response to tyrosine kinase inhibitor in; Nonsmall cell lung cancer, somatic; {Lung cancer, protection against}; {Lung cancer, resistance to}; {Lung cancer, susceptibility to}; {Nonsmall cell lung cancer, susceptibility to}<br>Cardiofaciocutaneous syndrome<br>Noonan syndrome 7<br>LEOPARD syndrome 3|<br><br><br>AD<br>AD<br>AD|2944|95.0%|
|CACNA2D1|NM_000722.4|Developmental and epileptic encephalopathy 110|AR|4290|98.2%|
|COL5A1|NM_000093.5|Ehlers-Danlos syndrome, classic type, 1<br>Fibromuscular dysplasia, multifocal|AD<br>AD|7233|99.0%|
|CTF1|NM_001330.3|n/a|AR|684|66.1%|
|DPP6|NM_130797.4|{Ventricular fibrillation, paroxysmal familial, 2}<br>Intellectual developmental disorder, autosomal dominant 33|AD<br>AD|3274|98.0%|
|DSC2|NM_024422.6|Arrhythmogenic right ventricular dysplasia 11; Arrhythmogenic right ventricular dysplasia 11 with mild palmoplantar keratoderma and woolly hair|AD/AR|3122|99.8%|
|FBN1|NM_000138.4|Acromicric dysplasia<br>Ectopia lentis, familial<br>Marfan syndrome<br>Stiff skin syndrome<br>MASS syndrome<br>Weill-Marchesani syndrome 2, dominant<br>Geleophysic dysplasia 2<br>Marfan lipodystrophy syndrome|AD<br>AD<br>AD<br>AD<br>AD<br>AD<br>AD<br>AD|10306|99.7%|
|HCN4|NM_005477.3|Brugada syndrome 8<br>Sick sinus syndrome 2<br>{Epilepsy, idiopathic generalized, susceptibility to, 18}|<br>AD<br>AD|3820|98.4%|
|JPH2|NM_020433.4|Cardiomyopathy, hypertrophic, 17<br>Cardiomyopathy, dilated, 2E|AD<br>AR|2221|96.4%|
|KCNH2|NM_000238.4|Short QT syndrome 1<br>Long QT syndrome 2; {Long QT syndrome, acquired, reduced susceptibility to}|<br>AD|3870|99.4%|
|KCNQ1|NM_000218.3|Long QT syndrome 1; {Long QT syndrome 1, acquired, susceptibility to}<br>Atrial fibrillation, familial, 3<br>Short QT syndrome 2<br>Jervell and Lange-Nielsen syndrome|AD<br>AD<br>AD<br>AR|2447|97.0%|
|MIB1|NM_020774.3|Left ventricular noncompaction 7|AD|3567|99.1%|
|MYO6|NM_004999.4|Deafness, autosomal dominant 22; Deafness, autosomal dominant 22, with hypertrophic cardiomyopathy<br>Deafness, autosomal recessive 37|AD<br>AR|4742|96.8%|
|PRKAG2|NM_016203.4|Wolff-Parkinson-White syndrome<br>Glycogen storage disease of heart, lethal congenital<br>Cardiomyopathy, hypertrophic 6|AD<br>AD<br>AD|2126|93.2%|
|PRKAR1A|NM_002734.4|Acrodysostosis 1, with or without hormone resistance<br>Carney complex, type 1<br>Myxoma, intracardiac<br>Pigmented nodular adrenocortical disease, primary, 1|AD<br>AD<br>AD<br>AD|1406|98.4%|
|RYR1|NM_000540.3|{Malignant hyperthermia susceptibility 1}<br>King-Denborough syndrome<br>Central core disease; Neuromuscular disease, congenital, with uniform type 1 fiber<br>Minicore myopathy with external ophthalmoplegia|AD<br>AD<br>AD/AR<br>AR|17873|98.7%|
|RYR2|NM_001035.3|Ventricular arrhythmias due to cardiac ryanodine receptor calcium release deficiency syndrome<br>Arrhythmogenic right ventricular dysplasia 2<br>Ventricular tachycardia, catecholaminergic polymorphic, 1|AD<br>AD<br>AD|17634|99.9%|
|SCN1B|NM_001037.5|Brugada syndrome 5; Cardiac conduction defect, nonspecific<br>Generalized epilepsy with febrile seizures plus, type 1<br>Atrial fibrillation, familial, 13<br>Developmental and epileptic encephalopathy 52|<br>AD<br>AD<br>AR|787|98.6%|
|SELENON|NM_020451.3|Myopathy, congenital, with fiber-type disproportion; Myopathy, congenital, with fiber-type disproportion 1<br>Muscular dystrophy, rigid spine, 1|AD/AR<br>AR|2111|91.4%|
|SGCB|NM_000232.4|Muscular dystrophy, limb-girdle, autosomal recessive 4|AR|1113|97.3%|
|SNTA1|NM_003098.3|Long QT syndrome 12|AD|1726|93.5%|
|SOS1|NM_005633.3|?Fibromatosis, gingival, 1<br>Noonan syndrome 4|AD<br>AD|4600|99.6%|
|TGFBR1|NM_004612.4|{Multiple self-healing squamous epithelioma, susceptibility to}<br>Loeys-Dietz syndrome 1|AD<br>AD|1746|93.0%|
|TRDN|NM_006073.4|Cardiac arrhythmia syndrome, with or without skeletal muscle weakness|AR|3256|91.2%|
|TTN|NM_001267550.2|Tibial muscular dystrophy, tardive<br>Myopathy, myofibrillar, 9, with early respiratory failure<br>Cardiomyopathy, dilated, 1G<br>Cardiomyopathy, familial hypertrophic, 9<br>Muscular dystrophy, limb-girdle, autosomal recessive 10<br>Salih myopathy|AD<br>AD<br>AD<br>AD<br>AR<br>AR|117388|99.3%|

(1) bp = basepar; + 26 bp = -20 og + 6 bp i intron for å dekke konservert spleisesete (basert på Refseqs fra UCSC refGene tabell av mars 2015, GRCh37/hg19)
(2) Andel sekvens lest minst 20 ganger

### Sekvensområder lest mindre enn 20 ganger ved HTS:

|Startposisjon (HGVSg)|Stopposisjon (HGVSg)|Gen/Region|Transkript|Ekson|x dekning|
|---|---|---|---|---|---|
|chr12:g.21971186N>N|chr12:g.21971205N>N|ABCC9|NM_005691.3|exon30|18|
|chr7:g.91621451N>N|chr7:g.91621470N>N|AKAP9|NM_005751.4|exon4|15|
|chr7:g.91645425N>N|chr7:g.91645448N>N|AKAP9|NM_005751.4|exon11|19|
|chr7:g.91645451N>N|chr7:g.91645587N>N|AKAP9|NM_005751.4|exon11|4|
|chr7:g.91706148N>N|chr7:g.91706161N>N|AKAP9|NM_005751.4|exon29|18|
|chr7:g.91706165N>N|chr7:g.91706187N>N|AKAP9|NM_005751.4|exon29|16|
|chr2:g.21266762N>N|chr2:g.21266768N>N|APOB|NM_000384.3|exon1|17|
|chr2:g.21266782N>N|chr2:g.21266783N>N|APOB|NM_000384.3|exon1|18|
|chr2:g.21266787N>N|chr2:g.21266789N>N|APOB|NM_000384.3|exon1|19|
|chr2:g.21266809N>N|chr2:g.21266811N>N|APOB|NM_000384.3|exon1|18|
|chr7:g.140485482N>N|chr7:g.140485628N>N|BRAF|NM_001374258.1|exon10|0|
|chr7:g.81642770N>N|chr7:g.81642846N>N|CACNA2D1|NM_000722.4|exon14|5|
|chr9:g.137534015N>N|chr9:g.137534023N>N|COL5A1|NM_000093.5|exon1|18|
|chr9:g.137534085N>N|chr9:g.137534148N>N|COL5A1|NM_000093.5|exon1|8|
|chr16:g.30913481N>N|chr16:g.30913713N>N|CTF1|NM_001330.3|exon3|0|
|chr7:g.153750047N>N|chr7:g.153750089N>N|DPP6|NM_130797.4|exon1|3|
|chr7:g.154263906N>N|chr7:g.154263931N>N|DPP6|NM_130797.4|exon5|17|
|chr18:g.28662197N>N|chr18:g.28662202N>N|DSC2|NM_024422.6|exon9|17|
|chr15:g.48892419N>N|chr15:g.48892451N>N|FBN1|NM_000138.4|exon5|9|
|chr15:g.73660110N>N|chr15:g.73660170N>N|HCN4|NM_005477.3|exon1|0|
|chr20:g.42788779N>N|chr20:g.42788858N>N|JPH2|NM_020433.4|exon2|2|
|chr7:g.150655498N>N|chr7:g.150655521N>N|KCNH2|NM_000238.4|exon4|9|
|chr7:g.150655522N>N|chr7:g.150655523N>N|KCNH2|NM_000238.4|exon4|19|
|chr7:g.150655525N>N|chr7:g.150655526N>N|KCNH2|NM_000238.4|exon4|19|
|chr11:g.2466308N>N|chr11:g.2466310N>N|KCNQ1|NM_000218.3|exon1|18|
|chr11:g.2466457N>N|chr11:g.2466528N>N|KCNQ1|NM_000218.3|exon1|4|
|chr18:g.19399435N>N|chr18:g.19399467N>N|MIB1|NM_020774.3|exon12|12|
|chr6:g.76538236N>N|chr6:g.76538336N>N|MYO6|NM_004999.4|exon4|11|
|chr6:g.76540112N>N|chr6:g.76540118N>N|MYO6|NM_004999.4|exon5|19|
|chr6:g.76540119N>N|chr6:g.76540121N>N|MYO6|NM_004999.4|exon5|19|
|chr6:g.76550910N>N|chr6:g.76550939N>N|MYO6|NM_004999.4|exon9|12|
|chr6:g.76608119N>N|chr6:g.76608134N>N|MYO6|NM_004999.4|exon30|18|
|chr9:g.139440257N>N|chr9:g.139440258N>N|NOTCH1|NM_017617.5|exon1|18|
|chr7:g.151262425N>N|chr7:g.151262489N>N|PRKAG2|NM_016203.4|exon13|13|
|chr7:g.151267250N>N|chr7:g.151267331N>N|PRKAG2|NM_016203.4|exon10|8|
|chr17:g.66521032N>N|chr17:g.66521055N>N|PRKAR1A|NM_002734.4|exon6|10|
|chr19:g.39055808N>N|chr19:g.39056033N>N|RYR1|NM_000540.3|exon91|0|
|chr1:g.237957146N>N|chr1:g.237957164N>N|RYR2|NM_001035.3|exon95|14|
|chr19:g.35521759N>N|chr19:g.35521770N>N|SCN1B|NM_001037.5|exon1|11|
|chr1:g.26126701N>N|chr1:g.26126882N>N|SELENON|NM_020451.3|exon1|0|
|chr4:g.52894266N>N|chr4:g.52894285N>N|SGCB|NM_000232.4|exon5|17|
|chr4:g.52904432N>N|chr4:g.52904443N>N|SGCB|NM_000232.4|exon1|18|
|chr20:g.32031207N>N|chr20:g.32031246N>N|SNTA1|NM_003098.3|exon1|6|
|chr20:g.32031373N>N|chr20:g.32031446N>N|SNTA1|NM_003098.3|exon1|8|
|chr2:g.39237855N>N|chr2:g.39237864N>N|SOS1|NM_005633.3|exon15|18|
|chr2:g.39241899N>N|chr2:g.39241907N>N|SOS1|NM_005633.3|exon11|12|
|chr9:g.101867467N>N|chr9:g.101867590N>N|TGFBR1|NM_004612.4|exon1|0|
|chr6:g.123576219N>N|chr6:g.123576227N>N|TRDN|NM_006073.4|exon37|17|
|chr6:g.123576267N>N|chr6:g.123576290N>N|TRDN|NM_006073.4|exon37|17|
|chr6:g.123581784N>N|chr6:g.123581791N>N|TRDN|NM_006073.4|exon34|18|
|chr6:g.123586456N>N|chr6:g.123586503N>N|TRDN|NM_006073.4|exon33|13|
|chr6:g.123592277N>N|chr6:g.123592351N>N|TRDN|NM_006073.4|exon30|12|
|chr6:g.123594475N>N|chr6:g.123594528N>N|TRDN|NM_006073.4|exon28|9|
|chr6:g.123599456N>N|chr6:g.123599512N>N|TRDN|NM_006073.4|exon26|6|
|chr6:g.123658757N>N|chr6:g.123658761N>N|TRDN|NM_006073.4|exon22|18|
|chr6:g.123699054N>N|chr6:g.123699063N>N|TRDN|NM_006073.4|exon17|14|
|chr6:g.123833441N>N|chr6:g.123833445N>N|TRDN|NM_006073.4|exon7|19|
|chr2:g.179511784N>N|chr2:g.179511788N>N|TTN|NM_001267550.2|exon215|19|
|chr2:g.179518341N>N|chr2:g.179518419N>N|TTN|NM_001267550.2|exon196|9|
|chr2:g.179519465N>N|chr2:g.179519575N>N|TTN|NM_001267550.2|exon191|1|
|chr2:g.179519632N>N|chr2:g.179519675N>N|TTN|NM_001267550.2|exon190|10|
|chr2:g.179522465N>N|chr2:g.179522474N>N|TTN|NM_001267550.2|exon188|19|
|chr2:g.179522481N>N|chr2:g.179522514N>N|TTN|NM_001267550.2|exon188|12|
|chr2:g.179522601N>N|chr2:g.179522642N>N|TTN|NM_001267550.2|exon187|13|
|chr2:g.179523005N>N|chr2:g.179523102N>N|TTN|NM_001267550.2|exon185|2|
|chr2:g.179523725N>N|chr2:g.179523835N>N|TTN|NM_001267550.2|exon182|3|
|chr2:g.179523892N>N|chr2:g.179524002N>N|TTN|NM_001267550.2|exon181|6|
|chr2:g.179526698N>N|chr2:g.179526775N>N|TTN|NM_001267550.2|exon179|7|
|chr2:g.179526862N>N|chr2:g.179526903N>N|TTN|NM_001267550.2|exon178|9|
|chr2:g.179527286N>N|chr2:g.179527363N>N|TTN|NM_001267550.2|exon176|6|
