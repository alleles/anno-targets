| Position | Type | Score | Length | inDB count (exact / overlapping) | Genes | Consequences |
| :-----| :----:| :----:| :----:| :----:| :-----| :----- |
| 16:4386827-4401337 | DUP | 9.1 | 14510 | 0 / 0 | CORO7, CORO7-PAM16, GLIS2, PAM16 | coding_sequence_variant, 3_prime_UTR_variant | coding_sequence_variant, 3_prime_UTR_variant, intron_variant | downstream_gene_variant | transcript_amplification |
| 14:50100329-50101843 | DUP | 8.9 | 1514 | 0 / 0 | DNAAF2 | 5_prime_UTR_variant | feature_elongation, coding_sequence_variant |
