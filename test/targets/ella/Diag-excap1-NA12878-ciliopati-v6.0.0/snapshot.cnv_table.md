| Position | Type | Score | Length | inDB count (exact / overlapping) | Genes | Consequences |
| :-----| :----:| :----:| :----:| :----:| :-----| :----- |
| 3:159998439-160095336 | DUP | **17.9** | 96897 | 0 / 1 | IFT80, TRIM59-IFT80 | feature_elongation, coding_sequence_variant, 5_prime_UTR_variant, intron_variant | feature_elongation, coding_sequence_variant, intron_variant |
| 3:180325423-180377566 | DUP | **17.7** | 52143 | 0 / 1 | CCDC39, TTC14 | coding_sequence_variant, 3_prime_UTR_variant, intron_variant |
| 12:88500643-88532955 | DUP | **16.9** | 32312 | 0 / 0 | CEP290, TMTC3 | feature_elongation, coding_sequence_variant, intron_variant | upstream_gene_variant |
| 9:26984297-27061103 | DUP | **14.6** | 76806 | 0 / 0 | IFT74, LRRC19 | coding_sequence_variant, 3_prime_UTR_variant, intron_variant | feature_elongation, coding_sequence_variant, intron_variant | transcript_amplification |
| 4:170482693-170510723 | DUP | 8.1 | 28030 | 0 / 0 | NEK1 | feature_elongation, coding_sequence_variant, intron_variant |
| 3:121509012-121527896 | DUP | 7.5 | 18884 | 1 / 1 | IQCB1 | feature_elongation, coding_sequence_variant, intron_variant |

