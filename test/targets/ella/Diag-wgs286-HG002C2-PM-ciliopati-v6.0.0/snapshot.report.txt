# Sample information


## Diag-wgs286-HG002C2-PM

| Sequencing info    |               |
|:-------------------|:-------------:|
| Sequencer ID       | E00401 |
| Capture kit        | wgs  |


## Diag-wgs286-HG004B3-MK

| Sequencing info    |               |
|:-------------------|:-------------:|
| Sequencer ID       | E00401 |
| Capture kit        | wgs  |


## Diag-wgs286-HG003B2-FM

| Sequencing info    |               |
|:-------------------|:-------------:|
| Sequencer ID       | E00401 |
| Capture kit        | wgs  |



# Pipeline versions





# Analysis supplement

## mtDNA

**Haplogroup** H5a7


 | Variant annotation |  |  |  | 
 |:--- | ---:| ---:| ---:|
 | **POS** | 4769 | 6800 | 8860 | 
 | **REF** | A | A | A | 
 | **ALT** | C | AG | T | 
 | **FILTER** | PASS | PASS | PASS | 
 | **MITOMAPCPM__Associated_diseases** |  |  |  | 
 | **CLINVAR_CLINSIG** |  |  |  | 
 | **VEP_Consequence** | missense_variant | frameshift_variant | missense_variant | 
 | **VEP_SYMBOL** | ND2 | MT-CO1 | MT-ATP6 | 
 | **VEP_HGVSc** | ND2.1:c.300A>C | ENST00000361624.2:c.898dup | ENST00000361899.2:c.334A>T | 
 | **VEP_HGVSp** | YP_003024027.1:p.Met100Ile | ENSP00000354499.2:p.Asp300GlyfsTer? | ENSP00000354632.2:p.Thr112Ser | 
 | **VEP_HGVSg** | MT:m.4769A>C | MT:m.6801dup | MT:m.8860A>T | 
 | **AD_Proband** | [0, 11132] | [3, 11220] | [0, 11103] | 
 | **AF_Proband** | 1 | 1 | 1 | 
 | **DP_Proband** | 11132 | 11223 | 11103 | 
 | **AD_Mother** | [0, 11113] | [4, 11141] | [0, 11164] | 
 | **AF_Mother** | 1 | 1 | 1 | 
 | **DP_Mother** | 11113 | 11145 | 11164 | 
 | **HELIXMTDB__Het** | 0 |  |  | 
 | **HELIXMTDB__Hom** | 1 |  |  | 
 | **GNOMAD_MT__AC_Het** | 0 |  |  | 
 | **GNOMAD_MT__AC_Hom** | 1 |  |  | 


