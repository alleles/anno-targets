#!/usr/bin/env bash

# The VCF must be stripped of some information to make it comparable across runs:
#   - Remove CSQ from INFO field for each record
#   - Keep headers CHROM, INFO, FORMAT and FILTER headers (but excluding CSQ and RefSeq_gff specs)
#   - Strip VEP information headers
#   - Strip MT-related information headers until MT data sets are handled properly
#   - Strip data versions, so that data updates only affect the variants (if anything at all)
function strip_vcf() {
    sed "s/CSQ=[^; ]*//" "${1}" \
        | grep -P "^([1-9XY]|#CHROM|##INFO|##FORMAT|##FILTER)" \
        | grep -v -e "^##" \
        | awk '{
            if ($1 ~ "inDB|HGMD|GNOMAD|CLINVARJSON") {
                gsub("\\(?(from.*\">)", "\">", $0)
                gsub(",Version=.*>", ">", $0)
                print
            }
            else print
        }'
}
