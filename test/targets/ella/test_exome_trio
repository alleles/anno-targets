#!/bin/bash

set -euf -o pipefail

THISDIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
DATADIR="${THISDIR}/../../testdata"

export LS_OUTPUT=/dev/stderr
source "${THISDIR}"/../../../thirdparty/log.sh

type -t on_error >/dev/null && ERROR_FUN=on_error || ERROR_FUN="echo"
type -t on_exit >/dev/null && EXIT_FUN=on_exit || EXIT_FUN="echo"
trap '${ERROR_FUN} "[${LINENO}: ${BASH_COMMAND}]"' ERR
trap '${EXIT_FUN} "Test exited prematurely"' EXIT

SCRATCH="$(realpath "${SCRATCH:-/scratch}")"

export GP_NAME="ciliopati"
export GP_VERSION="v6.0.0"
export SAMPLE_ID="Diag-excap136-HG002C2-PM"
export SEQUENCER_ID="E00401"

export ANALYSIS_NAME="${SAMPLE_ID}-${GP_NAME}-${GP_VERSION}"
export ANALYSIS_REQUESTED_DATE=2024-02-24
export CAPTUREKIT="agilent_sureselect_v05"
export GENDER=male
export FATHER_SAMPLE_ID="Diag-excap136-HG003B2-FM"
export FATHER_SEQUENCER_ID="E00401"
export MOTHER_SAMPLE_ID="Diag-excap136-HG004B3-MK"
export MOTHER_SEQUENCER_ID="E00401"
export PRIORITY=1
export SPECIALIZED_PIPELINE=""
export TARGETS_OUT="${SCRATCH}/output"
export TYPE="trio"

if [[ -z "$(ls -A "${SCRATCH}")" ]]; then
    mkdir -p "${TARGETS_OUT}"
    mkdir -p "${SCRATCH}/workdir"

    # All references to data need to be relative to mount inside container
    VCF="${DATADIR}/${TYPE}/${SAMPLE_ID}/${SAMPLE_ID}-TRIO.vcf"
    REGIONS="${DATADIR}/clinicalGenePanels/${GP_NAME}_${GP_VERSION}/${GP_NAME}_${GP_VERSION}_genes_transcripts.tsv"
    export ANNO_CONFIG_PATH=${THISDIR}/anno_global_config.json
    export BAM="${DATADIR}/${TYPE}/${SAMPLE_ID}/${SAMPLE_ID}.${GP_NAME}_${GP_VERSION}_exons_slop20.bam"
    export CNV_VCF="${DATADIR}/${TYPE}/${SAMPLE_ID}/${SAMPLE_ID}.cnv.bed"
    export WES_CNV_INHOUSE_DB="${DATADIR}/indb/Diag-excap1-${GP_NAME}-${GP_VERSION}-fake-inhousedb.bed.gz"
    export EXON_REGIONS="${DATADIR}/clinicalGenePanels/${GP_NAME}_${GP_VERSION}/${GP_NAME}_${GP_VERSION}_regions.bed"
    export FATHER_BAM="${DATADIR}/${TYPE}/${SAMPLE_ID}/${FATHER_SAMPLE_ID}.${GP_NAME}_${GP_VERSION}_exons_slop20.bam"
    export MOTHER_BAM="${DATADIR}/${TYPE}/${SAMPLE_ID}/${MOTHER_SAMPLE_ID}.${GP_NAME}_${GP_VERSION}_exons_slop20.bam"
    export PHENOTYPES="${DATADIR}/clinicalGenePanels/${GP_NAME}_${GP_VERSION}/${GP_NAME}_${GP_VERSION}_phenotypes.tsv"
    export REPORT_CONFIG="${DATADIR}/clinicalGenePanels/${GP_NAME}_${GP_VERSION}/report_config.txt"
    export TRANSCRIPTS="${DATADIR}/clinicalGenePanels/${GP_NAME}_${GP_VERSION}/${GP_NAME}_${GP_VERSION}_genes_transcripts.tsv"

    annotate_with_target \
        --workdir "${SCRATCH}/workdir" \
        --vcf "${VCF}" \
        --regions "${REGIONS}" \
        --target ella
else
    LSWARNING "Using data in scratch folder. Will not run anno."
fi

# Clean up stupid JAVA littering all over the place...
find "${SCRATCH}" -type d -name "?" -print0 | xargs -0 rm -rf {}

###########################
# Start checking results
###########################

# To simplify testing, we use snapshot tests where appropriate to make sure no unintended changes are introduced.
# If you're making changes, *validate the results* and update snapshots accordingly

#
# Check output files
#

pushd "${TARGETS_OUT}" >/dev/null
find . | sort -f >../file-list.txt
popd >/dev/null

set +e
RESULT_OUTPUT=$(
    diff "${SCRATCH}/file-list.txt" "${THISDIR}/${ANALYSIS_NAME}/snapshot.files.txt" 2>&1
)
set -e
if [[ -n "${RESULT_OUTPUT}" ]]; then
    LSERROR "Output files didn't match expected snapshot"
    LSERROR "\n${RESULT_OUTPUT}"
    exit 1
else
    LSINFO "PASS: Output files matched expected snapshot"
fi

#
# Check VCF
#

VCF_OUT="${TARGETS_OUT}/ella/${ANALYSIS_NAME}/${ANALYSIS_NAME}.vcf"

set +e
RESULT_OUTPUT=$(
    bcftools query -f '%CHROM\t%POS\t%REF\t%ALT\t%FILTER\n' "${VCF_OUT}" | sort -k 1,1V -k 2,2n -k 3 \
        | diff "${THISDIR}/${ANALYSIS_NAME}/snapshot.vcf.txt" - 2>&1
)
set -e
if [[ -n "${RESULT_OUTPUT}" ]]; then
    LSERROR "The output VCF file didn't match the expected snapshot"
    LSERROR "\n${RESULT_OUTPUT}"
    exit 1
else
    LSINFO "PASS: The output VCF file matched the expected snapshot"
fi

#
# CNV table in report.txt
#

# Check that our snapshot CNV table is added verbatim somewhere in the report.txt using some grep magic.
# Replace newlines with '.'s and use z to treat line breaks as any other character.
CNV_TABLE_MD=$(
    tr '\n*' . <"${THISDIR}/${ANALYSIS_NAME}/snapshot.cnv_table.md"
)
set +e
RESULT_OUTPUT=$(
    grep -zl "${CNV_TABLE_MD}" "${SCRATCH}/output/ella/${ANALYSIS_NAME}/report.txt"
)
set -e
if [[ -z ${RESULT_OUTPUT} ]]; then
    LSERROR "CNV table snapshot not found in report.txt"
    exit 1
else
    LSINFO "PASS: CNV table snapshot found in report.txt"
fi
