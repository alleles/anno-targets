### Gener/Regioner med dekningsgrad under 100%:

|Gen/Region|Transkript|Sykdom|Arvegang|Antall kodende<br> bp + 26 bp (1)|Dekningsgrad<br> (% bp) (2)|
|---|---|---|---|---|---|
|PMS2|NM_000535.7|Lynch syndrome 4<br>Mismatch repair cancer syndrome 4|<br>AR|2979|97.3%|

(1) bp = basepar; + 26 bp = -20 og + 6 bp i intron for å dekke konservert spleisesete (basert på Refseqs fra UCSC refGene tabell av mars 2015, GRCh37/hg19)
(2) Andel sekvens lest minst 20 ganger

### Sekvensområder lest mindre enn 20 ganger ved HTS:

|Startposisjon (HGVSg)|Stopposisjon (HGVSg)|Gen/Region|Transkript|Ekson|x dekning|
|---|---|---|---|---|---|
|chr5:g.112042989N>N|chr5:g.112043601N>N|APC promotor 1|N/A|N/A|0|
|chr5:g.112072689N>N|chr5:g.112073591N>N|APC promotor 2|N/A|N/A|0|
|chr7:g.6013113N>N|chr7:g.6013193N>N|PMS2|NM_000535.7|exon15|2|
