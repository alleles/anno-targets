#!/bin/bash
set -euf -o pipefail

# Annotate either a vcf file (CNV_VCF) or a bed file (CNV) by converting it to a vcf file first

echo -e "\n#### MODULE: Annotate cnv-file"

if [[ -f "${CNV:-}" && -f "${CNV_VCF:-}" ]]; then
    LSERROR "Both \$CNV and \$CNV_VCF is defined"
    exit 1
fi

if [[ -f "${CNV_VCF:-}" && ${ANALYSIS_TYPE} == "Exome" ]]; then
    annotation_cnv_bed_to_vcf "${CNV_VCF}" tmp_cnv_converted.vcf
    CNV_VCF=tmp_cnv_converted.vcf
fi

if [[ ! -f "${CNV_VCF:-}" ]]; then
    exit 0
fi

echo -e "Filter CNVs by REGIONS"

CMD="bedtools intersect -header -wa -a ${CNV_VCF} -b ${REGIONS} -u"
echo -e "${CMD}"
${CMD} >tmp_cnv_filtered.vcf

echo -e "Annotate filtered CNVs"

# HACK: If there are no variants in the vcf, VEP doesn't write anything..
if ! grep -vq '^#' tmp_cnv_filtered.vcf; then
    grep '^#' tmp_cnv_filtered.vcf >tmp_vep_annotated_cnv.vcf
else
    CMD="vep_offline
    --fasta ${FASTA}
    --force_overwrite
    --hgvs
    --numbers
    --domains
    --regulatory
    --canonical
    --pubmed
    --symbol
    --allow_non_variant
    --fork=4
    --vcf
    --allele_number
    --no_escape
    --failed=1
    --no_stats
    --refseq
    --shift_hgvs=1
    -i tmp_cnv_filtered.vcf
    -o tmp_vep_annotated_cnv.vcf"

    echo -e "${CMD}"
    ${CMD}
fi

cat >tmp_scripts.lua <<EOL

-- The annotated BED is only by overlap.
-- We want to check that the positions are exact matches

function postprocess_compare(indb_start, indb_end, indb_type, svtype)
    if not indb_start or not indb_end or not indb_type then
        return 0
    end

    -- when "concat", vcfanno strangely returns table if
    -- multiple values, but string if just a single value
    if type(indb_start) ~= "table" then
        indb_start = {indb_start}
    end
    if type(indb_end) ~= "table" then
        indb_end = {indb_end}
    end
    if type(indb_type) ~= "table" then
        indb_type = {indb_type}
    end

    local count = 0
    for i=1, #indb_start do
        local indb_start_val = tonumber(indb_start[i])
        local indb_end_val = tonumber(indb_end[i])
        local indb_type_val = indb_type[i]
        if indb_start_val == start and
           indb_end_val == stop and
           ((indb_type_val == "1" and svtype == "DEL") or
            (indb_type_val == "3" and svtype == "DUP")) then
            count = count + 1
        end

    end
    return count
end

EOL

cat >tmp_cnv_vcfannoconf.toml <<EOL
[[annotation]]
file="${WES_CNV_INHOUSE_DB}"
columns=[2, 3, 4, 1]
names=["indb_start", "indb_end", "indb_type", "indb_overlap_count"]
ops=["concat", "concat", "concat", "count"]

[[postannotation]]
fields=["indb_start", "indb_end", "indb_type", "SVTYPE"]
op="lua: postprocess_compare(indb_start, indb_end, indb_type, SVTYPE)"
name="inDB_identical_count"
type="Integer"

EOL

CMD="vcfanno -lua tmp_scripts.lua tmp_cnv_vcfannoconf.toml tmp_vep_annotated_cnv.vcf"
echo -e "${CMD}"
${CMD} 2> >(grep -v '^bix\.go:.\+chromosome.\+not found in') >"${OUTFILE}"

set +f
rm tmp*
set -f
