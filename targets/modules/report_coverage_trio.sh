#!/bin/bash

set -euf -o pipefail

THISDIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
source "${THISDIR}"/../../thirdparty/log.sh

LSINFO "\n## Trio coverage report ##\n"

LSINFO "DEPENDENCIES:"
LSINFO "ORDER_ID: ${ORDER_ID}"
LSINFO "MOTHER_ORDER_ID: ${MOTHER_ORDER_ID}"
LSINFO "FATHER_ORDER_ID: ${FATHER_ORDER_ID}"
LSINFO "COVERAGE_DEPTH_TRESHOLD: ${COVERAGE_DEPTH_THRESHOLD}"
LSINFO "REPORT_CONFIG: ${REPORT_CONFIG}"
LSINFO "TRANSCRIPTS: ${TRANSCRIPTS}"
LSINFO "PHENOTYPES: ${PHENOTYPES}"
# shellcheck disable=SC2153
LSINFO "GP_VERSION: ${GP_VERSION}"
LSINFO "ANALYSIS_NAME: ${ANALYSIS_NAME}"
LSINFO "OUTFILE: ${OUTFILE}"

CMD="report_coverage_trio
    --coverage-transcript-proband ${ORDER_ID}_coverageReport_transcript
    --coverage-transcript-mother ${MOTHER_ORDER_ID}_coverageReport_transcript
    --coverage-transcript-father ${FATHER_ORDER_ID}_coverageReport_transcript
    --config ${REPORT_CONFIG}
    --transcripts ${TRANSCRIPTS}
    --title ${ORDER_ID}
    --minimal-no-reads ${COVERAGE_DEPTH_THRESHOLD}
    --gp-suffix ${GP_VERSION}
    -o ${OUTFILE}"

echo -e "${CMD}"
${CMD}
