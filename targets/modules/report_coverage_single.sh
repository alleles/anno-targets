#!/bin/bash

set -euf -o pipefail

THISDIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
source "${THISDIR}"/../../thirdparty/log.sh

LSINFO "\n#### MODULE: Create coverage report"

LSINFO "Output as: ${OUTPUT}"
LSINFO "Output to: ${OUTFILE}"

LSINFO "DEPENDENCIES:"
LSINFO "ORDER_ID: ${ORDER_ID}"
LSINFO "COVERAGE_DEPTH_TRESHOLD: ${COVERAGE_DEPTH_THRESHOLD}"
LSINFO "REPORT_CONFIG: ${REPORT_CONFIG}"
LSINFO "TRANSCRIPTS: ${TRANSCRIPTS}"
LSINFO "PHENOTYPES: ${PHENOTYPES}"
# shellcheck disable=SC2153
LSINFO "GP_VERSION: ${GP_VERSION}"
LSINFO "OUTFILE: ${OUTFILE}"

CMD="report_coverage_single
    --coverage-transcript ${ORDER_ID}_coverageReport_transcript
    --coverage-low ${ORDER_ID}_lowCoverage.bed
    --config ${REPORT_CONFIG}
    --transcripts ${TRANSCRIPTS}
    --phenotypes ${PHENOTYPES}
    --title ${ORDER_ID}
    --minimal-no-reads ${COVERAGE_DEPTH_THRESHOLD}
    --gp-suffix ${GP_VERSION}
    --coverage-threshold 100
    --low-coverage-exon
    --low-coverage-hgvsg"

if [ "${OUTPUT}" == 'pdf' ]; then
    CMD="${CMD} --pdf ${OUTFILE}"
else
    CMD="${CMD} --markdown ${OUTFILE}"
fi

echo -e "${CMD}"
${CMD}
