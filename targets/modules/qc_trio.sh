#!/bin/bash
set -euf -o pipefail

echo -e "\n#### MODULE: Run trio quality control"

CMD=(gatk SelectVariants
    --exclude-non-variants
    --verbosity WARNING
    -R "${FASTA%%.*}.fasta"
    -V "${ORIGINAL_VCF}"
    -sn "${QC_SAMPLE_ID}"
    -O "tmp_${QC_SAMPLE_ID}.vcf")

echo -e "\nCommand to run:"

echo -e "${CMD[*]}"
"${CMD[@]}"
