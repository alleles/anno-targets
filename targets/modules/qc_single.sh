#!/bin/bash
set -euf -o pipefail
THISDIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
source "${THISDIR}"/../../thirdparty/log.sh

LSINFO "#### MODULE: Run coverage calculation and compute qc on analysis"
LSINFO "Sample: ${QC_SAMPLE_ID}"

# Remove regions from coverage calculation if defined in the config
cp "${EXON_REGIONS}" tmp_exon_regions.bed
exclude_regions=$(jq -r --arg ANALYSIS_TYPE "${ANALYSIS_TYPE}" '.[$ANALYSIS_TYPE].regions.exclude' "${ANALYSIS_TYPE_CONFIG}")
if [[ ${exclude_regions} != "null" ]]; then
    source "${THISDIR}"/../utils.source
    apply_regions "${exclude_regions}" "exclude" tmp_exon_regions.bed
    mv tmp_exon_regions.bed tmp_exon_regions_postexclude.bed
    # apply_regions only outputs the three required bed-columns. Intersect with the original
    # EXON_REGIONS to get all columns (which is required by the python scripts)
    bedtools intersect -a "${EXON_REGIONS}" -b tmp_exon_regions_postexclude.bed >tmp_exon_regions.bed
    rm tmp_exon_regions_postexclude.bed
fi

# Create slop file
CMD="bedtools slop
    -i tmp_exon_regions.bed
    -g ${FASTA%%.*}.chromsizes
    -s
    -l 20
    -r 6"

echo "${CMD}"
${CMD} | sort -k1,1V -k2,2n -k3,3n >tmp_cov_regions_slop.bed
echo ""

# Writes out $QC_SAMPLE_ID_coverageReport[|_transcript|_gene]
LSINFO "Run qc_coverage, depth threshold: ${COVERAGE_DEPTH_THRESHOLD}"

samtools view --threads 4 -u -T "${FASTA}" -ML tmp_cov_regions_slop.bed "${BAM}" -o tmp.slice.bam
samtools index -@ 4 tmp.slice.bam
if [ "${ANALYSIS_TYPE}" = "WGS" ]; then
    MQ=59
else
    MQ=20
fi
LSINFO "Setting MQ value to ${MQ} for ${ANALYSIS_TYPE}"
CMD="sambamba depth base -c 0 -t 4 -F 'mapping_quality > ${MQ} and not (chimeric or unmapped or secondary_alignment or duplicate or failed_quality_control)' -L tmp_cov_regions_slop.bed tmp.slice.bam >\"${QC_SAMPLE_ID}.coverage-per-bp.csv\""
echo "${CMD}"
eval "${CMD}"
# Convert sambamba output into format expected by qc_coverage
tail -n +2 "${QC_SAMPLE_ID}.coverage-per-bp.csv" | awk -v sample="${QC_SAMPLE_ID}" '{ print $1 "\t" $2 "\t" $2 + 1 "\t" sample "\t" $3 "\t+" }' >"${QC_SAMPLE_ID}.coverage-per-bp-conv.bed"

CMD="qc_coverage
     --coveragePerBP ${QC_SAMPLE_ID}.coverage-per-bp-conv.bed
     --minimumcoverage ${COVERAGE_DEPTH_THRESHOLD}
     --samplename ${QC_SAMPLE_ID}
     --regions tmp_cov_regions_slop.bed"
echo -e "${CMD}"
${CMD}

if [[ -n ${COVERAGE_FAILED_REGIONS_THRESHOLDS} ]]; then
    echo "Run low coverage check"
    CMD="low_coverage_check
          --output ${QC_SAMPLE_ID}_low_coverage_regions.json
          --low-coverage ${QC_SAMPLE_ID}_lowCoverage.bed
          --low-coverage-transcripts ${TRANSCRIPTS}
          --depth-threshold ${COVERAGE_DEPTH_THRESHOLD}
          --failed-regions-threshold ${COVERAGE_FAILED_REGIONS_THRESHOLDS}"

    echo -e "${CMD}"
    ${CMD}
else
    LSINFO "No failedCoverageRegions defined. Skipping low coverage check."
fi

rm tmp.slice.bam
rm tmp.slice.bam.bai
rm tmp_cov_regions_slop.bed
rm tmp_exon_regions.bed
rm "${QC_SAMPLE_ID}.coverage-per-bp-conv.bed"
rm "${QC_SAMPLE_ID}.coverage-per-bp.csv"
