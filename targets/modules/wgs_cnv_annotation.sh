#!/bin/bash
set -euf -o pipefail

THISDIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
source "${THISDIR}"/../../thirdparty/log.sh
source "${THISDIR}"/../../src/annotation/cnv/wgs_cnv_func.sh

LSINFO "\n#### MODULE: Annotate and filter sv vcf-file"
#####################
# Global variables: #
#####################
# VCF
# CNV_VCF
# TRANSCRIPTS
# ANNO_DATA=/anno/data
# FASTA
# WGS_CNV_INHOUSE_DB_DIR
# OUTFILE
# OUTFILE_TSV
# OUTFILE_TSV_ALL
# CALLER

# Set cnv callers in order of priority
if [[ ${CALLER} == "Dragen" ]]; then
    CNV_CALLERS="manta canvas"
else
    CNV_CALLERS="manta tiddit delly cnvnator"
fi

LSINFO "Step 1: Remove decoy contigs"
CANONICAL_CHROMS=$(jq '.WGS.svparams.interpretation_groups.canonical' "${ANALYSIS_TYPE_CONFIG}")
LSINFO "Limit ${CNV_VCF} by interpretation_group ${CANONICAL_CHROMS} > tmp_cnv.vcf"

remove_decoys \
    "${CNV_VCF}" \
    "${CANONICAL_CHROMS}" \
    "${CNV_CALLERS}" \
    "$(dirname "${FASTA}")/human_g1k_v37_decoy.fasta.fai" \
    outfile_cnv.vcf
N_VARIANTS=$(cgrep -vc "^#" outfile_cnv.vcf)
LSINFO "Found ${N_VARIANTS} for ${CANONICAL_CHROMS}"

LSINFO "Step 2: Divide CNVs into interpretation groups"
# shellcheck disable=SC2089
INTERPRETATION_GROUP_LARGE=$(jq '.WGS.svparams.interpretation_groups.large' "${ANALYSIS_TYPE_CONFIG}")
# shellcheck disable=SC2089
INTERPRETATION_GROUP_SMALL=$(jq '.WGS.svparams.interpretation_groups.small' "${ANALYSIS_TYPE_CONFIG}")

RESULT_SIZE_SPLIT=$(split_by_size outfile_cnv.vcf \
    "${INTERPRETATION_GROUP_LARGE}" \
    "${INTERPRETATION_GROUP_SMALL}" \
    "${CNV_CALLERS}" \
    tmp_cnv_large.vcf \
    tmp_cnv_small.vcf)
read -r N_LARGE_VARIANTS N_SMALL_VARIANTS <<<"${RESULT_SIZE_SPLIT}"
LSINFO "Found ${N_LARGE_VARIANTS} large variants and ${N_SMALL_VARIANTS} small variants"

LSINFO "Step 3: Filter small CNVs by transcript"
GP_SLOP=$(jq '.WGS.svparams.gene_panel_slop' "${ANALYSIS_TYPE_CONFIG}")
# Variants to include (overlapping genepanel)
filter_by_overlap \
    tmp_cnv_small.vcf \
    "${TRANSCRIPTS}" \
    "${GP_SLOP}" \
    "${FASTA%%.*}.chromsizes" \
    -u -header >tmp_cnv_small_infiltered.vcf
N_SMALL_INFILTERED_VARIANTS=$(cgrep -vc "^#" tmp_cnv_small_infiltered.vcf)
LSINFO "Found ${N_SMALL_INFILTERED_VARIANTS} small variants overlapping genepanel +/-${GP_SLOP}"

# Variants to exclude (not overlapping genepanel)
filter_by_overlap \
    tmp_cnv_small.vcf \
    "${TRANSCRIPTS}" \
    "${GP_SLOP}" \
    "${FASTA%%.*}.chromsizes" \
    -v -header >tmp_cnv_small_outfiltered.vcf
# Excluded variants are identified with FILTER '.' instead of 'PASS'
# In this way, the VCF header does not have to be updated
sed -i.bak 's/	PASS	/	.	/' tmp_cnv_small_outfiltered.vcf
N_SMALL_OUTFILTERED_VARIANTS=$(cgrep -vc "^#" tmp_cnv_small_outfiltered.vcf)
LSINFO "Excluded ${N_SMALL_OUTFILTERED_VARIANTS} small variants not overlapping genepanel +/-${GP_SLOP}"

LSINFO "Step 4: Combine into new, transcript filtered, sorted VCF"
# Combine included and excluded variants
CMD="combine_vcfs tmp_cnv_small_infiltered.vcf tmp_cnv_small_outfiltered.vcf"
LSINFO "${CMD} >tmp_cnv_small_filtered.vcf"
${CMD} >tmp_cnv_small_filtered.vcf

# Combine large and small variants
CMD="combine_vcfs tmp_cnv_large.vcf tmp_cnv_small_filtered.vcf"
LSINFO "${CMD} >outfile_cnv_combo.vcf"
${CMD} >outfile_cnv_combo.vcf
# Sort combined file
CMD="vcf-sort -c outfile_cnv_combo.vcf"
LSINFO "${CMD} >outfile_cnv_filtered.vcf"
${CMD} >outfile_cnv_filtered.vcf

#####################
# VEP annotations
#####################
LSINFO "Step 5: Annotate with VEP"
# Run VEP, sorting and vcf standardizing
annotate_cnv_with_vep \
    outfile_cnv_filtered.vcf \
    "${ANALYSIS_TYPE_CONFIG}" \
    "${ANNO_DATA}" \
    "${FASTA}" \
    "${CNV_CALLERS}" \
    outfile_cnv_vep.vcf

#####################
# Frequency annotations
#####################
LSINFO "Step 6: Annotate with frequency"
# Merged solver-specific variants into one merged file
# Parameters from Scilifelab FindSV
# svdb --merge --notag --pass_only --same_order --no_intra \
# --overlap 0.7 --bnd_distance 2500 \
# --vcf manta.vcf \
# tiddit.vcf \
# delly.vcf \
# cnvnator.vcf
#
# -- OR --
#
# svdb --merge --notag --same_order --no_intra \
# --overlap 0.7 --bnd_distance 2500 \
# --vcf manta.vcf \
# canvas.vcf
annotate_cnv_with_freq \
    "${CALLER}" \
    "${ANNO_DATA}" \
    outfile_cnv_vep.vcf \
    outfile_cnv_svdb.vcf
LSINFO "... finished frequency annotation by SVDB"

#####################
# AnnotSV annotations
#####################
LSINFO "Step 7: Run AnnotSV"
annotate_with_annotsv \
    outfile_cnv_svdb.vcf \
    "${ANNO_DATA}" \
    "${VCF}" \
    outfile_cnv_annotsv.vcf

#######################
# Annotate with vcfanno
#######################
LSINFO "Step 8: Run vcfanno"
# vcfanno performance parameters are set globally in ella-anno docker
CMD="vcfanno \
        -base-path ${ANNO_DATA} \
        ${ANNO_DATA}/vcfanno_config.toml \
        outfile_cnv_annotsv.vcf"

eval "${CMD}" 2> >(grep -v '^bix\.go:.\+chromosome.\+not found in') >outfile_cnv_vcfanno.vcf

#############################
# Annotate with liftOver
#############################
LSINFO "Step 9: Run liftOver"
annotate_with_liftover \
    outfile_cnv_vcfanno.vcf \
    "${ANNO_DATA}" \
    outfile_cnv_liftover.vcf

#############################
# Addional quality filtering
#############################
# 1. For callers that prioritize sensitivity over precision,
# add filters for low quality variants (if not already done in basepipe)
# 2. Also add quality filter based on annotations
LSINFO "Step 10: Add additional Quality filters"
add_quality_filters \
    outfile_cnv_liftover.vcf \
    "${ANALYSIS_TYPE_CONFIG}" \
    "${CNV_CALLERS}" \
    outfile_cnv_addqc.vcf

###################
# Required output #
###################
LSINFO "Step 11: Apply all filters and create outputs"
# Keep only low frequency variants
# Frequency filters based on the caller specific inhouse databases and SweGen are applied for the most prioritised caller
# In addition there is the "common" database GnomAD that is applied even if there are no other databases present
INTERPRETATION_GROUP_CNV="$(jq '.WGS.svparams.interpretation_groups.ella' "${ANALYSIS_TYPE_CONFIG}")"
FREQUENCY_FILTERS="$(jq '.WGS.svdb.criteria' "${ANALYSIS_TYPE_CONFIG}")"
RESCUE_FILTERS="$(jq '.WGS.svdb.exceptions' "${ANALYSIS_TYPE_CONFIG}")"

apply_all_filters \
    outfile_cnv_addqc.vcf \
    "${INTERPRETATION_GROUP_CNV}" \
    "${FREQUENCY_FILTERS}" \
    "${RESCUE_FILTERS}" \
    "${CNV_CALLERS}" \
    --output-format vcf >"${OUTFILE}"

# Make TSV file to look at in Excel, as easy lookup in IGV, for bedtools annotation etc
# shellcheck disable=SC2086
sv_wgs_filtering \
    "${OUTFILE}" \
    --caller-priority ${CNV_CALLERS} \
    --output-format tsv >"${OUTFILE_TSV}"

# Additional TSV file for manual inspection of high frequency cases
# Set quality filters that are to be debugged
# Include high frequency variants by `--debug`-flag
INTERPRETATION_GROUP_CNV_DEBUG="$(jq '.WGS.svparams.interpretation_groups.ella_debug' "${ANALYSIS_TYPE_CONFIG}")"
# shellcheck disable=SC2086

apply_all_filters \
    outfile_cnv_addqc.vcf \
    "${INTERPRETATION_GROUP_CNV_DEBUG}" \
    "${FREQUENCY_FILTERS}" \
    "${RESCUE_FILTERS}" \
    "${CNV_CALLERS}" \
    --debug --output-format tsv >"${OUTFILE_TSV_ALL}"
