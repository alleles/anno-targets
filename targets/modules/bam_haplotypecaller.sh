#!/bin/bash
set -euf -o pipefail
THISDIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
source "${THISDIR}"/../../thirdparty/log.sh

LSINFO "#### MODULE: Create HaplotypeCaller bam files"

mkdir -p "${OUTPUT_DIR}"

mkdir tmp_hc_bam

bedtools slop \
    -i "${SLICE_REGIONS}" \
    -g "${FASTA%%.*}.chromsizes" \
    -b "${SLOP_SIZE}" \
    >tmp_hc_bam/tmp_hc_regions_slop.bed

samtools view --threads 4 -u -T "${FASTA%%.*}.fasta" -ML tmp_hc_bam/tmp_hc_regions_slop.bed "${BAM_TO_CALL}" -o tmp_hc_bam/tmp.slice.bam
samtools index -@ 4 tmp_hc_bam/tmp.slice.bam

LSINFO "Run haplotypecaller"
CMD=(gatk HaplotypeCaller
    --verbosity WARNING
    -R "${FASTA%%.*}.fasta"
    -I tmp_hc_bam/tmp.slice.bam
    --max-alternate-alleles 3
    --read-filter OverclippedReadFilter
    -new-qual
    -bamout tmp_hc_bam/tmp_haplotypeCaller.bam
    --bam-writer-type CALLED_HAPLOTYPES
    -L tmp_hc_bam/tmp_hc_regions_slop.bed
    -O tmp_hc_bam/tmp_dummy.vcf)

echo -e "${CMD[*]}"
"${CMD[@]}"

LSINFO "Print reads from haplotypecaller"
CMD=(gatk PrintReads
    --verbosity WARNING
    -R "${FASTA%%.*}.fasta"
    -I tmp_hc_bam/tmp_haplotypeCaller.bam
    -O "${OUTPUT_DIR}/${TARGET_NAME}.bam"
    --read-filter ReadGroupBlackListReadFilter
    --read-group-black-list RG:ArtificialHaplotype)

echo -e "${CMD[*]}"
"${CMD[@]}"

rm -r tmp_hc_bam
