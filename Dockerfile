ARG ELLA_ANNO_IMAGE_NAME=local/ella-anno-base
ARG ELLA_ANNOBUILDER_IMAGE_NAME=${ELLA_ANNO_IMAGE_NAME:-local/ella-annobuilder-base}

FROM ${ELLA_ANNOBUILDER_IMAGE_NAME} AS builder

ARG MAINTAINER=${MAINTAINER:-OUS\ AMG\ <ella-support@medisin.uio.no>}
LABEL maintainer="${MAINTAINER}"

USER root

RUN apt-get update && apt-get install -y --no-install-recommends mariadb-server
RUN mkdir /run/mysqld/ && chown mysql:mysql /run/mysqld
RUN passwd -d root
COPY builder-entrypoint.sh /builder-entrypoint.sh

# inherited from ella-anno Dockerfile
USER ${ANNO_USER}

WORKDIR /anno

ENTRYPOINT ["/builder-entrypoint.sh"]

###

ARG ELLA_ANNO_IMAGE_NAME=local/ella-anno-base

FROM ${ELLA_ANNO_IMAGE_NAME} AS prod

ENV DEBIAN_FRONTEND=noninteractive \
    LANGUAGE=C.UTF-8 \
    LANG=C.UTF-8 \
    LC_ALL=C.UTF-8 \
    PATH=/anno/bin:$PATH \
    PERL5LIB=/anno/thirdparty/ensembl-vep-release/:/anno/thirdparty/vcftools/lib

RUN echo 'Acquire::ForceIPv4 "true";' | tee /etc/apt/apt.conf.d/99force-ipv4

RUN apt-get update && \
    apt-get install -y \
        openssh-client \
        python3-venv \
        tcl \
        tcllib && \
    echo "Cleanup:" && \
    apt-get clean && \
    apt-get autoclean && \
    rm -rf /var/lib/apt/lists/* && \
    rm -rf /usr/share/doc/* /usr/share/man/* /usr/share/groff/* /usr/share/info/* /tmp/* && \
    rm -rf /var/cache/apt/* /root/.cache

RUN mkdir -p /dist/bin/ /dist/thirdparty/ && \
    chown ${ANNO_USER}:${ANNO_USER} -R /dist

RUN mkdir -p /home/${ANNO_USER}/.vscode-server/extensions && \
    chown -R ${ANNO_USER}:${ANNO_USER} /home/${ANNO_USER}

RUN wget -O /usr/local/bin/shfmt \
    https://github.com/mvdan/sh/releases/download/v3.5.1/shfmt_v3.5.1_linux_amd64 && \
    chmod a+x /usr/local/bin/shfmt

USER ${ANNO_USER}

# Install OpenJDK Java version 8 JRE (not provided anymore by this Debian version)
ENV JDK_REL 8u242
ENV JDK_TAG b08
ENV JDK_URL github.com/AdoptOpenJDK/openjdk8-binaries/releases/download/jdk${JDK_REL}-${JDK_TAG}
LABEL versions.thirdparty.jdk=${JDK_REL}${JDK_TAG}
RUN mkdir -p /dist/thirdparty/java && \
    cd /dist/thirdparty/java && \
    curl -O -L ${JDK_URL}/OpenJDK8U-jre_x64_linux_hotspot_${JDK_REL}${JDK_TAG}.tar.gz && \
    tar -xf OpenJDK8U-jre_x64_linux_hotspot_${JDK_REL}${JDK_TAG}.tar.gz

ENV PATH=/dist/thirdparty/java/jdk8u242-b08-jre/bin/:$PATH

# Get GATK 
ENV GATK_VERSION 4.2.6.1
ENV GATK_ARCHIVE=gatk-${GATK_VERSION}.zip
LABEL versions.thirdparty.gatk=${GATK_VERSION}
RUN cd /dist/thirdparty && \
    curl -O -L  https://github.com/broadinstitute/gatk/releases/download/${GATK_VERSION}/${GATK_ARCHIVE} && \
    unzip ${GATK_ARCHIVE} && \
    rm -r ${GATK_ARCHIVE} && \ 
    echo '#!/bin/bash -ue \nDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"\njava -jar $DIR/../thirdparty/gatk-${GATK_VERSION}/gatk-package-${GATK_VERSION}-local.jar "$@"' > /dist/bin/gatk && \
    chmod +x /dist/bin/gatk

# Get sambamba
ENV SAMBAMBA_VERSION 0.7.1
LABEL versions.thirdparty.sambamba=${SAMBAMBA_VERSION}
RUN mkdir -p /dist/thirdparty/sambamba && \
    cd /dist/thirdparty/sambamba && \
    curl -O -L https://github.com/biod/sambamba/releases/download/v${SAMBAMBA_VERSION}/sambamba-${SAMBAMBA_VERSION}-linux-static.gz && \
    gunzip sambamba-${SAMBAMBA_VERSION}-linux-static.gz && \
    chmod +x sambamba-${SAMBAMBA_VERSION}-linux-static && \
    echo '#!/bin/bash -ue \nDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"\nexec $DIR/../thirdparty/sambamba/sambamba-${SAMBAMBA_VERSION}-linux-static "$@"' > /dist/bin/sambamba && \
    chmod +x /dist/bin/sambamba

# Get mosdepth
ENV MOSDEPTH_VERSION 0.3.1
LABEL versions.thirdparty.mosdepth=${MOSDEPTH_VERSION}
RUN mkdir -p /dist/thirdparty/mosdepth && \
    cd /dist/thirdparty/mosdepth && \
    curl -O -L https://github.com/brentp/mosdepth/releases/download/v${MOSDEPTH_VERSION}/mosdepth && \
    chmod +x mosdepth && \
    echo '#!/bin/bash -ue \nDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"\nexec $DIR/../thirdparty/mosdepth/mosdepth "$@"' > /dist/bin/mosdepth && \
    chmod +x /dist/bin/mosdepth

# Get samtools
ENV SAMTOOLS_VERSION 1.11
LABEL versions.thirdparty.samtools=${SAMTOOLS_VERSION}
RUN mkdir -p /dist/thirdparty/samtools && \
    cd /dist/thirdparty/samtools && \
    curl -O -L https://github.com/samtools/samtools/releases/download/${SAMTOOLS_VERSION}/samtools-${SAMTOOLS_VERSION}.tar.bz2 && \
    tar -xvf samtools-${SAMTOOLS_VERSION}.tar.bz2 && \
    cd samtools-${SAMTOOLS_VERSION} && \
    ./configure --prefix /dist/thirdparty/samtools --without-curses && \
    make && \
    make install && \
    echo '#!/bin/bash -ue \nDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"\nexec $DIR/../thirdparty/samtools/bin/samtools "$@"' > /dist/bin/samtools && \
    chmod +x /dist/bin/samtools

# Get AnnotSV
# update version for upgrade
ENV ANNOTSV_VERSION=3.4
ENV ANNOTSV=/dist/thirdparty/AnnotSV-$ANNOTSV_VERSION
LABEL versions.thirdparty.annotsv=${ANNOTSV_VERSION}

WORKDIR /dist/thirdparty
RUN wget https://github.com/lgmgeo/AnnotSV/archive/refs/tags/v${ANNOTSV_VERSION}.tar.gz && \
  tar -xvzf v${ANNOTSV_VERSION}.tar.gz && \
  rm v${ANNOTSV_VERSION}.tar.gz && \
  cd ${ANNOTSV} && \
  make PREFIX=. install

ENV PATH=/dist/bin:${PATH}
## add AnnotSV specific path to PATH
## AnnotSV expects its executable to be in $ANNOTSV/bin
ENV PATH=${PATH}:${ANNOTSV}/bin

# Uses /dist/anno-python venv from ella-anno image
COPY --chown=${ANNO_USER} requirements.txt /dist
# add pre-scanned host fingerprints for git+ssh package installs
COPY --chown=${ANNO_USER} ops/known_hosts /home/${ANNO_USER}/.ssh/
# forward ssh-agent from host for private repo access
RUN pipenv install --skip-lock -r /dist/requirements.txt

ENV TARGETS=/anno-targets
ENV PYTHONPATH=${TARGETS}/thirdparty/site-packages:${PYTHONPATH}

COPY --chown=${ANNO_USER} . ${TARGETS}/

ENV ANNO_CONFIG_PATH=${TARGETS}/config/global_config.json
ENV ANNO_INPUT_SCHEMA=${TARGETS}/config/input_schema.py

# create and chown testing mount points
RUN umask 000 && mkdir -p ${TARGETS}/test
