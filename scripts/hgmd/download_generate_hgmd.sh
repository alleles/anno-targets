#!/bin/bash

set -ETeu -o pipefail

THIS_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

MAX_TRIES=3
RETRY_WAIT_SECS=5
URL_QIAGEN=https://my.qiagendigitalinsights.com/bbp/data

usage() {
    echo
    echo "USAGE:"
    echo "    $(basename "${BASH_SOURCE[0]}") -o <output> -r <reference-fasta> <HGMD version>"
    echo
    exit 1
}

while [ $# -gt 0 ]; do
    key="$1"
    case "${key}" in
        -o | --output)
            OUTPUT="$2"
            shift 2
            ;;
        -r | --reference)
            REFERENCE_FASTA="$2"
            shift 2
            ;;
        *)
            break
            ;;
    esac
done

HGMD_VERSION="$*"

if [[ -z "${OUTPUT:-}" ]]; then
    echo >&2 "You must specify an output file name."
    usage
fi

if [[ -z "${HGMD_VERSION:-}" ]]; then
    echo >&2 "You must provide the HGMD version to download (format YYYY.Q)"
    usage
fi

if [[ -z "${HGMD_USER:-}" ]]; then
    echo >&2 "You must set the environment variable HGMD_USER"
    echo >&2 "NOTE: You may add it to the --env-file used for database credentials"
    usage
fi

if [[ -z "${HGMD_PASSWORD:-}" ]]; then
    echo >&2 "You must set environment variable HGMD_PASSWORD"
    echo >&2 "NOTE: You may add it to the --env-file used for database credentials"
    usage
fi

if [[ -z "${REFERENCE_FASTA:-}" ]]; then
    echo >&2 "You must set the reference genome in variable REFERENCE_FASTA"
    echo >&2 "NOTE: You should use the FASTA file downloaded by Anno-builder"
    usage
fi

OUTPUT_BASE="$(basename "${OUTPUT}")"
OUTPUT_DIR="$(dirname "${OUTPUT}")"
mkdir -p "${OUTPUT_DIR}"

if [[ -d "${OUTPUT}" ]]; then
    log "'${OUTPUT}' is a directory. Please specify a valid output file."
    usage
fi

OUTPUT_DRAFT="${OUTPUT_DIR}/draft__${OUTPUT_BASE%.vcf.gz}"

download_file() {
    local internal_file_name="$1"
    local output_path="$2"

    local download_error=0
    local retry_count=0

    echo "Downloading '${internal_file_name}'."
    while [[ ${retry_count} -lt ${MAX_TRIES} ]]; do
        # refresh the token for every download to mitigate the token expiration issue
        access_token="$(get_token)"

        download_error=0
        curl -# \
            -H "Authorization: Bearer ${access_token}" \
            -L "${URL_QIAGEN}/download/hgmd_download?name=${internal_file_name}" \
            -o "${output_path}" 2>&1 \
            || download_error=$?

        if [[ ${download_error} -eq 0 ]]; then
            break
        else
            echo "Error downloading file '${internal_file_name}'."
            retry_count=$((retry_count + 1))
            sleep ${RETRY_WAIT_SECS}
        fi
    done
    if [[ ${download_error} -ne 0 ]]; then
        echo "Failed to download '${internal_file_name}' after ${MAX_TRIES} attempts."
        exit 1
    fi
}

get_token() {
    local token
    token=$(
        curl -s --fail \
            -X POST \
            -H "Content-Type:application/x-www-form-urlencoded" \
            -d "username=${HGMD_USER}" \
            -d "password=${HGMD_PASSWORD}" \
            -d "grant_type=password" \
            -d "client_id=603912630-14192122372034111918-SmRwso" \
            "https://apps.ingenuity.com/qiaoauth/oauth/token"
    )
    if [[ -z "${token}" ]]; then
        echo "Could not get access token"
        exit 1
    fi
    echo "${token}" | jq -r '.access_token'
}

get_files() {
    access_token="$(get_token)"
    local files
    files=$(
        curl -s \
            -H "Authorization: Bearer ${access_token}" \
            -L "${URL_QIAGEN}/files/hgmd_download?version=${HGMD_VERSION}"
    )
    if [[ -z "${files}" ]]; then
        echo "Error [$?]: Not authorized or download limit exceeded."
        exit 1
    else
        # split files table into an array of strings ($names) using newline as record separator
        SAVEIFS=${IFS}
        IFS=$'\n'
        # shellcheck disable=SC2206
        names=(${files})
        NUMFILES=${#names[@]}
        echo "NUMBER OF FILES ------------------ ${NUMFILES}"
        # shellcheck disable=SC2004
        for ((j = 0; j < ${NUMFILES}; j++)); do echo "index: ${j}, value: ${names[${j}]}"; done
        IFS=${SAVEIFS}
        # shellcheck disable=SC2004
        for ((i = 0; i < ${NUMFILES}; i++)); do
            # split each string into array of words ($file) using tab as record separator
            SAVEIFS=${IFS}
            IFS=$'\t'
            singlefile=${names[${i}]}
            # shellcheck disable=SC2206
            file=(${singlefile})
            IFS=${SAVEIFS}
            local partition_dir="${file[2]}"
            mkdir -p "./${partition_dir}"
            local output_path="./${partition_dir}/${file[1]}"
            local internal_file_name="${file[0]}"
            download_file "${internal_file_name}" "${output_path}"
        done
    fi
}

if gunzip -t "hgmd_pro-${HGMD_VERSION}.dump.gz" 2>/dev/null; then
    echo "Data already downloaded."
else
    echo "Downloading data for HGMD version ${HGMD_VERSION}"
    get_files
fi

[[ -s "hgmd_pro-${HGMD_VERSION}.dump" ]] || gunzip -k "hgmd_pro-${HGMD_VERSION}.dump.gz"

echo "Loading database from dump"
while ! mysqladmin ping >/dev/null; do
    echo "MySQL not ready"
    sleep 1
done
mysql -e "drop database hgmd_pro" >/dev/null 2>&1 || true
mysqladmin create hgmd_pro
# Load only data for relevant tables, otherwise this takes forever..
# Perl negative lookahead: matches a backtick '`' _not_ followed by any (|) of the given strings
perlregex='`(?!hgmd_hg19_vcf|mutnomen|extrarefs|allmut)'
grep -v -P "^INSERT INTO ${perlregex}" "hgmd_pro-${HGMD_VERSION}.dump" | mysql hgmd_pro

echo "Generating VCF from SQL"
STEP_OUTPUT="${OUTPUT_DRAFT}.vcf"
mysql hgmd_pro <"${THIS_DIR}/hgmd_to_vcf.sql" >"${STEP_OUTPUT}"

echo "Sorting VCF"
STEP_INPUT="${OUTPUT_DRAFT}.vcf"
STEP_OUTPUT="${OUTPUT_DRAFT}.vcf.gz"
bcftools sort -Oz "${STEP_INPUT}" >"${STEP_OUTPUT}"

echo "Indexing sorted VCF"
tabix -p vcf -f "${STEP_OUTPUT}"

echo "Decomposing / Normalizing sorted VCF"
STEP_INPUT="${OUTPUT_DRAFT}.vcf.gz"
vt decompose -s "${STEP_INPUT}" | vt normalize -n -r "${REFERENCE_FASTA}" - | bgzip \
    >"${OUTPUT}"

echo "Indexing normalized VCF"
tabix -p vcf -f "${OUTPUT}"

echo "Dumping list of pubmed ids"
PUBMED_OUTPUT="${OUTPUT%.vcf.gz}_pubmed_ids.txt"
mysql hgmd_pro -N -B -e \
    "SELECT DISTINCT pmid FROM (
        SELECT pmid FROM extrarefs
        UNION (
            SELECT pmid FROM allmut
        )
    ) AS U
    WHERE pmid IS NOT NULL
    ORDER BY pmid;" \
    >"${PUBMED_OUTPUT}"

echo "Number of variants: $(zgrep -cv '^#' "${OUTPUT}")"
echo "Number of variants per chromosome:"
zgrep -v '^#' "${OUTPUT}" | cut -f 1 | uniq -c | awk '{print $2"\t"$1}'

Ndups=$(zgrep -v '^#' "${OUTPUT}" | cut -f 1,2,4,5 | uniq -d | wc -l)
Npubmeds=$(wc -l "${PUBMED_OUTPUT}" | cut -d ' ' -f 1)
echo "Number of duplicated entries: ${Ndups}"
echo "Number of pubmed ids: ${Npubmeds}"

# shellcheck disable=SC2005
for field in tag acc_num extrarefs pmid comments disease; do
    echo -n "Number of variants with field ${field}: "
    echo "$(zgrep -v '^#' "${OUTPUT}" | grep -c "${field}")"
done
