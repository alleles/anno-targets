DELIMITER //
-- Insert special character sequences for these characters to avoid trouble with 
-- the VCF format
DROP FUNCTION IF EXISTS tr;
CREATE FUNCTION tr(x TEXT) RETURNS TEXT DETERMINISTIC
BEGIN
  SELECT REPLACE(x, ' ', '@#SP') INTO x;
  SELECT REPLACE(x, ',', '@#CM') INTO x;
  SELECT REPLACE(x, ';', '@#SC') INTO x;
  SELECT REPLACE(x, '=', '@#EQ') INTO x;
  SELECT REPLACE(x, '\t', '@#TA') INTO x;
  RETURN x;
END

//

DELIMITER ;

\! echo "##fileformat=VCFv4.1"
\! echo "##FILTER=<ID=PASS,Description=\"All filters passed\">"
\! echo "##INFO=<ID=tag,Number=.,Type=String,Description=\"None\">"
\! echo "##INFO=<ID=acc_num,Number=.,Type=String,Description=\"None\">"
\! echo "##INFO=<ID=extrarefs,Number=.,Type=String,Description=\"Format: (pmid|reftag|disease|comments)\">"
\! echo "##INFO=<ID=disease,Number=.,Type=String,Description=\"None\">"
\! echo "##INFO=<ID=comments,Number=.,Type=String,Description=\"None\">"
\! echo "##INFO=<ID=pmid,Number=.,Type=String,Description=\"None\">"
\! echo "##INFO=<ID=tx,Number=.,Type=String,Description=\"None\">"
\! echo "##contig=<ID=1>"
\! echo "##contig=<ID=2>"
\! echo "##contig=<ID=3>"
\! echo "##contig=<ID=4>"
\! echo "##contig=<ID=5>"
\! echo "##contig=<ID=6>"
\! echo "##contig=<ID=7>"
\! echo "##contig=<ID=8>"
\! echo "##contig=<ID=9>"
\! echo "##contig=<ID=10>"
\! echo "##contig=<ID=11>"
\! echo "##contig=<ID=12>"
\! echo "##contig=<ID=13>"
\! echo "##contig=<ID=14>"
\! echo "##contig=<ID=15>"
\! echo "##contig=<ID=16>"
\! echo "##contig=<ID=17>"
\! echo "##contig=<ID=18>"
\! echo "##contig=<ID=19>"
\! echo "##contig=<ID=20>"
\! echo "##contig=<ID=21>"
\! echo "##contig=<ID=22>"
\! echo "##contig=<ID=X>"
\! echo "##contig=<ID=Y>"
WITH
    vcf AS (SELECT id as acc_num, chrom, pos, '.' AS id, ref, alt, '.' AS qual, '.' AS filter FROM hgmd_hg19_vcf WHERE info NOT LIKE '%MUT=REF%'),
    vcf_extrarefs AS (
        SELECT acc_num, CONCAT("extrarefs=", GROUP_CONCAT(er_info SEPARATOR ',')) AS info FROM (
            SELECT acc_num, concat_ws("|", COALESCE(pmid, ""), COALESCE(reftag, ""), COALESCE(tr(disease), ""), COALESCE(tr(comments), "")) as er_info FROM extrarefs) as foo GROUP BY acc_num
    ),
    vcf_mut AS (SELECT acc_num, CONCAT_WS(";", CONCAT("tag=",tag), CONCAT("disease=",tr(disease)), CONCAT("pmid=",pmid), CONCAT("comments=", tr(comments))) as info FROM allmut),
    vcf_tx AS (
        SELECT
            acc_num, CONCAT("tx=", refCORE, ".", refVER) AS info
            FROM mutnomen
    )

SELECT
    chrom AS "#CHROM",
    pos AS "POS",
    vcf.acc_num AS "ID",
    ref AS "REF",
    alt AS "ALT",
    '.' AS "QUAL",
    '.' AS "FILTER",
    CONCAT_WS(";", CONCAT("acc_num=", vcf.acc_num), vcf_tx.info, vcf_mut.info, vcf_extrarefs.info) AS "INFO"
    FROM vcf
    LEFT OUTER JOIN vcf_tx ON vcf_tx.acc_num=vcf.acc_num
    LEFT OUTER JOIN vcf_mut ON vcf_mut.acc_num=vcf.acc_num
    LEFT OUTER JOIN vcf_extrarefs ON vcf_extrarefs.acc_num=vcf.acc_num
    WHERE ref!=alt
    AND alt REGEXP '^[ACGT]*$';

