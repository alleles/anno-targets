"""
Run this script to reformat mitotip downloading file, the --reference option should only contain bases from mitochondrial

"""
import argparse
import sys

parser = argparse.ArgumentParser(description="Reformat the downloading data from mitotip")
parser.add_argument("--input", help="The downloading data from mitotip webpage", dest='input', required=True)
parser.add_argument("--reference", help="The reference genome to extract reference base in the vcf file", dest='ref', required=True)
parser.add_argument("--output", help="The output vcf file", dest='output', required=True)
args = parser.parse_args()

# Read in the reference genome
ref_pos = 1
ref_hash = dict()
with open(args.ref, "r") as ref:
    for ref_line in ref:
        if not ref_line.startswith('>'):
            for ref_base in ref_line.rstrip():
                ref_hash[ref_pos] = ref_base
                ref_pos = ref_pos + 1

required_index = ['MitoTIP_Score', 'Quartile', 'Count', 'Percentage', 'Mitomap_Status']

with open(args.input, "r") as f_in, open(args.output, "w") as f_out:
    for input_line in f_in:
        if input_line.startswith('Position'):
            headers = input_line.rstrip().split("\t")
        
            missing = 0
            for each_required_index in required_index:
                if not each_required_index in headers:
                    print("Required column name {} is not defined in the input file.".format(each_required_index));
                    missing = missing + 1

            if missing > 0:
                sys.exit("Stop the script because the above column name are missing.")
            else:
                print('##fileformat=VCFv4.2', file=f_out)
                print('##INFO=<ID=MitoTIP_Score,Number=1,Type=Float,Description="MitoTIP score">', file=f_out)
                print('##INFO=<ID=Quartile,Number=1,Type=String,Description="quartiles. Current MitoTIP scores range from -5.9 to 21.8, with a median score of 12.7 ">', file=f_out)
                print('##INFO=<ID=Count,Number=1,Type=Integer,Description="Genbank counts">', file=f_out)
                print('##INFO=<ID=Percentage,Number=1,Type=Float,Description="Genbank frequencies">', file=f_out)
                print('##INFO=<ID=Mitomap_Status,Number=1,Type=String,Description="Mitomap status:Absent, Confirmed, Present, Reported_but_unconfirmed, Unclear">', file=f_out)
                print('##contig=<ID=MT,length=16569>', file=f_out)
                print("\t".join(['#CHROM', 'POS', 'ID', 'REF', 'ALT', 'QUAL', 'FILTER', 'INFO']), file=f_out)

        else:
            infos = input_line.rstrip().split("\t")
            pos = int(infos.pop(0))
            ref = infos.pop(0)
            alt = infos.pop(0)

            if alt == ':':
                pos = pos - 1
                alt = ref_hash[pos]
                ref = ''.join([ref_hash[pos], ref]);

            info = str()
            for i in list(range(len(infos))):
                infos[i] = infos[i].replace(' ', '')
                if not info:
                    info = '='.join([headers[i+3], infos[i]])
                else:
                    info = ';'.join([info, '='.join([headers[i+3], infos[i]])])

            print("\t".join(['MT', str(pos), '.', ref, alt, '.', '.', info]), file=f_out)
