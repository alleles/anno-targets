"""
Run this script to reformat the HelixMTdb TSV file, the --reference option should only contain
bases from mitochondrial DNA
"""

import argparse


vcf_metainfo = """##fileformat=VCFv4.2
##FILTER=<ID=PASS,Description="All filters passed">
##INFO=<ID=feature,Number=1,Type=String,Description="genomic feature locations were annotated using the list from MITOMAP and further curated into four groups: protein-coding, rRNA, tRNA and non-coding (all remaining sites including the D-loop)">
##INFO=<ID=gene,Number=1,Type=String,Description="Gene the variant is located in">
##INFO=<ID=counts_hom,Number=1,Type=Integer,Description="Total number of homoplasmic variants">
##INFO=<ID=AF_hom,Number=1,Type=Float,Description="Allele fraction of homoplasmic variants">
##INFO=<ID=counts_het,Number=1,Type=Integer,Description="Total number of heteroplasmic variants">
##INFO=<ID=AF_het,Number=1,Type=Float,Description="Allele fraction of heteroplasmic variants">
##INFO=<ID=mean_ARF,Number=1,Type=Float,Description="Mean raw alternate allele fraction, (counts of reads supporting the alternate allele)/(counts of all reads at this position)">
##INFO=<ID=max_ARF,Number=1,Type=Float,Description="Maximum raw alternate allele fraction">
##INFO=<ID=haplogroups_for_homoplasmic_variants,Number=1,Type=String,Description="Haplogroups for homoplasmic variants">
##INFO=<ID=haplogroups_for_heteroplasmic_variants,Number=1,Type=String,Description="Haplogroups for heteroplasmic variants">
##INFO=<ID=OLD_MULTIALLELIC,Number=1,Type=String,Description="Original chr:pos:ref:alt encoding">
##INFO=<ID=OLD_VARIANT,Number=.,Type=String,Description="Original chr:pos:ref:alt encoding">
##contig=<ID=MT,length=16569>
"""

vcf_header = "\t".join(['#CHROM', 'POS', 'ID', 'REF', 'ALT', 'QUAL', 'FILTER', 'INFO']) + "\n"


parser = argparse.ArgumentParser(description="Reformat the TSV file downloaded from HelixMTdb")
parser.add_argument(
    "--input", help="The downloaded TSV file from HelixMTdb's webpage", dest="input", required=True
)
parser.add_argument(
    "--output", help="The output VCF file", dest="output", required=True
)
args = parser.parse_args()

required_entries = [
    'feature', 'gene', 'counts_hom', 'AF_hom', 'counts_het', 'AF_het', 'mean_ARF', 'max_ARF'
]


with open(args.input, "r") as f_in, open(args.output, "w") as f_out:
    for input_line in f_in:
        if input_line.startswith('locus'):
            header = input_line.rstrip().split("\t")
            for entry in required_entries:
                if not entry in header:
                    exit(f"Required entry '{entry}' is not defined in the input file: Aborting.")

            f_out.write(vcf_metainfo)
            f_out.write(vcf_header)

        else:
            info = input_line.rstrip().split("\t")
            pos = int(info[0].split(":")[1])
            alleles = info[1].replace('"','').replace('[','').replace(']', '').split(",")
            ref = alleles.pop(0)
            alt = ",".join(alleles)

            info_field = ""
            for n in list(range(2, len(info))):
                info[n] = info[n].replace(" ", "")

                # If max_ARF is NA or mean_ARF is NaN (not a float), skip
                if (
                    ("NA" in info[n] and header[n] == "max_ARF")
                    or ("NaN" in info[n] and header[n] == "mean_ARF")
                ):
                    print(f"Skipping record {info} due to non-meaningful ARF.")
                    continue

                # Build up INFO field
                info_part = "=".join([header[n], info[n]])
                if not info_field:
                    info_field = info_part
                else:
                    info_field = ";".join([info_field, info_part])

            print("\t".join(["MT", str(pos), ".", ref, alt, ".", ".", info_field]), file=f_out)

