#!/bin/bash -e

# Download and parse (to VCF) mitomap confirmed pathogenic mutations table from:
# https://www.mitomap.org/foswiki/bin/view/MITOMAP/ConfirmedMutations

THIS_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

MITOMAP_URL=https://www.mitomap.org/foswiki/bin/view/MITOMAP/ConfirmedMutations

usage() {
    echo
    echo "USAGE:"
    echo "    $(basename "${BASH_SOURCE[0]}") -o <output> <revision>"
    echo
    exit 1
}

log() {
    echo "[$$] $(date +%Y-%m-%d\ %H:%M:%S) - $1"
}

export HOME=/tmp

# set -x

while [ $# -gt 0 ]; do
    key="$1"
    case "${key}" in
        -o | --output)
            OUTPUT="$2"
            shift 2
            ;;
        -h | --help)
            usage
            ;;
        *)
            break
            ;;
    esac
done

if [[ -z "${CPM_TABLE:-}" ]]; then
    echo >&2 "Error! Undefined CPM_TABLE variable. Please, set it to point to the source CPM table."
    echo >&2 "[The table can be downloaded from '${MITOMAP_URL}']"
    exit 1
fi

if [[ ! -f "${CPM_TABLE:-}" ]]; then
    echo >&2 "Error! No source CPM table '${CPM_TABLE}' found."
    echo >&2 "[It can be downloaded from '${MITOMAP_URL}']"
    exit 1
fi

if [[ -z "${OUTPUT:-}" ]]; then
    echo >&2 "You must specify an output file name."
    usage
fi

OUTPUT_BASE="$(basename "${OUTPUT}")"
OUTPUT_DIR="$(dirname "${OUTPUT}")"
mkdir -p "${OUTPUT_DIR}"

if [[ -d "${OUTPUT}" ]]; then
    log "'${OUTPUT}' is a directory. Please specify a valid output file."
    usage
fi

if [[ -f "${OUTPUT}" ]]; then
    log "Removing existing '${OUTPUT}' file"
    rm "${OUTPUT}"
fi

OUTPUT_DRAFT="${OUTPUT_DIR}/draft__${OUTPUT_BASE%.vcf.gz}"

log "Downloading mitochondrial reference file"
"${THIS_DIR}/download_reference.sh" -o mtref.fasta

log "Downloading mitomap's CPM table and converting it to VCF"
python3 "${THIS_DIR}/convert_mitomap_cpm.py" --reference mtref.fasta \
    --output "${OUTPUT_DRAFT}.vcf" "${CPM_TABLE}"

log "Block gzipping VCF"
bgzip -c "${OUTPUT_DRAFT}.vcf" >"${OUTPUT}"

log "Indexing '${OUTPUT}'"
tabix -p vcf -f "${OUTPUT}"

log "Finished processing mitomap CPM data"
