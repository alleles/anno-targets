#!/bin/bash -e

# Extract mitochondrial DNA records from the complete ClinVar VCF

usage() {
    echo
    echo "USAGE:"
    echo "    $(basename "${BASH_SOURCE[0]}") -d <data directory>"
    echo
    exit 1
}

log() {
    echo "[$$] $(date +%Y-%m-%d\ %H:%M:%S) - $1"
}

while [ $# -gt 0 ]; do
    key="$1"
    case "${key}" in
        -d | --data-dir)
            DATA_DIR="$2"
            shift 2
            ;;
        -h | --help)
            usage
            ;;
        *)
            break
            ;;
    esac
done

if [[ -z "${DATA_DIR:-}" ]]; then
    echo >&2 "You must specify the location of the complete ClinVar VCF."
    usage
fi

readarray -d '' clinvar_vcfs < <(find "${DATA_DIR}" -name '*.vcf.gz' -print0)

if ((${#clinvar_vcfs[@]} < 1)); then
    echo "Error: No ClinVar VCFs found at '${DATA_DIR}'."
    exit 1
elif ((${#clinvar_vcfs[@]} > 1)); then
    printf "Error: Multiple ClinVar VCFs present:"
    printf "       - %s\n" "${clinvar_vcfs[@]}"
    printf "       Please, remove the unwanted ones and try again."
    exit 1
fi

bcftools view -r 'MT' -Oz "${clinvar_vcfs[0]}"
