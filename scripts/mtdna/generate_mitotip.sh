#!/bin/bash -e

# Download mitotip_scores.txt from https://mitomap.org/downloads
# Reformat the file to standard vcf file

THIS_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

usage() {
    if [[ -n $1 ]]; then
        echo "$1"
    fi
    echo
    echo "USAGE:"
    echo "    $(basename "${BASH_SOURCE[0]}") -o <output> <revision>"
    echo
    exit 1
}

log() {
    echo "[$$] $(date +%Y-%m-%d\ %H:%M:%S) - $1"
}

export HOME=/tmp

# set -x

while [ $# -gt 0 ]; do
    key="$1"
    case "${key}" in
        -o | --output)
            OUTPUT="$2"
            shift 2
            ;;
        -h | --help)
            usage
            ;;
        *)
            break
            ;;
    esac
done

REVISION="$*"

if [[ -z "${REVISION}" ]]; then
    usage "Error! revision is missing, e.g. 2022.03"
fi

if [[ -z "${OUTPUT:-}" ]]; then
    echo >&2 "You must specify an output file name."
    usage
fi

OUTPUT_BASE="$(basename "${OUTPUT}")"
OUTPUT_DIR="$(dirname "${OUTPUT}")"
mkdir -p "${OUTPUT_DIR}"

if [[ -d "${OUTPUT}" ]]; then
    log "'${OUTPUT}' is a directory. Please specify a valid output file."
    usage
fi

if [[ -f "${OUTPUT}" ]]; then
    log "Removing existing '${OUTPUT}' file"
    rm "${OUTPUT}"
fi

OUTPUT_DRAFT="${OUTPUT_DIR}/draft__${OUTPUT_BASE%.vcf.gz}"

log "Downloading mitochondrial reference file"
"${THIS_DIR}/download_reference.sh" -o mtref.fasta

log "Downloading mitomap TIP scores"
curl https://mitomap.org/downloads/mitotip_scores.txt --output "${OUTPUT_DRAFT}.txt"

log "Converting 'chrM' to 'MT' and writing VCF"
python3 "${THIS_DIR}"/convert_mitotip.py \
    --input "${OUTPUT_DRAFT}.txt" \
    --output "${OUTPUT_DRAFT}.vcf" \
    --reference mtref.fasta

log "Block gzipping VCF"
bgzip -c "${OUTPUT_DRAFT}.vcf" >"${OUTPUT}"

log "Indexing '${OUTPUT}'"
tabix -p vcf -f "${OUTPUT}"

log "Finished processing mitomap TIP scores data"
