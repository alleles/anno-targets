import argparse
import base64
import csv
import requests
import re
import sys


def parse_fields(record):
    required_fields = {
        'locus_type': "^Locus Type$",
        'locus': "^Locus$",
        'associated_diseases': "^Associated Diseases$",
        'allele': "^Allele$",
        'aa_rna': "^aa.+RNA$",
        'last_status_update': "^Last.*Status.*Update$"
    }

    parsed_record = dict()
    for key, value in required_fields.items():
        try:
            parsed_record[key] = next(
                record[srckey] for srckey in record.keys() if re.match(value, srckey)
            )
        except StopIteration:
            sys.exit(f"No field matches '{value}' in the source data: aborting.")

        # encode disease information
        if key == "associated_diseases":
            parsed_record[key] = base64.b64encode(parsed_record[key].encode('ascii')).decode()

        # convert dots to hyphens in dates
        if key == "last_status_update":
            parsed_record[key] = parsed_record[key].replace('.', '-')

        # remove spaces from comma-space pairs
        while ', ' in parsed_record[key]:
            parsed_record[key] = parsed_record[key].replace(', ', ',')

        # convert remaining spaces to underscores
        if ' ' in parsed_record[key]:
            parsed_record[key] = parsed_record[key].replace(' ', '_')

        if ';' in parsed_record[key]:
            print(f"Warning: semicolon detected in INFO section '{parsed_record[key]}'")

    return parsed_record


parser = argparse.ArgumentParser(
    description="Parse MITOMAP table of confirmed pathogenic variants and save them to a VCF"
)
parser.add_argument(
    "--reference",
    dest='ref',
    help="Reference genome FASTA file",
    required=True
)
parser.add_argument(
    "--output",
    dest='output',
    help="Output VCF file name",
    required=True
)
parser.add_argument(
    dest='input',
    help="Input CPM table file name"
)
args = parser.parse_args()


# Read in the reference genome
ref_pos = 1
ref_hash = dict()
with open(args.ref, "r") as ref:
    for ref_line in ref:
        if not ref_line.startswith('>'):
            for ref_base in ref_line.rstrip():
                ref_hash[ref_pos] = ref_base
                ref_pos = ref_pos + 1


with open(args.output, "w") as f_out:

    # print headers for the VCF file
    print('##fileformat=VCFv4.2', file=f_out)
    print('##ALT=<ID=INV,Description="Inversion">', file=f_out)
    print('##INFO=<ID=locus_type,Number=1,Type=String,Description="tRNA/coding/rRNA">', file=f_out)
    print('##INFO=<ID=locus,Number=1,Type=String,Description="Gene">', file=f_out)
    print('##INFO=<ID=associated_diseases,Number=1,Type=String,Description="Associated diseases, base64 coded, base64.b64decode(value.encode(\'ascii\')).decode(\'ascii\')">', file=f_out)
    print('##INFO=<ID=aa_rna,Number=1,Type=String,Description="Amino acid change or RNA change">', file=f_out)
    print('##INFO=<ID=last_status_update,Number=1,Type=String,Description="Last status update date">', file=f_out)
    print('##INFO=<ID=SVTYPE,Number=1,Type=String,Description="Type of structural variant">', file=f_out)
    print('##INFO=<ID=END,Number=1,Type=Integer,Description="End position of the variant described in this record">', file=f_out)
    print('##contig=<ID=MT,length=16569>', file=f_out)
    print("\t".join(['#CHROM', 'POS', 'ID', 'REF', 'ALT', 'QUAL', 'FILTER', 'INFO']), file=f_out)

    header = dict()

    with open(args.input, 'r') as cpm_table:
        cpm_reader = csv.DictReader(cpm_table, delimiter=',')

        for record in cpm_reader:
            if not any(item for item in record):
                # ignore blank records
                continue

            parsed_record = parse_fields(record)

            # assemble INFO field
            info = ""
            for tag, text in parsed_record.items():
                if not info:
                    info = '='.join([tag, text])
                else:
                    info = ';'.join([info, '='.join([tag, text])])

            # parse deletion lines
            # m.3273delT or m.9205_9206delTA, pos is 3272(3273-1) or 9204(9205-1)
            if 'del' in parsed_record['allele']:
                (ref_info, base_del) = parsed_record['allele'].replace('m.', '').split('del')
                if '_' in ref_info:
                    ref_info = ref_info.split('_')[0]
                base_for = ref_hash[int(ref_info)-1]
                pos = str(int(ref_info) - 1)
                ref = ''.join([base_for, base_del])
                alt = base_for
                print(
                    "\t".join([
                        'MT',
                        pos,
                        '.',
                        ref,
                        alt,
                        '.',
                        '.',
                        info
                    ]),
                    file=f_out
                )
                continue

            # parse inversion lines:
            # m.3902_3908ACCTTGCinv, pos is 3902
            if 'inv' in parsed_record['allele']:
                [ref_info, alt] = parsed_record['allele'].replace('m.', '').split('_')
                pos = ref_info
                ref = ref_hash[int(ref_info)]
                print(
                    "\t".join([
                        'MT',
                        pos,
                        '.',
                        ref,
                        '<INV>',
                        '.',
                        '.',
                        ';'.join([
                            info, 'SVTYPE=INV', '='.join([
                                'END', "".join(filter(str.isdigit, alt))
                            ])
                        ])
                    ]),
                    file=f_out
                )
                continue

            # parse insertion lines:
            # m.5537_5538insT, pos is 5537
            if 'ins' in parsed_record['allele']:
                [ref_info, alt] = parsed_record['allele'].replace('m.', '').split('_')
                pos = ref_info
                ref = ref_hash[int(ref_info)]
                print(
                    "\t".join([
                        'MT',
                        pos,
                        '.',
                        ref,
                        ''.join([ref_hash[int(ref_info)], alt.split('ins')[1]]),
                        '.',
                        '.',
                        info
                    ]),
                    file=f_out
                )
                continue
            
            # parse SNP lines: m.3890G>A
            [ref_info, alt] = parsed_record['allele'].replace('m.', '').split('>')
            ref = ref_info[len(ref_info) - 1]
            pos = ref_info.replace(ref, '')
            if pos.isdigit():
                print(
                    "\t".join([
                        'MT',
                        pos,
                        '.',
                        ref,
                        alt,
                        '.',
                        '.',
                        info
                    ]),
                    file=f_out
                )
            else:
                print(f"Warning: '{parsed_record['allele']}' has no digital position.")

