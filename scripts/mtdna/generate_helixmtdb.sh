#!/bin/bash -e

# Download HelixMTdb_${REVISION}.tsv from https://helix-research-public.s3.amazonaws.com/mito/
# Reformat the file to a standard VCF file

THIS_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

BASE_URL=https://helix-research-public.s3.amazonaws.com/mito

usage() {
    if [[ -n $1 ]]; then
        echo "$1"
    fi
    echo
    echo "USAGE:"
    echo "    $(basename "${BASH_SOURCE[0]}") -o <output> <revision>"
    echo
    exit 1
}

log() {
    echo "[$$] $(date +%Y-%m-%d\ %H:%M:%S) - $1"
}

export HOME=/tmp

# set -x

while [ $# -gt 0 ]; do
    key="$1"
    case "${key}" in
        -o | --output)
            OUTPUT="$2"
            shift 2
            ;;
        -h | --help)
            usage
            ;;
        *)
            break
            ;;
    esac
done

REVISION="$*"

if [[ -z "${REVISION}" ]]; then
    usage "Error! no revision specified, e.g. '20200327'"
fi

if [[ -z "${OUTPUT:-}" ]]; then
    echo >&2 "You must specify an output file name."
    usage
fi

OUTPUT_BASE="$(basename "${OUTPUT}")"
OUTPUT_DIR="$(dirname "${OUTPUT}")"
mkdir -p "${OUTPUT_DIR}"

if [[ -d "${OUTPUT}" ]]; then
    log "'${OUTPUT}' is a directory. Please specify a valid output file."
    usage
fi

if [[ -f "${OUTPUT}" ]]; then
    log "Removing existing '${OUTPUT}' file"
    rm "${OUTPUT}"
fi

log "Downloading mitochondrial reference file"
"${THIS_DIR}/download_reference.sh" -o mtref.fasta

OUTPUT_DRAFT="${OUTPUT_DIR}/draft__${OUTPUT_BASE%.vcf.gz}"

set -x

log "Downloading 'HelixMTdb_${REVISION}.tsv'"
STEP_INPUT="${BASE_URL}/HelixMTdb_${REVISION}.tsv"
STEP_OUTPUT="${OUTPUT_DRAFT}.tsv"
curl "${STEP_INPUT}" --output "${STEP_OUTPUT}"

log "Converting 'chrM' to 'MT' and writing VCF"
STEP_INPUT="${OUTPUT_DRAFT}.tsv"
STEP_OUTPUT="${OUTPUT_DRAFT}.vcf"
python3 "${THIS_DIR}"/convert_helixmtdb.py --input "${STEP_INPUT}" --output "${STEP_OUTPUT}"

echo "Sorting VCF"
STEP_INPUT="${OUTPUT_DRAFT}.vcf"
STEP_OUTPUT="${OUTPUT_DRAFT}_sorted.vcf"
bcftools sort "${STEP_INPUT}" >"${STEP_OUTPUT}"

echo "Decomposing / Normalizing sorted VCF"
STEP_INPUT="${OUTPUT_DRAFT}_sorted.vcf"
vt decompose -s "${STEP_INPUT}" | vt normalize -n -r mtref.fasta - | bgzip >"${OUTPUT}"

echo "Indexing '${OUTPUT}'"
tabix -p vcf -f "${OUTPUT}"

log "Finished processing HelixMTdb data"
