#!/bin/bash -e

# Download mitochondrial DNA reference FASTA (rename the sequence to 'MT')

THIS_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

RESOURCE="mitochondria_references/Homo_sapiens_assembly38.chrM.fasta"
BASEURL="https://github.com/broadinstitute/gatk/blob/master/src/test/resources/large"

usage() {
    echo
    echo "USAGE:"
    echo "    $(basename "${BASH_SOURCE[0]}") -d <data directory>"
    echo
    exit 1
}

while [ $# -gt 0 ]; do
    key="$1"
    case "${key}" in
        -o | --output)
            OUTPUT="$2"
            shift 2
            ;;
        -h | --help)
            usage
            ;;
        *)
            break
            ;;
    esac
done

if [[ -z "${OUTPUT:-}" ]]; then
    echo >&2 "You must specify an output file name."
    usage
fi

wget -O "${OUTPUT}" ${BASEURL}/${RESOURCE}?raw=true
read -r from_str to_str <"${THIS_DIR}"/chr_name_conversion.txt
sed -i "s/^>${from_str}/>${to_str}/" "${OUTPUT}"
