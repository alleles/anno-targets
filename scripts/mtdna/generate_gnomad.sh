#!/bin/bash -e

# Download gnomAD chrM from http://gnomad.broadinstitute.org/downloads
# Note that Broad keep changing file name formats. This version is configured for the 3.1 release.

THIS_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

usage() {
    if [[ -n $1 ]]; then
        echo "$1"
    fi
    echo
    echo "USAGE:"
    echo "    $(basename "${BASH_SOURCE[0]}") [-s/--skip-md5] -o <output> <revision>"
    echo
    exit 1
}

log() {
    echo "[$$] $(date +%Y-%m-%d\ %H:%M:%S) - $1"
}

pcnt() {
    local CNT
    CNT=$(pgrep -P $$ | wc -l)
    echo $((CNT - 1))
}

rm_on_mismatch() {
    local local_file="$1"
    local remote_hash="$2"
    local local_hash
    local_hash=$(openssl dgst -md5 -binary "${local_file}" | openssl enc -base64)
    if [[ "${local_hash}" != "${remote_hash}" ]]; then
        rm "${local_file}" # (hash mismatch)
    else
        log "Skipping already downloaded file: $(basename "${local_file}")"
    fi
}

export HOME=/tmp

# set -x

while [ $# -gt 0 ]; do
    key="$1"
    case "${key}" in
        -o | --output)
            OUTPUT="$2"
            shift 2
            ;;
        -s | --skip-md5)
            SKIP_MD5=1
            shift
            ;;
        -h | --help)
            usage
            ;;
        *)
            break
            ;;
    esac
done

REVISION="$*"

if [[ -z "${REVISION}" ]]; then
    usage "Error! revision is missing, e.g. 3.1"
fi

if [[ -z "${OUTPUT:-}" ]]; then
    echo >&2 "You must specify an output file name."
    usage
fi

OUTPUT_DIR="$(dirname "${OUTPUT}")"
mkdir -p "${OUTPUT_DIR}"

GSURL=gs://gcp-public-data--gnomad/release/${REVISION}/vcf/genomes

GSUTIL=$(which gsutil 2>/dev/null || echo)
if [[ -z ${GSUTIL} ]]; then
    usage "Unable to find gsutil. Make sure it's installed and accessible from PATH, and try again."
fi

MAX_PCNT=${MAX_PCNT:-$(grep -c processor /proc/cpuinfo)}

mapfile -t GS_FILES < <(${GSUTIL} ls "${GSURL}/gnomad.genomes.v${REVISION}.sites.chrM.vcf.bgz*")
# Go through all the files listed remotely and check for any them already exist locally
# If a file exists, compare its size and md5sum (unless set to skip md5 check)
# * If both values match, keep the local file and do not download a new one
# * Otherwise, delete the local file and download a new one
for gs_file in "${GS_FILES[@]}"; do
    local_file=${GNOMAD_DATA_DIR}/$(basename "${gs_file}")
    if [[ -f ${local_file} ]]; then
        # compare file size for faster exclusion
        gs_file_size=$(${GSUTIL} du "${gs_file}" | cut -f 1 -d ' ')

        if [[ ${gs_file_size} -eq $(stat -c %s "${local_file}") ]]; then
            if [[ -z ${SKIP_MD5} ]]; then
                while [[ $(pcnt) -ge ${MAX_PCNT} ]]; do
                    sleep 15
                done
                gs_file_hash=$(
                    ${GSUTIL} hash "${gs_file}" | grep 'Hash (md5):' | perl -lane 'print $F[-1]'
                )
                # checksum is single thread and slow, so we parallelize what we can
                rm_on_mismatch "${local_file}" "${gs_file_hash}" &
            fi
        else
            rm "${local_file}" # (size mismatch)
        fi
    fi
done
wait

mapfile -t LOCAL_FILES < <(ls .)
for local_file in "${LOCAL_FILES[@]}"; do
    for i in "${!GS_FILES[@]}"; do
        if [[ "$(basename "${GS_FILES[i]}")" == "${local_file}" ]]; then
            unset "GS_FILES[${i}]"
        fi
    done
done

# Download all new files in parallel
if [[ ${#GS_FILES[@]} -gt 0 ]]; then
    log "Downloading ${#GS_FILES[@]} gnomAD files"
    ${GSUTIL} -m cp "${GS_FILES[@]}" ./
else
    echo "All gnomAD chrM files already downloaded"
fi

log "Converting 'chrM' to 'MT' and writing VCF"
bcftools annotate -x INFO/hap_hl_hist,INFO/pop_hl_hist,INFO/vep \
    --rename-chrs "${THIS_DIR}"/chr_name_conversion.txt -Oz \
    "gnomad.genomes.v${REVISION}.sites.chrM.vcf.bgz" \
    >draft__gnomad.genomes.vcf.gz

mv draft__gnomad.genomes.vcf.gz "${OUTPUT}"

log "Indexing '${OUTPUT}'"
tabix -p vcf -f "${OUTPUT}"

log "Finished processing gnomAD data"
